package com.openrsc.interfaces.misc;

import com.openrsc.interfaces.InputListener;
import com.openrsc.interfaces.NComponent;
import com.openrsc.interfaces.NCustomComponent;
import com.openrsc.interfaces.misc.party.Party;
import com.openrsc.client.entityhandling.EntityHandler.GUIPARTS;
import orsc.enumerations.MessageType;
import com.openrsc.client.entityhandling.EntityHandler;
import orsc.graphics.gui.SocialLists;
import orsc.mudclient;
import orsc.util.GenUtil;
import orsc.Config;
import java.util.Date;
import java.text.SimpleDateFormat;

public class CombatStyleMenu {
	public static mudclient mc;
	public NComponent combatStyleMenuComponent;

	public CombatStyleMenu(final mudclient graphics) {
		
		combatStyleMenuComponent = new NComponent(graphics);
		//combatStyleMenuComponent.setBackground(0xFFFFFF, 0xFFFFFF, 128);
		combatStyleMenuComponent.setLocation((graphics.getGameWidth() - 175) / 20, graphics.getGameHeight() - 310);

		NCustomComponent combatStyleMenuItself = new NCustomComponent(graphics) {
			@Override
			public void render() {
				float combatStyleMenuWidth = 120;
				int i2 = getY() - 6;
				int var12;
				if(graphics.getCombatStyle() == 0) {
					graphics.getSurface().drawBoxAlpha(getX() - 25, i2 - 9, 62, 10, GenUtil.buildColor(255, 0, 0), 128);
				} else 
				if(graphics.getCombatStyle() == 1) {
					graphics.getSurface().drawBoxAlpha(getX() - 25, i2 + 3, 62, 10, GenUtil.buildColor(255, 0, 0), 128);
				} else 
				if(graphics.getCombatStyle() == 2) {
					graphics.getSurface().drawBoxAlpha(getX() - 25, i2 + 15, 62, 10, GenUtil.buildColor(255, 0, 0), 128);
				} else 
				if(graphics.getCombatStyle() == 3) {
					graphics.getSurface().drawBoxAlpha(getX() - 25, i2 + 28, 62, 10, GenUtil.buildColor(255, 0, 0), 128);
				}
				graphics.getSurface().drawString("Controlled", getX() - 20, i2, 0xffffff, 0);
				i2 += 12;
				graphics.getSurface().drawString("Aggressive", getX() - 20, i2, 0xffffff, 0);
				i2 += 12;
				graphics.getSurface().drawString("Accurate", getX() - 20, i2, 0xffffff, 0);
				i2 += 12;
				graphics.getSurface().drawString("Defensive", getX() - 20, i2, 0xffffff, 0);
				i2 += 12;
				if (graphics.getMouseX() >= getX() - 26 && graphics.getMouseX() <= getX() + 37) {
					if (graphics.getMouseClick() == 1) {
						//graphics.showMessage(false, null, "THISTHISTHISTHIS", MessageType.GAME, 0,
						//null, "@cya@");
					}
				}
				combatStyleMenuComponent.setSize(62, 52);
			}
		};
		combatStyleMenuItself.setLocation(25, 20);
		combatStyleMenuComponent.setBackground(10000536, 10000536, 80);

		final NComponent headerComponent = new NComponent(graphics);
		headerComponent.setSize(62, 16);
		//headerComponent.setBackground(0, 0, 156);
		headerComponent.setLocation(0, 0);
		headerComponent.setFontColor(0xFFFFFF, 0xFFFFFF);
		headerComponent.setTextCentered(true);
		headerComponent.setInputListener(new InputListener() {
			@Override
			public boolean onMouseDown(int clickX, int clickY, int mButtonDown, int mButtonClick) {

				if (mButtonDown == 2 && combatStyleMenuComponent.isVisible()) {
					int newX = clickX - (headerComponent.getWidth() / 2);
					int newY = clickY - 5;

					int totalCoverageX = newX + combatStyleMenuComponent.getWidth();
					int totalCoverageY = newY + combatStyleMenuComponent.getHeight();

					if (totalCoverageX > graphics.getGameWidth()) {
						newX -= totalCoverageX - graphics.getGameWidth();
					}
					if (totalCoverageY > graphics.getGameHeight()) {
						newY -= totalCoverageY - graphics.getGameHeight();
					}
					if (newX < 0)
						newX = 0;
					if (newX < 0)
						newX = 0;
					combatStyleMenuComponent.setLocation(newX, newY);
					return true;
				}
				return false;
			}
		});
		
		NComponent menuButton = new NComponent(graphics);
		menuButton.setLocation(0, 0);
		menuButton.setSize(62, 14);
		menuButton.setInputListener(new InputListener() {
			@Override
			public boolean onMouseDown(int clickX, int clickY, int mButtonDown, int mButtonClick) {
				if (mButtonClick == 1) {
					graphics.setCombatStyle(0);
					return true;
				}
				return false;
			}
		});
		combatStyleMenuComponent.addComponent(menuButton);
		NComponent menuButton2 = new NComponent(graphics);
		menuButton2.setLocation(0, 14);
		menuButton2.setSize(62, 14);
		menuButton2.setInputListener(new InputListener() {
			@Override
			public boolean onMouseDown(int clickX, int clickY, int mButtonDown, int mButtonClick) {
				if (mButtonClick == 1) {
					graphics.setCombatStyle(1);
					return true;
				}
				return false;
			}
		});
		combatStyleMenuComponent.addComponent(menuButton2);
		NComponent menuButton3 = new NComponent(graphics);
		menuButton3.setLocation(0, 28);
		menuButton3.setSize(62, 14);
		menuButton3.setInputListener(new InputListener() {
			@Override
			public boolean onMouseDown(int clickX, int clickY, int mButtonDown, int mButtonClick) {
				if (mButtonClick == 1) {
					graphics.setCombatStyle(2);
					return true;
				}
				return false;
			}
		});
		combatStyleMenuComponent.addComponent(menuButton3);
		NComponent menuButton4 = new NComponent(graphics);
		menuButton4.setLocation(0, 42);
		menuButton4.setSize(62, 14);
		menuButton4.setInputListener(new InputListener() {
			@Override
			public boolean onMouseDown(int clickX, int clickY, int mButtonDown, int mButtonClick) {
				if (mButtonClick == 1) {
					graphics.setCombatStyle(3);
					return true;
				}
				return false;
			}
		});
		combatStyleMenuComponent.addComponent(menuButton4);
		combatStyleMenuComponent.addComponent(headerComponent);
		combatStyleMenuComponent.addComponent(combatStyleMenuItself);
		combatStyleMenuComponent.setVisible(false);

	}

	public void show() {
		combatStyleMenuComponent.setVisible(true);
	}

	public void hide() {
		combatStyleMenuComponent.setVisible(false);
	}

	public void resetCombatStyleMenu() {
		combatStyleMenuComponent.setVisible(false);
	}

	public NComponent getComponent() {
		return combatStyleMenuComponent;
	}
}
