package com.openrsc.interfaces.misc;

import com.openrsc.interfaces.InputListener;
import com.openrsc.interfaces.NComponent;
import com.openrsc.interfaces.NCustomComponent;
import com.openrsc.interfaces.misc.party.Party;
import com.openrsc.client.entityhandling.EntityHandler.GUIPARTS;
import com.openrsc.client.entityhandling.EntityHandler;
import orsc.graphics.gui.SocialLists;
import orsc.mudclient;
import orsc.util.GenUtil;
import orsc.Config;
import java.util.Date;
import java.text.SimpleDateFormat;

public class SideMenu {
	public static mudclient mc;
	public NComponent sideMenuComponent;

	public SideMenu(final mudclient graphics) {
		
		sideMenuComponent = new NComponent(graphics);
		//sideMenuComponent.setBackground(0xFFFFFF, 0xFFFFFF, 128);
		sideMenuComponent.setLocation((graphics.getGameWidth() - 175) / 20, graphics.getGameHeight() - 310);

		NCustomComponent sideMenuItself = new NCustomComponent(graphics) {
			@Override
			public void render() {
				float sideMenuWidth = 120;
				int i2 = getY() - 6;
				int i3 = 0;
				int var12;
				if (graphics.getF2OppName() == 0) {
					if(graphics.getLocalPlayer().inCombat == 1 && graphics.getOppCurHp() != 0){
						graphics.getSurface().drawString("@red@" + graphics.getOppName() + "@whi@: " + graphics.getOppCurHp() + "/" + graphics.getOppMaxHp(), getX() - 20, i2, 0xffffff, 0);
						i2 += 12;
						i3 += 1;
					}
				}
				if (graphics.getF2Hits() == 0) {
					int hpMissing = 0;
					double prog1 = 0;
					double prog2 = 0;
					hpMissing = graphics.playerStatBase[3] - graphics.playerStatCurrent[3];
					prog1 = ((double) hpMissing / graphics.playerStatBase[3]);
					prog2 = (prog1 * 75);
					int prog3 = (int) Math.round(prog2);
					graphics.getSurface().drawBox(getX() - 20, i2 - 9, 75, 10, 0xFF0000);
					graphics.getSurface().drawBox(getX() - 20, i2 - 9, 75 - prog3, 10, 0x00FF00);
					graphics.getSurface().drawString(
						"Hits: " + graphics.playerStatCurrent[3] + "@gre@/@whi@" + graphics.playerStatBase[3], getX() - 20, i2, 0xffffff, 0);
					i2 += 12;
					i3 += 1;
				}
				if (graphics.getF2Prayer() == 0) {
					int hpMissing = 0;
					double prog1 = 0;
					double prog2 = 0;
					hpMissing = graphics.playerStatBase[5] - graphics.playerStatCurrent[5];
					prog1 = ((double) hpMissing / graphics.playerStatBase[5]);
					prog2 = (prog1 * 75);
					int prog3 = (int) Math.round(prog2);
					graphics.getSurface().drawBox(getX() - 20, i2 - 9, 75, 10, 0x9F9F9F);
					graphics.getSurface().drawBox(getX() - 20, i2 - 9, 75 - prog3, 10, 0x00FF00);
					graphics.getSurface().drawString(
						"Prayer: " + graphics.playerStatCurrent[5] + "@gre@/@whi@" + graphics.playerStatBase[5], getX() - 20, i2, 0xffffff, 0);
					i2 += 12;
					i3 += 1;
				}
				if (graphics.getF2Fatigue() == 0) {
					int hpMissing = 0;
					double prog1 = 0;
					double prog2 = 0;
					hpMissing = 100 - graphics.getStatFatigue();
					prog1 = ((double) hpMissing / 100);
					prog2 = (prog1 * 75);
					int prog3 = (int) Math.round(prog2);
					graphics.getSurface().drawBox(getX() - 20, i2 - 9, 75, 10, 0x9F9F9F);
					graphics.getSurface().drawBox(getX() - 20, i2 - 9, 75 - prog3, 10, 0xFF0000);
						graphics.getSurface().drawString(
						"Fatigue: " + graphics.getStatFatigue() + "%", getX() - 20, i2, 0xffffff, 0);
					i2 += 12;
					i3 += 1;
				}
				if (graphics.getOp() == 0) {
					graphics.getSurface().drawString(
						"Online Players: " + graphics.getOnlinePlayers() + "", getX() - 20, i2, 0xffffff, 0);
					i2 += 12;
					i3 += 1;
				}
				if (graphics.getF2KillStreak() == 0) {
					graphics.getSurface().drawString(
						"Kill Streak: " + graphics.getKillStreak() + "", getX() - 20, i2, 0xffffff, 0);
					i2 += 12;
					i3 += 1;
				}
				Date now = new Date();
				SimpleDateFormat simpleDateformat = new SimpleDateFormat("MM/dd/yyyy");
				if (graphics.getF2Date() == 0) {
					graphics.getSurface().drawString(
						"@yel@" + simpleDateformat.format(now), getX() - 20, i2, 0xffffff, 0);
					i2 += 12;
					i3 += 1;
				}
				if (graphics.getLocalPlayer().isDev()) {
					graphics.getSurface().drawString("Tile: @gre@(@whi@" + (graphics.getPlayerLocalX() + graphics.getMidRegionBaseX())
						+ "@gre@,@whi@" + (graphics.getPlayerLocalZ() + graphics.getMidRegionBaseZ()) + "@gre@)", getX() - 20, i2, 0xffffff, 0);
					i2 += 12;
					i3 += 1;
				}
				sideMenuComponent.setSize(100, i3 * 12 + 4);
			}
		};
		sideMenuItself.setLocation(25, 20);
		sideMenuComponent.setBackground(10000536, 10000536, 80);

		final NComponent headerComponent = new NComponent(graphics);
		int i4 = 0;
		if (graphics.getF2OppName() == 0) {
			if(graphics.getLocalPlayer().inCombat == 1 && graphics.getOppCurHp() != 0){
				i4 += 1;
			}
		}
		if (graphics.getF2Hits() == 0) {
			i4 += 1;
		}
		if (graphics.getF2Prayer() == 0) {
			i4 += 1;
		}
		if (graphics.getF2Fatigue() == 0) {
			i4 += 1;
		}
		if (graphics.getOp() == 0) {
			i4 += 1;
		}
		if (graphics.getF2KillStreak() == 0) {
			i4 += 1;
		}
		if (graphics.getF2Date() == 0) {
			i4 += 1;
		}
		if (graphics.getLocalPlayer().isDev()) {
			i4 += 1;
		}
		headerComponent.setSize(100, 16);
		//headerComponent.setBackground(0, 0, 156);
		headerComponent.setLocation(0, 0);
		headerComponent.setFontColor(0xFFFFFF, 0xFFFFFF);
		headerComponent.setTextCentered(true);
		headerComponent.setInputListener(new InputListener() {
			@Override
			public boolean onMouseDown(int clickX, int clickY, int mButtonDown, int mButtonClick) {

				if (mButtonDown == 2 && sideMenuComponent.isVisible()) {
					int newX = clickX - (headerComponent.getWidth() / 2);
					int newY = clickY - 5;

					int totalCoverageX = newX + sideMenuComponent.getWidth();
					int totalCoverageY = newY + sideMenuComponent.getHeight();

					if (totalCoverageX > graphics.getGameWidth()) {
						newX -= totalCoverageX - graphics.getGameWidth();
					}
					if (totalCoverageY > graphics.getGameHeight()) {
						newY -= totalCoverageY - graphics.getGameHeight();
					}
					if (newX < 0)
						newX = 0;
					if (newX < 0)
						newX = 0;
					sideMenuComponent.setLocation(newX, newY);
					return true;
				}
				return false;
			}
		});

		sideMenuComponent.addComponent(headerComponent);
		sideMenuComponent.addComponent(sideMenuItself);
		sideMenuComponent.setVisible(false);

	}

	public void show() {
		sideMenuComponent.setVisible(true);
	}

	public void hide() {
		sideMenuComponent.setVisible(false);
	}

	public void resetSideMenu() {
		sideMenuComponent.setVisible(false);
	}

	public NComponent getComponent() {
		return sideMenuComponent;
	}
}
