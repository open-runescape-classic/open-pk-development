package com.openrsc.interfaces.misc;

import orsc.Config;
import orsc.graphics.gui.Panel;
import orsc.graphics.two.GraphicsController;
import orsc.enumerations.InputXAction;
import orsc.graphics.gui.InputXPrompt;
import orsc.mudclient;


public final class TdmInterface {
	public Panel experienceConfig;
	public int experienceConfigScroll;
	public boolean selectSkillMenu = false;
	int width = 280, height = 135;
	private boolean visible = false;
	private mudclient mc;
	private int panelColour, textColour, bordColour, lineColour;
	private int x, y;

	public TdmInterface(mudclient mc) {
		this.mc = mc;

		x = (mc.getGameWidth() - width) / 2;
		y = (mc.getGameHeight() - height) / 2;

		experienceConfig = new Panel(mc.getSurface(), 5);
		experienceConfigScroll = experienceConfig.addScrollingList(x + 95, y + 34, 160, height - 40, 20, 2, false);
	}

	public void reposition() {
		x = (mc.getGameWidth() - width) / 2;
		y = (mc.getGameHeight() - height) / 2;

		experienceConfig.reposition(experienceConfigScroll, x + 95, y + 34, 160, height - 40);
	}

	public void onRender(GraphicsController graphics) {
		reposition();
		drawExperienceConfig();
	}

	private void drawExperienceConfig() {
		reposition();

		panelColour = 0x989898;
		textColour = 0xffffff;
		bordColour = 0x000000;
		lineColour = 0x000000;

		experienceConfig.handleMouse(mc.getMouseX(), mc.getMouseY(), mc.getMouseButtonDown(), mc.getLastMouseDown());

		mc.getSurface().drawBoxAlpha(x, y, width, height, panelColour, 90);
		mc.getSurface().drawBoxBorder(x, width, y, height, bordColour);
		this.drawString("Team Deathmatch ", x + 10, y + 30, 3, textColour);
		this.drawString("" + mc.getTdmCbLvl(), x + 127, y + 59, 3, textColour);
		this.drawString("" + mc.getTdmKillLimit(), x + 127, y + 87, 3, textColour);
		if(mc.getTdmInProgress() > 0){
			this.drawString("*In Progress*", x + 140, y + 30, 3, 0xFFFF00);
		}
		mc.getSurface().drawLineHoriz(x, y + 35, width, lineColour);
		int nextLevelExpA = mc.experienceArray[0];
		nextLevelExpA = mc.experienceArray[mc.getPlayerStatBase(0) - 1];
		int nL0 = nextLevelExpA - mc.getPlayerExperience(0);
		this.drawButton(x + 5, y + 45, 95, 20, "Combat Level", 3, false, new ButtonHandler() {
			@Override
			void handle() {
				mc.showItemModX(InputXPrompt.setTdmLvlX, InputXAction.SETTDMCOMBATLEVEL, true);
			}
		});
		this.drawButton(x + 5, y + 70, 95, 20, "Kill limit", 3, false, new ButtonHandler() {
			@Override
			void handle() {
				mc.showItemModX(InputXPrompt.setTdmKillLimitX, InputXAction.SETTDMKILLLIMIT, true);
			}
		});
		this.drawButton(x + 90, y + 105, 95, 20, "@gre@Create TDM", 3, false, new ButtonHandler() {
			@Override
			void handle() {
				mc.sendCommandString("createtdm");
				setVisible(false);
			}
		});
		this.drawCloseButton(x + 245, y + 6, 24, 24, "X", 5, new ButtonHandler() {
			@Override
			void handle() {
				setVisible(false);
			}
		});

		experienceConfig.clearList(experienceConfigScroll);
		int nextLevelExpD = mc.experienceArray[1];
		nextLevelExpD = mc.experienceArray[mc.getPlayerStatBase(1) - 1];
		int nL1 = nextLevelExpD - mc.getPlayerExperience(1);

		if (selectSkillMenu)
			mc.getSurface().drawBoxAlpha(x, y, width, height, 0, 192);
	}

	private void drawString(String str, int x, int y, int font, int color) {
		if (color == 0xFFFFFF) {
			mc.getSurface().drawShadowText(str, x, y, color, font, false);
		} else {
			mc.getSurface().drawString(str, x, y, color, font);
		}
	}

	private void drawStringCentered(String str, int x, int y, int font, int color) {
		int stringWid = mc.getSurface().stringWidth(font, str);
		drawString(str, x + (width / 2) - (stringWid / 2), y, font, color);
	}

	private void drawCloseButton(int x, int y, int width, int height, String text, int font, ButtonHandler handler) {
		int bgBtnColour = 0x333333; // grey
		if (mc.getMouseX() >= x && mc.getMouseY() >= y && mc.getMouseX() <= x + width && mc.getMouseY() <= y + height) {
			bgBtnColour = 16711680; // blue
			if (mc.getMouseClick() == 1) {
				handler.handle();
				mc.setMouseClick(0);
			}
		}
		mc.getSurface().drawBoxAlpha(x, y, width, height, bgBtnColour, 192);
		mc.getSurface().drawBoxBorder(x, width, y, height, 0x242424);
		mc.getSurface().drawString(text, x + (width / 2) - (mc.getSurface().stringWidth(font, text) / 2) - 1, y + height / 2 + 5, textColour, font);
	}

	private void drawButton(int x, int y, int width, int height, String text, int font, boolean checked, ButtonHandler handler) {
		int bgBtnColour = 0x333333; // grey
		if (checked) {
			bgBtnColour = 16711680; // red
		}
		if (mc.getMouseX() >= x && mc.getMouseY() >= y && mc.getMouseX() <= x + width && mc.getMouseY() <= y + height && !selectSkillMenu) {
			if (!checked) {
				bgBtnColour = 0x6580B7; // blue
			}
			if (mc.getMouseClick() == 1) {
				handler.handle();
				mc.setMouseClick(0);
			}
		}
		mc.getSurface().drawBoxAlpha(x, y, width, height, bgBtnColour, 192);
		mc.getSurface().drawBoxBorder(x, width, y, height, 0x242424);
		mc.getSurface().drawString(text, x + (width / 2) - (mc.getSurface().stringWidth(font, text) / 2) - 1, y + height / 2 + 5, textColour, font);
	}

	public boolean isVisible() {
		return visible;
	}

	public void setVisible(boolean visible) {
		this.visible = visible;
	}
}
