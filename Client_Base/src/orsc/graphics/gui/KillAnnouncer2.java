package orsc.graphics.gui;

public class KillAnnouncer2 {
	
	public String killString,killerString,killedString;
	public int killPicture;
	public long displayTime;
	public long pkpo;
	
	public KillAnnouncer2(String kill) {
		killString = kill;
		displayTime = System.currentTimeMillis();
	}

	public KillAnnouncer2(String killer, String killed, int killType, long pkp) {
		killerString = killer;
		killedString = killed;
		killPicture = killType;
		pkpo = pkp;
		displayTime = System.currentTimeMillis();
	}
}
