# Open PK Development

This repository is a fork for the development of Open PK, independent from the main Open RSC framework. Once it is completely ready for release, it will be manually back-ported into the main Open RSC framework.