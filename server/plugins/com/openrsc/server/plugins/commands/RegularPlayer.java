package com.openrsc.server.plugins.commands;

import com.openrsc.server.content.clan.ClanInvite;
import com.openrsc.server.content.clan.ClanManager;
import com.openrsc.server.content.clan.Clan;
import com.openrsc.server.content.party.PartyPlayer;
import com.openrsc.server.model.Point;
import com.openrsc.server.content.party.PartyRank;
import com.openrsc.server.model.entity.player.Group;
import com.openrsc.server.model.entity.player.Player;
import com.openrsc.server.model.snapshot.Chatlog;
import com.openrsc.server.net.DiscordService;
import com.openrsc.server.net.rsc.ActionSender;
import com.openrsc.server.plugins.listeners.action.CommandListener;
import com.openrsc.server.sql.query.logs.ChatLog;
import com.openrsc.server.util.rsc.DataConversions;
import com.openrsc.server.util.rsc.MessageType;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.Arrays;
import java.util.Set;
import java.util.Iterator;
import java.util.Comparator;
import java.util.Map;
import java.util.Random;
import java.sql.ResultSet;
import java.util.Collections;

public final class RegularPlayer implements CommandListener {
	private static final Logger LOGGER = LogManager.getLogger(RegularPlayer.class);

	public static String messagePrefix = null;
	public static String badSyntaxPrefix = null;

	public void onCommand(String cmd, String[] args, Player player) {
		if (isCommandAllowed(player, cmd)) {

			if (messagePrefix == null) {
				messagePrefix = player.getWorld().getServer().getConfig().MESSAGE_PREFIX;
			}
			if (badSyntaxPrefix == null) {
				badSyntaxPrefix = player.getWorld().getServer().getConfig().BAD_SYNTAX_PREFIX;
			}

			handleCommand(cmd, args, player);
		}
	}

	public boolean isCommandAllowed(Player player, String cmd) {
		return player.getWorld().getServer().getConfig().PLAYER_COMMANDS || player.isMod();
	}

	ArrayList<String> iw = new ArrayList<String>();
	ArrayList<String> i2 = new ArrayList<String>();
	SortedMap<Integer, String> sm =
		new TreeMap<Integer, String>(Collections.reverseOrder());
	SortedMap<Integer, String> s2m =
		new TreeMap<Integer, String>(Collections.reverseOrder());
	SortedMap<Integer, String> s3m =
		new TreeMap<Integer, String>(Collections.reverseOrder());

	public void pkp1(Player player) throws SQLException {
		PreparedStatement fetchPlayers = player.getWorld().getServer().getDatabaseConnection()
			.prepareStatement("SELECT `username`, `pkpoints` FROM `" + player.getWorld().getServer().getConfig().MYSQL_TABLE_PREFIX + "players` WHERE `pkpoints`>0");
		ResultSet playersResult = fetchPlayers.executeQuery();

		for (Player p : player.getWorld().getPlayers()) {
			long l = p.getPkPoints();
			int i = (int) l;
			if(i > 0) {
				s2m.put(i, p.getUsername());
			}
		}
		while (playersResult.next()) {
			s2m.put(playersResult.getInt("pkpoints"), "" + playersResult.getString("username"));
			ActionSender.sendBox(player, "" + "All Time PK Point Leaderboard % " + s2m, true);
		}
		playersResult.close();
	}
	public void cp1(Player player) throws SQLException {
		PreparedStatement fetchPlayers = player.getWorld().getServer().getDatabaseConnection()
			.prepareStatement("SELECT `name`, `clan_points` FROM `" + player.getWorld().getServer().getConfig().MYSQL_TABLE_PREFIX + "clan` WHERE `clan_points`>0");
		ResultSet playersResult = fetchPlayers.executeQuery();

		for (Clan c : ClanManager.clans) {
			long l = c.getClanPoints();
			int i = (int) l;
			if(i > 0) {
				s3m.put(i, c.getClanName());
			}
		}
		while (playersResult.next()) {
			String s1 = "" + playersResult.getString("name") + "=" + playersResult.getInt("clan_points") + "";
			s3m.put(playersResult.getInt("clan_points"), "" + playersResult.getString("name"));
			ActionSender.sendBox(player, "" + "Clan Point Leaderboard % " + s3m, true);
		}
		playersResult.close();
	}


	public void handleCommand(String cmd, String[] args, Player player) {
		if (cmd.equalsIgnoreCase("wilderness")) {
			int TOTAL_PLAYERS_IN_WILDERNESS = 0;
			int PLAYERS_IN_F2P_WILD = 0;
			int PLAYERS_IN_P2P_WILD = 0;
			int EDGE_DUNGEON = 0;
			for (Player p : player.getWorld().getPlayers()) {
				if (p.getLocation().inWilderness()) {
					TOTAL_PLAYERS_IN_WILDERNESS++;
				}
				if (p.getLocation().inFreeWild() && !p.getLocation().inBounds(195, 3206, 234, 3258)) {
					PLAYERS_IN_F2P_WILD++;
				}
				if ((p.getLocation().wildernessLevel() >= 48 && p.getLocation().wildernessLevel() <= 56)) {
					PLAYERS_IN_P2P_WILD++;
				}
				if (p.getLocation().inBounds(195, 3206, 234, 3258)) {
					EDGE_DUNGEON++;
				}
			}

			ActionSender.sendBox(player, "There are currently @red@" + TOTAL_PLAYERS_IN_WILDERNESS + " @whi@player" + (TOTAL_PLAYERS_IN_WILDERNESS == 1 ? "" : "s") + " in wilderness % %"
					+ "F2P wilderness(Wild Lvl. 1-48) : @dre@" + PLAYERS_IN_F2P_WILD + "@whi@ player" + (PLAYERS_IN_F2P_WILD == 1 ? "" : "s") + " %"
					+ "P2P wilderness(Wild Lvl. 48-56) : @dre@" + PLAYERS_IN_P2P_WILD + "@whi@ player" + (PLAYERS_IN_P2P_WILD == 1 ? "" : "s") + " %"
					+ "Edge dungeon wilderness(Wild Lvl. 1-9) : @dre@" + EDGE_DUNGEON + "@whi@ player" + (EDGE_DUNGEON == 1 ? "" : "s") + " %"
				, false);
		} else if (cmd.equalsIgnoreCase("c") && player.getWorld().getServer().getConfig().WANT_CLANS) {
			if (player.getClan() == null) {
				player.message(messagePrefix + "You are not in a clan.");
				return;
			}
			String message = "";
			for (String arg : args) {
				message = message + arg + " ";
			}
			player.getClan().messageChat(player, "@cya@" + player.getStaffName() + ":@whi@ " + message);
		} else if (cmd.equalsIgnoreCase("clanaccept") && player.getWorld().getServer().getConfig().WANT_CLANS) {
			if (player.getActiveClanInvite() == null) {
				player.message(messagePrefix + "You have not been invited to a clan.");
				return;
			}
			player.getActiveClanInvite().accept();
			player.message(messagePrefix + "You have joined clan " + player.getClan().getClanName());
		} else if (cmd.equalsIgnoreCase("partyaccept")) {
			if (player.getActivePartyInvite() == null) {
				//player.message(messagePrefix + "You have not been invited to a party.");
				return;
			}
			player.getActivePartyInvite().accept();
			player.message(messagePrefix + "You have joined the party");
		} else if (cmd.equalsIgnoreCase("claninvite") && player.getWorld().getServer().getConfig().WANT_CLANS) {
			if (args.length < 1) {
				player.message(badSyntaxPrefix + cmd.toUpperCase() + " [name]");
				return;
			}

			long invitePlayer = DataConversions.usernameToHash(args[0]);
			Player invited = player.getWorld().getPlayer(invitePlayer);
			if (!player.getClan().isAllowed(1, player)) {
				player.message(messagePrefix + "You are not allowed to invite into clan " + player.getClan().getClanName());
				return;
			}

			if (invited == null) {
				player.message(messagePrefix + "Invalid name or player is not online");
				return;
			}

			ClanInvite.createClanInvite(player, invited);
			player.message(messagePrefix + invited.getUsername() + " has been invited into clan " + player.getClan().getClanName());
		} else if (cmd.equalsIgnoreCase("clankick") && player.getWorld().getServer().getConfig().WANT_CLANS) {
			if (args.length < 1) {
				player.message(badSyntaxPrefix + cmd.toUpperCase() + " [name]");
				return;
			}

			if (player.getClan() == null) {
				player.message(messagePrefix + "You are not in a clan.");
				return;
			}

			String playerToKick = args[0].replace("_", " ");
			long kickedHash = DataConversions.usernameToHash(args[0]);
			Player kicked = player.getWorld().getPlayer(kickedHash);
			if (!player.getClan().isAllowed(3, player)) {
				player.message(messagePrefix + "You are not allowed to kick that player.");
				return;
			}

			if (player.getClan().getLeader().getUsername().equals(playerToKick)) {
				player.message(messagePrefix + "You can't kick the leader.");
				return;
			}

			player.getClan().removePlayer(playerToKick);
			player.message(messagePrefix + playerToKick + " has been kicked from clan " + player.getClan().getClanName());

			if (kicked != null)
				kicked.message(messagePrefix + "You have been kicked from clan " + player.getClan().getClanName());
		} /*else if (cmd.equalsIgnoreCase("gameinfo")) {
			player.updateTotalPlayed();
			long timePlayed = player.getCache().getLong("total_played");

			ActionSender.sendBox(player,
				"@lre@Player Information: %"
					+ " %"
					+ "@gre@Coordinates:@whi@ " + player.getLocation().toString() + " %"
					+ "@gre@Total Time Played:@whi@ " + DataConversions.getDateFromMsec(timePlayed) + " %"
				, true);
		} */ else if (cmd.equalsIgnoreCase("event")) {
			if (!player.getWorld().EVENT) {
				player.message(messagePrefix + "There is no event running at the moment");
				return;
			}
			if (player.getLocation().inWilderness()) {
				player.message(messagePrefix + "Please move out of wilderness first");
				return;
			} else if (player.isJailed()) {
				player.message(messagePrefix + "You can't participate in events while you are jailed.");
				return;
			}
			if (player.getCombatLevel() > player.getWorld().EVENT_COMBAT_MAX || player.getCombatLevel() < player.getWorld().EVENT_COMBAT_MIN) {
				player.message(messagePrefix + "This event is only for combat level range: " + player.getWorld().EVENT_COMBAT_MIN + " - "
					+ player.getWorld().EVENT_COMBAT_MAX);
				return;
			}
			player.teleport(player.getWorld().EVENT_X, player.getWorld().EVENT_Y);
		} else if (cmd.equalsIgnoreCase("g") || cmd.equalsIgnoreCase("pk")) {
			if (!player.getWorld().getServer().getConfig().WANT_GLOBAL_CHAT) return;
			if (player.isMuted()) {
				player.message(messagePrefix + "You are muted, you cannot send messages");
				return;
			}
			if (player.getCache().hasKey("global_mute") && (player.getCache().getLong("global_mute") - System.currentTimeMillis() > 0 || player.getCache().getLong("global_mute") == -1) && cmd.equals("g")) {
				long globalMuteDelay = player.getCache().getLong("global_mute");
				player.message(messagePrefix + "You are " + (globalMuteDelay == -1 ? "permanently muted" : "temporary muted for " + (int) ((player.getCache().getLong("global_mute") - System.currentTimeMillis()) / 1000 / 60) + " minutes") + " from the ::g chat.");
				return;
			}
			long sayDelay = 0;
			if (player.getCache().hasKey("say_delay")) {
				sayDelay = player.getCache().getLong("say_delay");
			}

			long waitTime = 4500;

			if (player.isMod()) {
				waitTime = 0;
			}

			if (System.currentTimeMillis() - sayDelay < waitTime) {
				player.message(messagePrefix + "You can only use this command every " + (waitTime / 1000) + " seconds");
				return;
			}

			if (player.getLocation().onTutorialIsland() && !player.isMod()) {
				return;
			}

			player.getCache().store("say_delay", System.currentTimeMillis());

			StringBuilder newStr = new StringBuilder();
			for (String arg : args) {
				newStr.append(arg).append(" ");
			}
			newStr = new StringBuilder(newStr.toString().replace('~', ' '));
			newStr = new StringBuilder(newStr.toString().replace('@', ' '));
			String channelPrefix = cmd.equals("g") ? "@gr2@[Global] " : "@or1@[PKing] ";
			int channel = cmd.equalsIgnoreCase("g") ? 1 : 2;
			for (Player p : player.getWorld().getPlayers()) {
				if (p.getSocial().isIgnoring(player.getUsernameHash()))
					continue;
				if (p.getGlobalBlock() == 3 && channel == 2) {
					continue;
				}
				if (p.getGlobalBlock() == 4 && channel == 1) {
					continue;
				}
				if (p.getGlobalBlock() != 2) {
					String header = "";
					ActionSender.sendMessage(p, player, 0, MessageType.QUEST, channelPrefix + "@whi@" + player.getUsername() + (player.getClan() != null ? "@cla@<" + player.getClan().getClanTag() + "> @whi@" : "") + header + ": "
						+ (channel == 1 ? "@gr2@" : "@or1@") + newStr, player.getIcon());
				}
			}
			if (cmd.equalsIgnoreCase("g")) {
				player.getWorld().getServer().getGameLogger().addQuery(new ChatLog(player.getWorld(), player.getUsername(), "(Global) " + newStr));
				player.getWorld().addEntryToSnapshots(new Chatlog(player.getUsername(), "(Global) " + newStr));
			} else {
				player.getWorld().getServer().getGameLogger().addQuery(new ChatLog(player.getWorld(), player.getUsername(), "(PKing) " + newStr));
				player.getWorld().addEntryToSnapshots(new Chatlog(player.getUsername(), "(PKing) " + newStr));
			}
		} else if (cmd.equalsIgnoreCase("p")) {
			if (player.isMuted()) {
				player.message(messagePrefix + "You are muted, you cannot send messages");
				return;
			}
			if (player.getCache().hasKey("global_mute") && (player.getCache().getLong("global_mute") - System.currentTimeMillis() > 0 || player.getCache().getLong("global_mute") == -1) && cmd.equals("g")) {
				long globalMuteDelay = player.getCache().getLong("global_mute");
				player.message(messagePrefix + "You are " + (globalMuteDelay == -1 ? "permanently muted" : "temporary muted for " + (int) ((player.getCache().getLong("global_mute") - System.currentTimeMillis()) / 1000 / 60) + " minutes") + " from the ::g chat.");
				return;
			}
			long sayDelay = 0;
			if (player.getCache().hasKey("say_delay")) {
				sayDelay = player.getCache().getLong("say_delay");
			}

			long waitTime = 1200;

			if (player.isMod()) {
				waitTime = 0;
			}

			if (System.currentTimeMillis() - sayDelay < waitTime) {
				player.message(messagePrefix + "You can only use this command every " + (waitTime / 1000) + " seconds");
				return;
			}

			if (player.getLocation().onTutorialIsland() && !player.isMod()) {
				return;
			}
			if (player.getParty() == null) {
				return;
			}

			player.getCache().store("say_delay", System.currentTimeMillis());

			StringBuilder newStr = new StringBuilder();
			for (String arg : args) {
				newStr.append(arg).append(" ");
			}
			newStr = new StringBuilder(newStr.toString().replace('~', ' '));
			newStr = new StringBuilder(newStr.toString().replace('@', ' '));
			String channelPrefix = "@whi@[@or1@Party@whi@] ";
			int channel = cmd.equalsIgnoreCase("p") ? 1 : 2;
			for (Player p : player.getWorld().getPlayers()) {
				if (p.getSocial().isIgnoring(player.getUsernameHash()))
					continue;
				if (p.getParty() == player.getParty()) {
					ActionSender.sendMessage(p, player, 1, MessageType.CLAN_CHAT, channelPrefix + "" + player.getUsername() + ": @or1@" + newStr, player.getIcon());
				}
			}
			if (cmd.equalsIgnoreCase("g")) {
				player.getWorld().getServer().getGameLogger().addQuery(new ChatLog(player.getWorld(), player.getUsername(), "(Global) " + newStr));
				player.getWorld().addEntryToSnapshots(new Chatlog(player.getUsername(), "(Global) " + newStr));
			} else {
				player.getWorld().getServer().getGameLogger().addQuery(new ChatLog(player.getWorld(), player.getUsername(), "(PKing) " + newStr));
				player.getWorld().addEntryToSnapshots(new Chatlog(player.getUsername(), "(PKing) " + newStr));
			}
		} else if (cmd.equalsIgnoreCase("online")) {
			int players = (int) (player.getWorld().getPlayers().size());
			for (Player p : player.getWorld().getPlayers()) {
				if (p.isMod() && p.getSettings().getPrivacySetting(1)) {
					players--;
				}
			}
			player.message(messagePrefix + "Players Online: " + players);
		} else if (cmd.equalsIgnoreCase("tips")) {
			ActionSender.sendBox(player, "Tips % 1. Left or Right click on any stat to bring up stat changing panel % 2. Use the rowboat in lumbridge to get too Edgeville % 3. Earn points by killing npcs,  burying bones, or casting magic spells % 4. You can customize the side menu in General Settings % 5. ::g = global chat, ::p = party chat, ::c = clan chat % 6. To create a PARTY, right click any player in the Online List and press invite % 7. You can save and load stat presets in the stat panel % 8. The MARKET is located in Edgeville % 9. Farmer NPC in Edgeville gives cooked trout for feathers % 10. Sell Points for GP at the Edgeville General Store % 11. A sleeping bag spawns in the Edgeville General Store % 12. Rune items are only obtainable by NPC drops % 13. To compare stats versus another player, right click the player in the Online List and press @yel@compare@whi@ % 14. Track xp/hr with the Experience Counter %", true);
		} else if (cmd.equalsIgnoreCase("tdmgo")) {
			player.getWorld().setTdmInProgress(2);
			for (Player p : player.getWorld().getPlayers()) {
				ActionSender.sendTdmInProgress(p);
			}
		} else if (cmd.equalsIgnoreCase("uniqueonline")) {
			ArrayList<String> IP_ADDRESSES = new ArrayList<>();
			for (Player p : player.getWorld().getPlayers()) {
				if (!IP_ADDRESSES.contains(p.getCurrentIP()))
					IP_ADDRESSES.add(p.getCurrentIP());
			}
			player.message(messagePrefix + "There are " + IP_ADDRESSES.size() + " unique players online");
		} else if (cmd.equalsIgnoreCase("leaveparty")) {
			player.getParty().removePlayer(player.getUsername());
		} else if (cmd.equalsIgnoreCase("leaveclan")) {
			player.getClan().removePlayer(player.getUsername());
		} else if (cmd.equalsIgnoreCase("joinclan")) {
			String clanToJoin = args[0].replace("_", " ");
			if (player.getWorld().getClanManager().getClan(clanToJoin) != null) {
				if (player.getWorld().getClanManager().getClan(clanToJoin).getAllowSearchJoin() == 0) {
					ClanInvite.createClanJoinRequest(player.getWorld().getClanManager().getClan(clanToJoin), player);
				} else {
					player.message(messagePrefix + "This clan is not accepting join requests");
				}
			} else {
				player.message(messagePrefix + "this");
			}
		} else if (cmd.equalsIgnoreCase("shareloot")) {
			if (player.getParty().getPlayer(player.getUsername()).getRank().equals(PartyRank.LEADER)) {
				for (PartyPlayer m : player.getParty().getPlayers()) {
					if (m.getShareLoot() > 0) {
						m.setShareLoot(0);
						m.getPlayerReference().message("@whi@[@blu@Party@whi@] - @whi@Loot Sharing has been @red@Disabled");
						ActionSender.sendParty(m.getPlayerReference());
					} else {
						m.setShareLoot(1);
						ActionSender.sendParty(m.getPlayerReference());
						m.getPlayerReference().message("@whi@[@blu@Party@whi@] - @whi@Loot Sharing has been @gre@Enabled");

					}
				}
			}
		} /*else if (cmd.equalsIgnoreCase("shareexp")) {
			if (player.getParty().getPlayer(player.getUsername()).getRank().equals(PartyRank.LEADER)) {
				for (PartyPlayer m : player.getParty().getPlayers()) {
					if (m.getShareExp() > 0) {
						m.setShareExp(0);
						m.getPlayerReference().message("@whi@[@blu@Party@whi@] - @whi@Exp Sharing has been @red@Disabled");
						ActionSender.sendParty(m.getPlayerReference());
					} else {
						m.setShareExp(1);
						ActionSender.sendParty(m.getPlayerReference());
						m.getPlayerReference().message("@whi@[@blu@Party@whi@] - @whi@Exp Sharing has been @gre@Enabled");

					}
				}
			}
		} */ 
		else if (cmd.equalsIgnoreCase("u2")) {
			try {
				pkp1(player);
			} catch (SQLException e) {
				LOGGER.catching(e);
			}
		} else if (cmd.equalsIgnoreCase("u3")) {
			try {
				cp1(player);
			} catch (SQLException e) {
				LOGGER.catching(e);
			}
		} else if (cmd.equals("wildernesslist")) {
			iw.clear();
			for (Player p : player.getWorld().getPlayers()) {
				if (p.getLocation().inWilderness()) {
					int i = p.getLocation().wildernessLevel();
					int i2 = p.getCombatLevel();
					iw.add(p.getUsername() + "[@yel@Lvl" + i2 + "@whi@] - @red@Wild Lvl" + i + "@whi@");
				}
			}
			ActionSender.sendBox(player, "" + "Players currently in the wilderness % " + iw, true);
		} else if (cmd.equals("onlinelist")) { // modern onlinelist display using ActionSender.SendOnlineList()
			ActionSender.sendOnlineList(player);
		} else if (cmd.equals("jointdm")) {
			if(player.getLocation().inWilderness() || player.inCombat()) {
				player.message("You cannot do this in combat or while in the wilderness.");
				return;
			}
			if(player.getCombatLevel() != player.getWorld().getTdmCbLvl()) {
				player.message("You must be lvl " + player.getWorld().getTdmCbLvl() + " to join this TDM");
				return;
			}
			if(player.getWorld().getTdmInProgress() == 0) {
				player.message("There is no open TDM queue");
				return;
			}
			if(player.getWorld().getTdmInProgress() == 2) {
				player.message("There currently is a live TDM match ongoing. Please wait until it finishes.");
				return;
			}
			if(player.getWorld().getTdmTeam1Size() == player.getWorld().getTdmTeam2Size()){
				player.getWorld().setTdmTeam1Size(player.getWorld().getTdmTeam1Size() + 1);
				player.setTdmTeam(1);
				ActionSender.sendTdmTeam(player);
			} else
			if(player.getWorld().getTdmTeam1Size() > player.getWorld().getTdmTeam2Size()){
				player.getWorld().setTdmTeam2Size(player.getWorld().getTdmTeam2Size() + 1);
				player.setTdmTeam(2);
				ActionSender.sendTdmTeam(player);
			} else
			if(player.getWorld().getTdmTeam2Size() > player.getWorld().getTdmTeam1Size()){
				player.getWorld().setTdmTeam1Size(player.getWorld().getTdmTeam1Size() + 1);
				player.setTdmTeam(1);
				ActionSender.sendTdmTeam(player);
			}
			for (Player p : player.getWorld().getPlayers()) {
				ActionSender.sendTdmTeam1Size(p);
				ActionSender.sendTdmTeam2Size(p);
			}
			Point tdm = new Point(DataConversions.random(105, 119), DataConversions.random(9, 24));
			player.summon(tdm);
			player.setInTdm(1);
			ActionSender.sendInTdm(player);
			ActionSender.sendBox(player, "Gear up now. You will NOT be able to access bank when TDM starts.", false);
			if(player.getWorld().getTdmInProgress() == 1) {
				player.setAccessingBank(true);
				ActionSender.showBank(player);
			}
		} else if (cmd.equals("createtdm")) {
			if(player.getLocation().inWilderness() || player.inCombat()) {
				player.message("You cannot do this in combat or while in the wilderness.");
				return;
			}
			if(player.getCombatLevel() != player.getWorld().getTdmCbLvl()) {
				player.message("You must be lvl " + player.getWorld().getTdmCbLvl() + " to start this TDM");
				return;
			}
			if(player.getWorld().getTdmInProgress() > 0) {
				if(player.getWorld().getTdmInProgress() == 1) {
					player.message("There already is an open TDM queue. Type ::jointdm to join");
					return;
				} else
				if(player.getWorld().getTdmInProgress() == 2) {
					player.message("There currently is a live TDM match ongoing. Please wait until it finishes.");
					return;
				}
			} else {
				if(player.getWorld().getTdmTeam1Size() == player.getWorld().getTdmTeam2Size()){
					player.getWorld().setTdmTeam1Size(player.getWorld().getTdmTeam1Size() + 1);
					player.setTdmTeam(1);
					ActionSender.sendTdmTeam(player);
				} else
				if(player.getWorld().getTdmTeam1Size() > player.getWorld().getTdmTeam2Size()){
					player.getWorld().setTdmTeam2Size(player.getWorld().getTdmTeam2Size() + 1);
					player.setTdmTeam(2);
					ActionSender.sendTdmTeam(player);
				} else
				if(player.getWorld().getTdmTeam2Size() > player.getWorld().getTdmTeam1Size()){
					player.getWorld().setTdmTeam1Size(player.getWorld().getTdmTeam1Size() + 1);
					player.setTdmTeam(1);
					ActionSender.sendTdmTeam(player);
				}
				Point tdm = new Point(DataConversions.random(105, 119), DataConversions.random(9, 24));
				player.summon(tdm);
				player.getWorld().setTdmInProgress(1);
				player.getWorld().setTdmQueueTimer();
				player.setInTdm(1);
				ActionSender.sendInTdm(player);
				ActionSender.sendBox(player, "Gear up now. You will NOT be able to access bank when TDM starts.", false);
				if(player.getWorld().getTdmInProgress() == 1) {
					player.setAccessingBank(true);
					ActionSender.showBank(player);
				}
				for (Player p : player.getWorld().getPlayers()) {
					ActionSender.sendTdmInProgress(p);
					ActionSender.sendTdmTeam1Size(p);
					ActionSender.sendTdmTeam2Size(p);
					if (player.getWorld().getServer().tdmQueueEventUpdate(0)) {
						ActionSender.startTdmQueue(p, 300);
					} else {
						ActionSender.startTdmQueue(p, 300);
					}
					p.message("TDM queue is open. lvl " + player.getWorld().getTdmCbLvl() + ". " + player.getWorld().getTdmKillLimit() + " kills to win.");
				}
			}
		} else if (cmd.equals("pkpointlist")) {
			sm.clear();
			for (Player p : player.getWorld().getPlayers()) {
				long l = p.getPkPoints();
				int i = (int) l;
				if(i > 0) {
					sm.put(i, p.getUsername());
				}
			}
			Set s = sm.entrySet();
			Iterator i = s.iterator();
			/*while (i.hasNext()) {
				Map.Entry m = (Map.Entry)i.next();
				int key = (Integer)m.getKey();
				String value = (String)m.getValue();	*/
			ActionSender.sendBox(player, "" + "Online Player PK Point Leaderboards % " + sm, true);
			//}
		} else if (cmd.equalsIgnoreCase("groups") || cmd.equalsIgnoreCase("ranks")) {
			ArrayList<String> groups = new ArrayList<>();
			for (HashMap.Entry<Integer, String> entry : Group.GROUP_NAMES.entrySet()) {
				groups.add(Group.getStaffPrefix(player.getWorld(), entry.getKey()) + entry.getValue() + (player.isDev() ? " (" + entry.getKey() + ")" : ""));
			}
			ActionSender.sendBox(player, "@whi@Server Groups:%" + StringUtils.join(groups, "%"), true);
		} else if (cmd.equalsIgnoreCase("wantloginbox")) {
			if(player.getWantLoginBox() < 1) {
				player.setWantLoginBox(1);
				ActionSender.sendWantLoginBox(player);
			} else
			{
				player.setWantLoginBox(0);
				ActionSender.sendWantLoginBox(player);
			}
		} else if (cmd.equalsIgnoreCase("wantogcombatstylebox")) {
			if(player.getWantOgCombatStyleBox() < 1) {
				player.setWantOgCombatStyleBox(1);
				ActionSender.sendWantOgCombatStyleBox(player);
			} else
			{
				player.setWantOgCombatStyleBox(0);
				ActionSender.sendWantOgCombatStyleBox(player);
			}
		} else if (cmd.equalsIgnoreCase("time") || cmd.equalsIgnoreCase("date") || cmd.equalsIgnoreCase("datetime")) {
			player.message(messagePrefix + " the current time/date is:@gre@ " + new java.util.Date().toString());
		} else if (cmd.equalsIgnoreCase("killfeed")) {
			ActionSender.sendBox(player, "" + "" + player.getWorld().getPvpKills(), true);
		} else if (player.getWorld().getServer().getConfig().NPC_KILL_LIST && cmd.equalsIgnoreCase("kills")) {
			StringBuilder kills = new StringBuilder("NPC Kill List for " + player.getUsername() + " % %");
			//PreparedStatement statement = player.getWorld().getServer().getDatabaseConnection().prepareStatement(
			//	"SELECT * FROM `" + player.getWorld().getServer().getConfig().MYSQL_TABLE_PREFIX + "npckills` WHERE playerID = ? ORDER BY killCount DESC LIMIT 16");
			//statement.setInt(1, player.getDatabaseID());
			//ResultSet result = statement.executeQuery();
			for (Map.Entry<Integer, Integer> entry : player.getKillCache().entrySet()) {
				kills.append("NPC: ").append(player.getWorld().getServer().getEntityHandler().getNpcDef(entry.getKey()).getName()).append(" - Kill Count: ").append(entry.getValue()).append("%");
			}
			ActionSender.sendBox(player, kills.toString(), true);
		} else if (cmd.equalsIgnoreCase("compare")) {
			if (args.length < 1) {
				player.message(badSyntaxPrefix + cmd.toUpperCase() + " [name]");
				return;
			}

			Player p1 = player.getWorld().getPlayer(DataConversions.usernameToHash(args[0]));
			if(p1 == null) {
				player.message("This player is not currently online");
				return;
			}
			if(p1 == player) {
				player.message("You can't compare with yourself");
				return;
			}
			player.countPvpKills(player, p1);
			player.countPvpDeaths(player, p1);
		} else if (cmd.equalsIgnoreCase("pair")) {
			if (player.getCache().hasKey("discordID")) {
				player.message("Your account is already paired. Please message a mod on discord to unpair.");
			} else {
				if (player.getCache().hasKey("pair_token")) {
					player.message("Your pair token is: " + player.getCache().getString("pair_token"));
				} else {
					Random rand = new Random();
					int tokenLength = 10;
					StringBuilder builder = new StringBuilder(tokenLength);
					for (int i = 0; i < tokenLength; i++) {
						boolean isCharacter = (rand.nextInt(2)) == 1;
						if (isCharacter) {
							boolean isCaps = (rand.nextInt(2)) == 1;
							if (isCaps) {
								builder.append((char) ((rand.nextInt(26)) + 65));
							} else {
								builder.append((char) ((rand.nextInt(26)) + 97));
							}
						} else {
							builder.append((char) ((rand.nextInt(10)) + 48));
						}
					}
					try {
						PreparedStatement pinStatement = player.getWorld().getServer().getDatabaseConnection().prepareStatement("INSERT INTO `" + player.getWorld().getServer().getConfig().MYSQL_TABLE_PREFIX + "player_cache`(`playerID`, `type`, `key`, `value`) VALUES(?, ?, ?, ?)");
						pinStatement.setInt(1, player.getDatabaseID());
						pinStatement.setInt(2, 1);
						pinStatement.setString(3, "pair_token");
						pinStatement.setString(4, builder.toString());
						pinStatement.executeUpdate();

						player.getCache().store("pair_token", builder.toString());
					} catch (SQLException a) {
						a.printStackTrace();
					}

					player.message("Your pair token is: " + builder.toString());
				}
			}
		} else if (cmd.equalsIgnoreCase("d")) {
			String message = String.join(" ", args);
			player.getWorld().getServer().getDiscordService().sendMessage("[InGame] " + player.getUsername() + ": " + message);

			for (Player p : player.getWorld().getPlayers()) {
				ActionSender.sendMessage(p, null, 0, MessageType.GLOBAL_CHAT, "@whi@[@gr2@G>D@whi@] @or1@" + player.getUsername() + "@yel@: " + message, 0);
			}
		} else if (cmd.equalsIgnoreCase("commands")) {
			ActionSender.sendBox(player, ""
				+ "@yel@Commands available: %"
				+ "Type :: before you enter your command, see the list below. % %"
				+ "@whi@::online - shows players currently online %"
				+ "@whi@::uniqueonline - shows number of unique IPs logged in %"
				+ "@whi@::onlinelist - shows players currently online in a list %"
				+ "@whi@::g <message> - to talk in @gr1@general @whi@global chat channel %"
				+ "@whi@::p <message> - to talk in @or1@pking @whi@global chat channel %"
				+ "@whi@::c <message> - talk in clan chat %"
				+ "@whi@::claninvite <name> - invite player to clan %"
				+ "@whi@::clankick <name> - kick player from clan %"
				+ "@whi@::clanaccept - accept clan invitation %"
				+ "@whi@::groups - shows available ranks on the server %"
				+ "@whi@::wilderness - shows the wilderness activity %"
				+ "@whi@::time - shows the current server time %", true
			);
		}
	}
}
