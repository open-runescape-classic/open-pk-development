package com.openrsc.server.plugins.defaults;

import com.openrsc.server.constants.ItemId;
import com.openrsc.server.constants.NpcId;
import com.openrsc.server.constants.Quests;
import com.openrsc.server.constants.Skills;
import com.openrsc.server.event.ShortEvent;
import com.openrsc.server.model.container.Item;
import com.openrsc.server.model.entity.GameObject;
import com.openrsc.server.model.entity.npc.Npc;
import com.openrsc.server.model.entity.player.Player;
import com.openrsc.server.util.rsc.DataConversions;
import com.openrsc.server.util.rsc.MessageType;

import static com.openrsc.server.plugins.Functions.*;

/**
 * Does default action to unhandled(non-quest related) doors.
 *
 * @author n0m
 */
public class DoorAction {

	public boolean blockWallObjectAction(final GameObject obj,
										 final Integer click, final Player player) {

		if (obj.getDoorDef().name.toLowerCase().contains("door")
			|| obj.getDoorDef().name.equalsIgnoreCase("door")
			|| obj.getDoorDef().name.equalsIgnoreCase("odd looking wall")
			|| obj.getDoorDef().name.equalsIgnoreCase("doorframe")) {
			return true;
		}

		// Tutorial Doors
		if (obj.getID() == 75 && obj.getX() == 222 && obj.getY() == 743) {
			return true;
		} else if (obj.getID() == 76 && obj.getX() == 224 && obj.getY() == 737) {
			return true;
		} else if (obj.getID() == 77 && obj.getX() == 220 && obj.getY() == 727) {
			return true;
		} else if (obj.getID() == 78 && obj.getX() == 212 && obj.getY() == 729) {
			return true;
		} else if (obj.getID() == 80 && obj.getX() == 206 && obj.getY() == 730) {
			return true;
		} else if (obj.getID() == 81 && obj.getX() == 201 && obj.getY() == 734) {
			return true;
		} else if (obj.getID() == 82 && obj.getX() == 198 && obj.getY() == 746) {
			return true;
		} else if (obj.getID() == 83 && obj.getX() == 204 && obj.getY() == 752) {
			return true;
		} else if (obj.getID() == 84 && obj.getX() == 209 && obj.getY() == 754) {
			return true;
		} else if (obj.getID() == 85 && obj.getX() == 217 && obj.getY() == 760) {
			return true;
		} else if (obj.getID() == 88 && obj.getX() == 222 && obj.getY() == 760) {
			return true;
		} else if (obj.getID() == 89 && obj.getX() == 226 && obj.getY() == 760) {
			return true;
		} else if (obj.getID() == 90 && obj.getX() == 230 && obj.getY() == 759) {
			return true;
		}

		return false;
	}

	public void onWallObjectAction(final GameObject obj, final Integer click,
								   final Player p) {

		// Door's lock needs to be picked, or needs a key.
		if (blockInvUseOnWallObject(obj, null, p)) {
			p.message("The door is locked");
			return;
		}

		// Tutorial Doors
		if (obj.getID() == 75 && obj.getX() == 222 && obj.getY() == 743) {
			if (p.getCache().hasKey("tutorial")
				&& p.getCache().getInt("tutorial") >= 10) {
				doDoor(obj, p);
			} else {
				p.message("You should speak to a guide before going through this door");
			}
		} else if (obj.getID() == 76 && obj.getX() == 224 && obj.getY() == 737) {
			if (p.getCache().hasKey("tutorial")
				&& p.getCache().getInt("tutorial") >= 15) {
				doDoor(obj, p);
			} else {
				p.message("Speak to the controls guide before going through this door");
			}
		} else if (obj.getID() == 77 && obj.getX() == 220 && obj.getY() == 727) {
			if (p.getCache().hasKey("tutorial")
				&& p.getCache().getInt("tutorial") >= 25) {
				doDoor(obj, p);
			} else {
				p.message("Speak to the combat instructor before going through this door");
			}
		} else if (obj.getID() == 78 && obj.getX() == 212 && obj.getY() == 729) {
			if (p.getCache().hasKey("tutorial")
				&& p.getCache().getInt("tutorial") >= 35) {
				doDoor(obj, p);
			} else {
				p.message("You should speak to a cooking instructor before going through this door");
			}
		} else if (obj.getID() == 80 && obj.getX() == 206 && obj.getY() == 730) {
			if (p.getCache().hasKey("tutorial")
				&& p.getCache().getInt("tutorial") >= 40) {
				doDoor(obj, p);
			} else {
				p.message("You should speak to a finance advisor before going through this door");
			}
		} else if (obj.getID() == 81 && obj.getX() == 201 && obj.getY() == 734) {
			if (p.getCache().hasKey("tutorial")
				&& p.getCache().getInt("tutorial") >= 45) {
				doDoor(obj, p);
			} else {
				p.message("You should speak to the fishing instructor before going through this door");
			}
		} else if (obj.getID() == 82 && obj.getX() == 198 && obj.getY() == 746) {
			if (p.getCache().hasKey("tutorial")
				&& p.getCache().getInt("tutorial") >= 55) {
				doDoor(obj, p);
			} else {
				p.message("You should speak to the mining instructor before going through this door");
			}
		} else if (obj.getID() == 83 && obj.getX() == 204 && obj.getY() == 752) {
			if (p.getCache().hasKey("tutorial")
				&& p.getCache().getInt("tutorial") >= 60) {
				doDoor(obj, p);
			} else {
				p.message("You should speak to a bank assistant before going through this door");
			}
		} else if (obj.getID() == 84 && obj.getX() == 209 && obj.getY() == 754) {
			if (p.getCache().hasKey("tutorial")
				&& p.getCache().getInt("tutorial") >= 65) {
				doDoor(obj, p);
			} else {
				p.message("You should speak to the quest advisor before going through this door");
			}
		} else if (obj.getID() == 85 && obj.getX() == 217 && obj.getY() == 760) {
			if (p.getCache().hasKey("tutorial")
				&& p.getCache().getInt("tutorial") >= 70) {
				doDoor(obj, p);
			} else {
				p.message("You should speak to the wilderness guide before going through this door");
			}
		} else if (obj.getID() == 88 && obj.getX() == 222 && obj.getY() == 760) {
			if (p.getCache().hasKey("tutorial")
				&& p.getCache().getInt("tutorial") >= 80) {
				doDoor(obj, p);
			} else {
				p.message("You should speak to a magic instructor before going through this door");
			}
		} else if (obj.getID() == 89 && obj.getX() == 226 && obj.getY() == 760) {
			if (p.getCache().hasKey("tutorial")
				&& p.getCache().getInt("tutorial") >= 90) {
				doDoor(obj, p);
			} else {
				p.message("You should speak to a fatigue expert before going through this door");
			}
		} else if (obj.getID() == 90 && obj.getX() == 230 && obj.getY() == 759) {
			if (p.getCache().hasKey("tutorial")
				&& p.getCache().getInt("tutorial") >= 100) {
				doDoor(obj, p);
			} else {
				p.message("You should speak to the community instructor before going through this door");
			}
		}
		
		if (obj.getID() == 99 && obj.getX() == 263 && obj.getY() == 104) {
				doDoor(obj, p);
		}
		if (obj.getID() == 99 && obj.getX() == 266 && obj.getY() == 100) {
				doDoor(obj, p);
		}
		if (obj.getID() == 99 && obj.getX() == 270 && obj.getY() == 104) {
				doDoor(obj, p);
		}
		
		if (obj.getID() == 99 && obj.getX() == 160 && obj.getY() == 108) {
				doDoor(obj, p);
		}
		
		if (obj.getID() == 99 && obj.getX() == 160 && obj.getY() == 103) {
				doDoor(obj, p);
		}


		switch (obj.getID()) {
			case 20:
				replaceGameObject(obj, p, 1, true);
				break;

			case 1:
				replaceGameObject(obj, p, 2, false);
				break;

			case 2:
				replaceGameObject(obj, p, 1, true);
				break;

			case 9:
				replaceGameObject(obj, p, 8, false);
				break;

			case 8:
				replaceGameObject(obj, p, 9, true);
				break;

			case 94:
			case 23:
				p.message("The door is locked");
				break;

			case 150: // Fight Arena: Ogre Cage (?)
				doDoor(obj, p);
				break;
		}
	}

	public boolean blockInvUseOnWallObject(GameObject obj, Item item,
										   Player player) {

		/* Ernest the Chicken */
		if (obj.getID() == 35 && obj.getY() == 545) {
			return true;
		}
		if (obj.getID() == 110) {
			return true;
		}
		/* Dragon Slayer Maze Doors */
		if (obj.getID() >= 48 && obj.getID() <= 53 || obj.getID() == 60) {
			return true;
		}
		/* Shield of arrav */
		if (obj.getID() == 20) {
			return true;
		}
		/* witches house door */
		if (obj.getID() == 69) {
			return true;
		}
		/* varrocks shortcut to edgeville dung*/
		if (obj.getID() == 23) {
			return true;
		}
		/* taverly dungeon door near jailer */
		if ((obj.getID() == 83 && obj.getX() == 360 && obj.getY() == 3428)
			|| (obj.getID() == 83 && obj.getX() == 360
			&& obj.getY() == 3425)) {
			return true;
		}
		/* Door to enter blue dragons in taverly dungeon */
		if (obj.getID() == 84 && obj.getX() == 355 && obj.getY() == 3353) {
			return true;
		}

		return false;
	}

	public boolean blockObjectAction(GameObject obj, String command,
									 Player player) {
		if (obj.getGameObjectDef().getName().toLowerCase().contains("gate")
			|| obj.getGameObjectDef().getName().toLowerCase().contains("door")) {
			if (command.equalsIgnoreCase("close")
				|| command.equalsIgnoreCase("open")) {
				return true;
			}
		}
		return false;
	}

	public void onObjectAction(GameObject obj, String command, Player player) {
		if (obj.getGameObjectDef().getName().toLowerCase().contains("gate")) {
			handleGates(obj, player);
		} else if (obj.getGameObjectDef().getName().toLowerCase().contains("door")) {
			handleObjectDoor(obj, player);
		}
	}

	private void handleObjectDoor(GameObject obj, Player player) {

		if (blockInvUseOnWallObject(obj, null, player)) {
			player.message("the door is locked");
			return;
		}

		switch (obj.getID()) {
			case 142: // Black Knight Big Door
				player.message("the doors are locked");
				break;
			/* Regular Doors */
			case 18:
				replaceGameObject(17, true, player, obj);
				break;
			case 17:
				replaceGameObject(18, false, player, obj);
				break;
			case 58:
				replaceGameObject(57, false, player, obj);
				break;

			case 57: // Brimhaven Gate (434, 682)
				if (obj.getX() == 434 && obj.getY() == 682) {
					if (!player.getWorld().getServer().getConfig().MEMBER_WORLD) {
						player.message(
							"You need to be a member to use this gate");
						return;
					}
				}
				replaceGameObject(58, true, player, obj);
				break;

			case 63:
				replaceGameObject(64, false, player, obj);
				break;

			case 64:
				if (obj.getX() == 467 && obj.getY() == 518) {
					player.message("The doors are locked");
					break;
				} else if (obj.getX() == 558 && obj.getY() == 587) {
					player.message("The doors are locked");
					break;
				} else {
					replaceGameObject(63, true, player, obj);
				}
				break;

			case 79:
				replaceGameObject(78, false, player, obj);
				break;

			case 78:
				replaceGameObject(79, true, player, obj);
				break;

			case 135:
				replaceGameObject(136, true, player, obj);
				break;

			case 136:
				replaceGameObject(135, false, player, obj);
				break;

			default:
				player.message(
					"Nothing interesting happens");
		}
	}

	private void handleGates(GameObject obj, Player player) {
		boolean members = false;

		switch (obj.getID()) {
			case 60:
				replaceGameObject(59, true, player, obj);
				return;

			case 59:
				replaceGameObject(60, false, player, obj);
				return;

			case 57:
				replaceGameObject(obj, player, 58, true);
				return;

			case 58:
				replaceGameObject(obj, player, 57, false);
				return;

			case 513: // eastern Varrock gate for family crest or biohazard or just wanna go in :)
				if (obj.getX() != 93 || obj.getY() != 521) {
					return;
				}
				if (!player.getWorld().getServer().getConfig().MEMBER_WORLD) {
					player.message(
						"You need to be a member to use this gate");
					return;
				}
				if (player.getX() >= 94) {
					player.teleport(92, 522, false);
				} else {
					doGate(player, obj, 514);
				}
				player.message("you open the gate and pass through");
				return;

			case 93: // Red dragon gate (140, 180)
				members = true;
				break;

			case 137: // Members Gate near Doric (341, 487)
				members = true;
				break;

			case 347: //
				members = true;
				break;

			case 138: // Members Gate near Crafting Guild (343, 581)
				members = true;
				break;

			case 254: // Members Gate near Brimhaven (434, 682)
				members = true;
				break;

			case 305: // Members Gate in Edgeville Dungeon (196, 1266)
				members = true;
				break;

			case 1089: // Members Gate near Dig Site (59, 573)
				members = true;
				break;

			case 508: // Lesser Cage Gate
				members = false;
				break;

			case 319:
				// Members Gate near King Black Dragon ladder (285, 185)
				members = true;
				break;

			default:
				player.message(
					"Nothing interesting happens");

				return;
		}
		if (members && !player.getWorld().getServer().getConfig().MEMBER_WORLD) {
			player.sendMemberErrorMessage();
			return;
		}
		player.message("you go through the gate");
		doGate(player, obj);
		/*if(player.getY() >= 141 && obj.getID() == 347) { // Not authentic
			// Unwield so they cannot be stationary with it
			player.unwieldMembersItems();
		}*/
	}

	private void replaceGameObject(final int newID, final boolean open,
								   final Player p, final GameObject object) {
		p.getWorld().replaceGameObject(object,
			new GameObject(object.getWorld(), object.getLocation(), newID, object
				.getDirection(), object.getType()));
		p.playSound(open ? "opendoor" : "closedoor");
	}

	private void replaceGameObject(GameObject obj, Player owner, int newID,
								   boolean open) {
		if (!owner.cantConsume()) {
			owner.setConsumeTimer(1000);
			if (open) {
				owner.message("The " + (obj.getGameObjectDef().getName().equalsIgnoreCase("gate") ? "gate" : "door") + " swings open");
			} else {
				owner.message("The " + (obj.getGameObjectDef().getName().equalsIgnoreCase("gate") ? "gate" : "door") + " creaks shut");
				owner.setBusyTimer(650);
			}
			owner.playSound(open ? "opendoor" : "closedoor");
			owner.getWorld().replaceGameObject(obj, new GameObject(obj.getWorld(), obj.getLocation(), newID, obj.getDirection(), obj.getType()));
		}
	}
}
