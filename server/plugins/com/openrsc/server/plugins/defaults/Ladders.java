package com.openrsc.server.plugins.defaults;

import com.openrsc.server.constants.ItemId;
import com.openrsc.server.constants.NpcId;
import com.openrsc.server.constants.Skills;
import com.openrsc.server.event.ShortEvent;
import com.openrsc.server.model.TelePoint;
import com.openrsc.server.model.entity.GameObject;
import com.openrsc.server.model.entity.npc.Npc;
import com.openrsc.server.model.entity.player.Player;
import com.openrsc.server.net.rsc.ActionSender;
import com.openrsc.server.util.rsc.DataConversions;
import com.openrsc.server.util.rsc.Formulae;
import com.openrsc.server.util.rsc.MessageType;

import static com.openrsc.server.plugins.Functions.*;


public class Ladders {

	public boolean blockObjectAction(GameObject obj, String command,
									 Player player) {
		return (command.equals("climb-down") || command.equals("go down") || command
			.equals("climb down"))
			|| command.equals("climb-up")
			|| command.equals("go up")
			|| command.equals("pull");
	}

	public void onObjectAction(GameObject obj, String command, Player player) {
		player.setBusyTimer(650);
		if (obj.getID() == 487 && !player.getWorld().getServer().getConfig().MEMBER_WORLD) {
			player.message(player.MEMBER_MESSAGE);
			return;
		} else if (obj.getID() == 79 && obj.getX() == 243 && obj.getY() == 95) {
			player.message("Are you sure you want to go down to this lair?");
			int menu = showMenu(player, "Yes I take the risk!", "No stay up here.");
			if (menu == 0) {
				player.message("You climb down the manhole and land in a water lair");
				player.teleport(98, 2931);
			} else if (menu == 1) {
				player.message("You decide to stay.");
			}
			//player.message("The new dungeon is available in a couple of minutes");
			//player.message("We are doing the decoration, please stay tuned.");
			return;
		} else if (obj.getID() == 5 && (obj.getX() == 98 && obj.getY() == 2930 || obj.getX() == 137 && obj.getY() == 2932)) {
			player.teleport(243, 96);
			player.message("You climb up the ladder");
			return;
		} else if (obj.getID() == 629) {
			player.teleport(576, 3580);
			player.message("You go up the stairs");
			return;
		} else if (obj.getID() == 621) {
			player.teleport(606, 3556);
			player.message("You go up the stairs");
			return;
		}
		TelePoint telePoint = player.getWorld().getServer().getEntityHandler().getObjectTelePoint(obj
			.getLocation(), command);
		if (telePoint != null) {
			player.teleport(telePoint.getX(), telePoint.getY(), false);
		} else if (obj.getID() == 487) {
			player.message("You pull the lever");
			player.teleport(567, 3330);
			sleep(600);
			if (player.getX() == 567 && player.getY() == 3330) {
				displayTeleportBubble(player, player.getX(), player.getY(), false);
			}
		} else if (obj.getID() == 488) {
			player.message("You pull the lever");
			player.teleport(282, 3019);
			sleep(600);
			if (player.getX() == 282 && player.getY() == 3019) {
				displayTeleportBubble(player, player.getX(), player.getY(), false);
			}
		} else if (obj.getID() == 349) {
			player.playerServerMessage(MessageType.QUEST, "Nothing interesting happens");
		} else if (obj.getID() == 348) {
			player.playerServerMessage(MessageType.QUEST, "Nothing interesting happens");
		} else if (obj.getID() == 776) {
			player.playerServerMessage(MessageType.QUEST, "Nothing interesting happens");
		} else if (obj.getID() == 223 && obj.getX() == 312 && obj.getY() == 3348) { // ladder to black hole
			if (!hasItem(player, ItemId.DISK_OF_RETURNING.id())) {
				message(player, "you seem to be missing a disk to use the ladder");
			} else {
				message(player, 1200, "You climb down the ladder");
				int offX = DataConversions.random(0,4) - 2;
				int offY = DataConversions.random(0,4) - 2;
				player.teleport(305 + offX, 3300 + offY);
				ActionSender.sendPlayerOnBlackHole(player);
			}
		} else if (obj.getID() == 342 && obj.getX() == 611 && obj.getY() == 601) {
			player.playerServerMessage(MessageType.QUEST, "Nothing interesting happens");
		} else if (obj.getID() == 249 && obj.getX() == 98 && obj.getY() == 3537) { // lost city (Zanaris) ladder
			player.playerServerMessage(MessageType.QUEST, "Nothing interesting happens");
		} else if (obj.getID() == 1187 && obj.getX() == 446 && obj.getY() == 3367) {
			player.teleport(222, 110, false);
		} else if (obj.getID() == 331 && obj.getX() == 150 && obj.getY() == 558) {
			player.teleport(151, 1505, false);
		} else if (obj.getID() == 6 && obj.getX() == 282 && obj.getY() == 185 && !player.getWorld().getServer().getConfig().MEMBER_WORLD) {
			player.message(player.MEMBER_MESSAGE);
		} else if (obj.getID() == 6 && obj.getX() == 148 && obj.getY() == 1507) {
			player.teleport(148, 563, false);
		} else if (obj.getID() == 630 && obj.getX() == 576 && obj.getY() == 3577) {
			// TODO ????
		} else if (command.equals("climb-up") || command.equals("climb up")
			|| command.equals("go up")) {
			int[] coords = coordModifier(player, true, obj);
			player.teleport(coords[0], coords[1], false);
			player.message(
				"You " + command.replace("-", " ") + " the "
					+ obj.getGameObjectDef().getName().toLowerCase());
		} else if (command.equals("climb-down") || command.equals("climb down")
			|| command.equals("go down")) {
			int[] coords = coordModifier(player, false, obj);
			player.teleport(coords[0], coords[1], false);
			player.message(
				"You " + command.replace("-", " ") + " the "
					+ obj.getGameObjectDef().getName().toLowerCase());
		}
	}

	private int[] coordModifier(Player player, boolean up, GameObject object) {
		if (object.getGameObjectDef().getHeight() <= 1) {
			return new int[]{player.getX(),
				Formulae.getNewY(player.getY(), up)};
		}
		int[] coords = {object.getX(), Formulae.getNewY(object.getY(), up)};
		switch (object.getDirection()) {
			case 0:
				coords[1] -= (up ? -object.getGameObjectDef().getHeight() : 1);
				break;
			case 2:
				coords[0] -= (up ? -object.getGameObjectDef().getHeight() : 1);
				break;
			case 4:
				coords[1] += (up ? -1 : object.getGameObjectDef().getHeight());
				break;
			case 6:
				coords[0] += (up ? -1 : object.getGameObjectDef().getHeight());
				break;
		}
		return coords;
	}

}
