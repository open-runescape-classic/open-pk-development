package com.openrsc.server.plugins.misc;

import com.openrsc.server.model.entity.GameObject;
import com.openrsc.server.model.entity.player.Player;
import com.openrsc.server.plugins.listeners.action.ObjectActionListener;
import com.openrsc.server.plugins.listeners.executive.ObjectActionExecutiveListener;

import static com.openrsc.server.plugins.Functions.*;

public class MagicalPool implements ObjectActionListener, ObjectActionExecutiveListener {

	@Override
	public boolean blockObjectAction(GameObject obj, String command, Player player) {
		return obj.getID() == 1166 || obj.getID() == 1155;
	}

	@Override
	public void onObjectAction(GameObject obj, String command, Player player) {
		if (obj.getID() == 1155) {
			
			if (!player.canUsePool()) {
				player.message("You have just died, you must wait for "
										+ player.secondsUntillPool()
										+ " seconds before using this pool again");
				return;
			}
			while (System.currentTimeMillis()
					- player.getLastMoved() < 10000
					&& player.getLocation().inWilderness()) {
				player.message("You must stand still for 10 seconds before using portal");
				return;
			}
			while (System.currentTimeMillis()
					- player.getCombatTimer() < 10000
					&& player.getLocation().inWilderness()) {
				player.message("You must be out of combat for 10 seconds before using portal");
				return;
			}
			int option = showMenu(player, "Lumbridge", "Edgeville", "Mage Bank", "@or2@Castle", "@or2@Greater Demons/Red Dragon Isle", "@or2@Animated Axe Hut", "@or2@Hobgoblins", "@or1@Lvl 48 Wild Altar", "@or1@Lvl 41 Wild Lesser Demons");
			
			if (option == 0) {
				player.teleport(124, 641);
			} else if (option == 1) {
				player.teleport(218, 457);
			} else if (option == 2) {
				player.teleport(447, 3373);
			} else if (option == 3) {
				player.teleport(264, 339);
			} else if (option == 4) {
				player.teleport(115, 169);
			} else if (option == 5) {
				player.teleport(157, 113);
			} else if (option == 6) {
				player.teleport(231, 236);
			} else if (option == 7) {
				player.teleport(320, 198);
			} else if (option == 8) {
				player.teleport(297, 187);
			}
			
		}
	}
}
