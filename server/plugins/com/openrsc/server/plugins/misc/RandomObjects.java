package com.openrsc.server.plugins.misc;

import com.openrsc.server.constants.NpcId;
import com.openrsc.server.constants.Quests;
import com.openrsc.server.constants.Skills;
import com.openrsc.server.event.ShortEvent;
import com.openrsc.server.model.entity.GameObject;
import com.openrsc.server.model.entity.npc.Npc;
import com.openrsc.server.model.entity.player.Player;
import com.openrsc.server.plugins.listeners.action.ObjectActionListener;
import com.openrsc.server.plugins.listeners.executive.ObjectActionExecutiveListener;
import com.openrsc.server.util.rsc.MessageType;

import static com.openrsc.server.plugins.Functions.*;

public class RandomObjects implements ObjectActionExecutiveListener, ObjectActionListener {

	@Override
	public void onObjectAction(final GameObject object, String command, Player owner) {
		if (command.equals("search") && object.getID() == 17) {
			owner.message(
				"You search the chest, but find nothing");
			return;
		}
		switch (object.getID()) {
			case 78:
				if (command.equals("open")) {
					owner.setBusyTimer(600);
					owner.playerServerMessage(MessageType.QUEST, "You slide open the manhole cover");
					replaceObject(object, new GameObject(object.getWorld(), object.getLocation(), 79, object.getDirection(), object.getType()));
				}
				break;
			case 203:
				if (command.equals("close"))
					replaceObject(object, new GameObject(object.getWorld(), object.getLocation(), 202, object.getDirection(), object.getType()));
				else
					owner.message("the coffin is empty.");
				break;
			case 202:
				replaceObject(object, new GameObject(object.getWorld(), object.getLocation(), 203, object.getDirection(), object.getType()));
				break;
		}
		// ARDOUGNE WALL GATEWAY FOR BIOHAZARD ETC...
		if (object.getID() == 400) {
			owner.playerServerMessage(MessageType.QUEST, "The plant takes a bite at you!");
			owner.damage(getCurrentLevel(owner, Skills.HITS) / 10 + 2);
		}
	}

	@Override
	public boolean blockObjectAction(GameObject obj, String command, Player player) {
		if (obj.getID() == 417) {
			return true;
		}
		if ((obj.getID() == 78 && command.equals("open")) || (obj.getID() == 79 && command.equals("close"))) {
			return true;
		}
		if (obj.getID() == 613 || obj.getID() == 643) {
			return true;
		}
		if (obj.getID() == 202 || obj.getID() == 203 || inArray(obj.getID(), 241, 242, 243))
			return true;
		if (obj.getLocation().getX() == 94 && obj.getLocation().getY() == 521
			&& obj.getID() == 60) {
			if (player.getWorld().getServer().getConfig().MEMBER_WORLD) {
				return true;
			}
		}
		if (obj.getID() == 400) {
			return true;
		}
		if (obj.getID() == 450) {
			return true;
		}
		return false;
	}

}
