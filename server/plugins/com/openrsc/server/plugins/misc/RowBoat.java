package com.openrsc.server.plugins.misc;

import com.openrsc.server.model.entity.GameObject;
import com.openrsc.server.net.rsc.ActionSender;
import com.openrsc.server.model.entity.player.Player;
import com.openrsc.server.plugins.listeners.action.ObjectActionListener;
import com.openrsc.server.plugins.listeners.executive.ObjectActionExecutiveListener;

import static com.openrsc.server.plugins.Functions.*;

public class RowBoat implements ObjectActionListener, ObjectActionExecutiveListener {

	@Override
	public boolean blockObjectAction(GameObject obj, String command, Player player) {
		return obj.getID() == 454;
	}

	@Override
	public void onObjectAction(GameObject obj, String command, Player player) {
		if (obj.getID() == 454 && obj.getX() == 123) {
			
			int option = showMenu(player, "Take boat to Edgeville for 10 points?");
			
			if (option == 0) {
				if(player.getPoints() < 10) {
					player.message("It costs 10 points to ride the boat");
					return;
				} else {
				player.teleport(197, 442);
				player.setPoints(player.getPoints() - 10);
				ActionSender.sendPoints(player);
				return;
				}
			}
			
		}
	}
}
