package com.openrsc.server.plugins.npcs;

import com.openrsc.server.constants.ItemId;
import com.openrsc.server.model.Point;
import com.openrsc.server.model.Shop;
import com.openrsc.server.model.container.Item;
import com.openrsc.server.model.entity.npc.Npc;
import com.openrsc.server.model.entity.player.Player;
import com.openrsc.server.model.world.World;
import com.openrsc.server.net.rsc.ActionSender;
import com.openrsc.server.plugins.ShopInterface;
import com.openrsc.server.plugins.listeners.action.TalkToNpcListener;
import com.openrsc.server.plugins.listeners.executive.TalkToNpcExecutiveListener;
import com.openrsc.server.plugins.listeners.action.NpcCommandListener;
import com.openrsc.server.plugins.listeners.executive.NpcCommandExecutiveListener;

import static com.openrsc.server.plugins.Functions.npcTalk;
import static com.openrsc.server.plugins.Functions.showMenu;

public final class Shops implements NpcCommandListener, NpcCommandExecutiveListener {


	private final Shop shopZambo = new Shop(false, 12400, 100, 40, 0, new Item(222, 20000));
	private final Shop shopHorvik = new Shop(false, 12400, 100, 40, 0, new Item(ItemId.BRONZE_CHAIN_MAIL_BODY.id(),
		100), new Item(ItemId.IRON_CHAIN_MAIL_BODY.id(), 100), new Item(ItemId.STEEL_CHAIN_MAIL_BODY.id(), 100), new Item(431, 100), new Item(ItemId.MITHRIL_CHAIN_MAIL_BODY.id(), 100), new Item(116, 100),
		new Item(ItemId.BRONZE_PLATE_MAIL_BODY.id(), 100), new Item(ItemId.IRON_PLATE_MAIL_BODY.id(), 100), new Item(ItemId.STEEL_PLATE_MAIL_BODY.id(), 100), new Item(120, 100),
		new Item(ItemId.BLACK_PLATE_MAIL_BODY.id(), 100), new Item(ItemId.MITHRIL_PLATE_MAIL_BODY.id(), 100), new Item(206, 100), new Item(ItemId.IRON_PLATE_MAIL_LEGS.id(), 100), new Item(121, 100), new Item(248, 100), new Item(122, 100), new Item(123, 100), new Item(108, 100), new Item(6, 100), new Item(109, 100), new Item(110, 100), new Item(230, 100), new Item(111, 100), new Item(104, 100), new Item(5, 100), new Item(470, 100), new Item(105, 100), new Item(106, 100), new Item(107, 100), new Item(214, 100), new Item(215, 100), new Item(215, 100), new Item(225, 100), new Item(434, 100), new Item(226, 100), new Item(227, 100), new Item(420, 100));
	private final Shop shopFernahei = new Shop(false, 12400, 100, 40, 0,
		new Item(ItemId.SWORDFISH.id(), 20000), new Item(ItemId.LOBSTER.id(), 20000),
		new Item(ItemId.TUNA.id(), 20000), new Item(ItemId.SALMON.id(), 20000), new Item(327, 20000), new Item(1614, 20000), new Item(1617, 20000), new Item(1611, 20000), new Item(1571, 20000), new Item(1601, 20000));
	private final Shop shopZaff = new Shop(false, 12400, 100, 40, 0, new Item(101, 1000), new Item(102, 1000), new Item(103, 1000), new Item(197, 1000), new Item(198, 1000), new Item(32, 10000), new Item(34, 10000), new Item(35, 10000), new Item(36, 10000), new Item(37, 10000), new Item(41, 10000));
	private final Shop shopThessalia = new Shop(false, 12400, 100, 40, 0, new Item(ItemId.WHITE_APRON.id(),
		3), new Item(ItemId.LEATHER_ARMOUR.id(), 100), new Item(ItemId.LEATHER_GLOVES.id(), 100), new Item(ItemId.BOOTS.id(), 100),
		new Item(ItemId.BROWN_APRON.id(), 100), new Item(ItemId.PINK_SKIRT.id(), 100), new Item(ItemId.BLACK_SKIRT.id(), 100),
		new Item(ItemId.BLUE_SKIRT.id(), 100), new Item(216, 100), new Item(184, 100), new Item(229, 1000), new Item(209, 1000), new Item(ItemId.RED_CAPE.id(), 100), new Item(514, 100), new Item(513, 100), new Item(511, 100), new Item(512, 100),
		new Item(ItemId.PRIEST_ROBE.id(), 100), new Item(ItemId.PRIEST_GOWN.id(), 100), new Item(388, 100), new Item(389, 100), new Item(184, 100), new Item(199, 100), new Item(185, 100));
	private final Shop shopLowe = new Shop(false, 12400, 100, 40, 0, new Item(190, 50000), new Item(11, 50000), new Item(59, 250), new Item(60, 250), new Item(ItemId.LONGBOW.id(), 250), new Item(ItemId.SHORTBOW.id(), 250));
	private final Shop shopGaius = new Shop(false, 12400, 100, 40, 0,
		new Item(ItemId.BRONZE_2_HANDED_SWORD.id(), 4), new Item(ItemId.IRON_2_HANDED_SWORD.id(), 3), new Item(ItemId.STEEL_2_HANDED_SWORD.id(), 2),
		new Item(ItemId.BLACK_2_HANDED_SWORD.id(), 1), new Item(ItemId.MITHRIL_2_HANDED_SWORD.id(), 1), new Item(ItemId.ADAMANTITE_2_HANDED_SWORD.id(), 1), new Item(205, 100), new Item(89, 100), new Item(90, 100), new Item(429, 100), new Item(98, 100), new Item(91, 100), new Item(92, 100));
	private final Shop shopDavon = new Shop(false, 12400, 100, 40, 0, new Item(ItemId.SAPPHIRE_AMULET_OF_MAGIC.id(), 1000), new Item(235, 1000), new Item(ItemId.EMERALD_AMULET_OF_PROTECTION.id(), 1000), new Item(186, 1000));
	private final Shop shopCassie = new Shop(false, 12400, 100, 40, 0,
		new Item(ItemId.WOODEN_SHIELD.id(), 100), new Item(ItemId.BRONZE_SQUARE_SHIELD.id(), 100), new Item(ItemId.BRONZE_KITE_SHIELD.id(), 100),
		new Item(ItemId.IRON_SQUARE_SHIELD.id(), 100), new Item(ItemId.IRON_KITE_SHIELD.id(), 100), new Item(ItemId.STEEL_SQUARE_SHIELD.id(), 100),
		new Item(ItemId.STEEL_KITE_SHIELD.id(), 100), new Item(ItemId.BLACK_SQUARE_SHIELD.id(), 100), new Item(ItemId.BLACK_KITE_SHIELD.id(), 100), new Item(ItemId.MITHRIL_SQUARE_SHIELD.id(), 100), new Item(ItemId.MITHRIL_KITE_SHIELD.id(), 100), new Item(ItemId.ADAMANTITE_SQUARE_SHIELD.id(), 100), new Item(ItemId.ADAMANTITE_KITE_SHIELD.id(), 100));
	//private Shop[] shops = null;
	
	@Override
	public void onNpcCommand(Npc n, String command, Player p) {
		if (n.getID() == 165) {
			if (command.equalsIgnoreCase("Shop")) {
				p.setAccessingShop(shopZambo);
				ActionSender.showShop(p, shopZambo);
			}
		} else
		if (n.getID() == 48) {
			p.setAccessingShop(shopHorvik);
			ActionSender.showShop(p, shopHorvik);
		} else
		if (n.getID() == 616) {
			p.setAccessingShop(shopFernahei);
			ActionSender.showShop(p, shopFernahei);
		} else
		if (n.getID() == 69) {
			p.setAccessingShop(shopZaff);
			ActionSender.showShop(p, shopZaff);
		} else
		if (n.getID() == 59) {
			p.setAccessingShop(shopThessalia);
			ActionSender.showShop(p, shopThessalia);
		} else
		if (n.getID() == 58) {
			p.setAccessingShop(shopLowe);
			ActionSender.showShop(p, shopLowe);
		} else
		if (n.getID() == 228) {
			p.setAccessingShop(shopGaius);
			ActionSender.showShop(p, shopGaius);
		} else
		if (n.getID() == 278) {
			p.setAccessingShop(shopDavon);
			ActionSender.showShop(p, shopDavon);
		} else
		if (n.getID() == 101) {
			p.setAccessingShop(shopCassie);
			ActionSender.showShop(p, shopCassie);
		}
	}
	@Override
	public boolean blockNpcCommand(Npc n, String command, Player p) {
		if (command.equalsIgnoreCase("Shop")) {
			return true;
		}
		return false;
	}


}
