package com.openrsc.server.plugins.npcs.shilo;

import com.openrsc.server.constants.ItemId;
import com.openrsc.server.constants.NpcId;
import com.openrsc.server.model.Shop;
import com.openrsc.server.model.container.Item;
import com.openrsc.server.model.entity.npc.Npc;
import com.openrsc.server.model.entity.player.Player;
import com.openrsc.server.model.world.World;
import com.openrsc.server.net.rsc.ActionSender;
import com.openrsc.server.plugins.ShopInterface;
import com.openrsc.server.plugins.listeners.action.TalkToNpcListener;
import com.openrsc.server.plugins.listeners.executive.TalkToNpcExecutiveListener;

import static com.openrsc.server.plugins.Functions.npcTalk;
import static com.openrsc.server.plugins.Functions.showMenu;

public class BetaShop implements ShopInterface,
	TalkToNpcExecutiveListener, TalkToNpcListener {

	private final Shop shop = new Shop(false, 12400, 100, 40, 0,
		new Item(112, 20000), new Item(93, 20000), new Item(81, 20000), new Item(316, 20000), new Item(317, 20000), new Item(400, 20000), new Item(401, 20000), new Item(402, 20000), new Item(404, 20000), new Item(406, 20000), new Item(407, 20000), new Item(222, 50000), new Item(31, 50000), new Item(33, 50000), new Item(38, 50000));

	@Override
	public void onTalkToNpc(Player p, Npc n) {
		if (n.getID() == 803) {
			npcTalk(p, n, "Welcome to the Beta Shop",
				"Would you like to see my items?");
			int menu = showMenu(p, n,
				"Yes please!",
				"No");
			if (menu == 0) {
				p.setAccessingShop(shop);
				ActionSender.showShop(p, shop);
			} else if (menu == 1) {
				npcTalk(p, n, "Ok.");
			}
		}
	}

	@Override
	public boolean blockTalkToNpc(Player p, Npc n) {
		return n.getID() == 803;
	}

	@Override
	public Shop[] getShops(World world) {
		return new Shop[]{shop};
	}

	@Override
	public boolean isMembers() {
		return true;	
	}

}
