package com.openrsc.server.plugins.npcs.lumbridge;

import com.openrsc.server.constants.ItemId;
import com.openrsc.server.constants.NpcId;
import com.openrsc.server.model.Shop;
import com.openrsc.server.model.container.Item;
import com.openrsc.server.model.container.Inventory;
import com.openrsc.server.model.entity.npc.Npc;
import com.openrsc.server.model.entity.player.Player;
import com.openrsc.server.model.world.World;
import com.openrsc.server.net.rsc.ActionSender;
import com.openrsc.server.plugins.ShopInterface;
import com.openrsc.server.plugins.listeners.action.TalkToNpcListener;
import com.openrsc.server.plugins.listeners.executive.TalkToNpcExecutiveListener;

import static com.openrsc.server.plugins.Functions.npcTalk;
import static com.openrsc.server.plugins.Functions.showMenu;

public final class BobsAxes implements TalkToNpcExecutiveListener, TalkToNpcListener {

	public boolean blockTalkToNpc(final Player p, final Npc n) {
		return n.getID() == 77;
	}

	public boolean isMembers() {
		return false;
	}

	@Override
	public void onTalkToNpc(final Player p, final Npc n) {
		npcTalk(p, n, "Hello, I will trade you 1 cooked trout for 3 feathers.");
		int option = showMenu(p, n, "Ok. Take all of my feathers for your trout.",
			"No thanks");
		switch (option) {
			case 0:
				if (p.getInventory().countId(381) > 2){
					Inventory inventory = p.getInventory();
					Item item;
					Item item2;
					item = new Item(1603, p.getInventory().countId(381) / 3);
					item2 = new Item(381, p.getInventory().countId(381));
					npcTalk(p, n, "Ok, here you are.");
					inventory.add(item, false);
					inventory.remove(item2, false);
					ActionSender.sendInventory(p);
				} else {
					npcTalk(p, n, "Come back with at least 3 feathers mate.");
				}
				break;
			case 1:
				npcTalk(p, n, "Alright sir.");
				break;
		}
	}
}
