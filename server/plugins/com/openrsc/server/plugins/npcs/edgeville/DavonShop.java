package com.openrsc.server.plugins.npcs.brimhaven;

import com.openrsc.server.constants.ItemId;
import com.openrsc.server.constants.NpcId;
import com.openrsc.server.model.Shop;
import com.openrsc.server.model.container.Item;
import com.openrsc.server.model.entity.npc.Npc;
import com.openrsc.server.model.entity.player.Player;
import com.openrsc.server.model.world.World;
import com.openrsc.server.net.rsc.ActionSender;
import com.openrsc.server.plugins.ShopInterface;
import com.openrsc.server.plugins.listeners.action.TalkToNpcListener;
import com.openrsc.server.plugins.listeners.executive.TalkToNpcExecutiveListener;

import static com.openrsc.server.plugins.Functions.npcTalk;
import static com.openrsc.server.plugins.Functions.showMenu;

public class DavonShop implements ShopInterface, TalkToNpcExecutiveListener, TalkToNpcListener {

	private final Shop shop = new Shop(false, 12400, 100, 40, 0, new Item(ItemId.SAPPHIRE_AMULET_OF_MAGIC.id(), 1000), new Item(235, 1000), new Item(ItemId.EMERALD_AMULET_OF_PROTECTION.id(), 1000), new Item(186, 1000));

	@Override
	public void onTalkToNpc(Player p, Npc n) {
		npcTalk(p, n, "Want to do some amulet trading");
		int menu = showMenu(p, n, "What are you selling?", "No thanks");
		if (menu == 0) {
			p.message("Davon opens up his jacket to reveal some amulets");
			p.setAccessingShop(shop);
			ActionSender.showShop(p, shop);
		} else if (menu == 1) {
			npcTalk(p, n, "See ya around slick");
		}
	}

	@Override
	public boolean blockTalkToNpc(Player p, Npc n) {
		return n.getID() == NpcId.DAVON.id();
	}

	@Override
	public Shop[] getShops(World world) {
		return new Shop[]{shop};
	}

	@Override
	public boolean isMembers() {
		return true;
	}
}
