package com.openrsc.server.plugins.npcs.varrock;

import com.openrsc.server.constants.ItemId;
import com.openrsc.server.constants.NpcId;
import com.openrsc.server.model.Shop;
import com.openrsc.server.model.container.Item;
import com.openrsc.server.model.entity.npc.Npc;
import com.openrsc.server.model.entity.player.Player;
import com.openrsc.server.model.world.World;
import com.openrsc.server.net.rsc.ActionSender;
import com.openrsc.server.plugins.ShopInterface;
import com.openrsc.server.plugins.listeners.action.TalkToNpcListener;
import com.openrsc.server.plugins.listeners.executive.TalkToNpcExecutiveListener;

import static com.openrsc.server.plugins.Functions.*;

public final class EdgeShop implements ShopInterface,
	TalkToNpcExecutiveListener, TalkToNpcListener {

	/*private final Shop shop = new Shop(false, 12400, 100, 40, 0, new Item(190, 50000), new Item(638, 50000), new Item(640, 50000), new Item(11, 50000), new Item(221, 2000), new Item(474, 2000), new Item(477, 2000), new Item(483, 2000), new Item(480, 2000), new Item(498, 2000), new Item(486, 2000), new Item(492, 2000), new Item(495, 2000), new Item(ItemId.SWORDFISH.id(), 10000), new Item(59, 250), new Item(ItemId.LONGBOW.id(), 250), new Item(186, 200), new Item(315, 500), new Item(314, 500), new Item(235, 500), new Item(316, 500), new Item(388, 50),
		new Item(389, 50), new Item(429, 100), new Item(77, 100), new Item(78, 100), new Item(79, 100), new Item(80, 100), new Item(101, 1000), new Item(102, 1000), new Item(197, 1000), new Item(198, 1000), new Item(1217, 1000), new Item(1218, 1000), new Item(1216, 1000), new Item(103, 10000), new Item(32, 10000), new Item(34, 10000), new Item(35, 10000), new Item(36, 10000), new Item(37, 10000), new Item(40, 10000), new Item(41, 10000), new Item(42, 10000), new Item(46, 10000));
*/
private final Shop shop = new Shop(false, 12400, 100, 40, 0, new Item(28, 10000));

	@Override
	public boolean blockTalkToNpc(final Player p, final Npc n) {
		return n.getID() == 185 || n.getID() == 186;
	}

	@Override
	public Shop[] getShops(World world) {
		return new Shop[]{shop};
	}

	@Override
	public boolean isMembers() {
		return false;
	}

	@Override
	public void onTalkToNpc(final Player p, final Npc n) {
		npcTalk(p, n, "Would you like to sell your points for Gp?", "1 Gp costs 3 Points.");

		int option = showMenu(p, n, false,
			"Yes please", "No thanks");

		if (option == 0) {
			playerTalk(p, n, "Yes Please");
			//p.setAccessingShop(shop);
			ActionSender.showPoints2Gp(p);
		} else if (option == 1) {
			playerTalk(p, n, "No thanks");
		}
	}

}
