package com.openrsc.server.plugins.npcs.shilo;

import com.openrsc.server.constants.ItemId;
import com.openrsc.server.constants.NpcId;
import com.openrsc.server.model.Shop;
import com.openrsc.server.model.container.Item;
import com.openrsc.server.model.entity.npc.Npc;
import com.openrsc.server.model.entity.player.Player;
import com.openrsc.server.model.world.World;
import com.openrsc.server.net.rsc.ActionSender;
import com.openrsc.server.plugins.ShopInterface;
import com.openrsc.server.plugins.listeners.action.TalkToNpcListener;
import com.openrsc.server.plugins.listeners.executive.TalkToNpcExecutiveListener;

import static com.openrsc.server.plugins.Functions.npcTalk;
import static com.openrsc.server.plugins.Functions.showMenu;

public class Fernahei implements ShopInterface,
	TalkToNpcExecutiveListener, TalkToNpcListener {

	private final Shop shop = new Shop(false, 12400, 100, 40, 0,
		new Item(ItemId.SWORDFISH.id(), 20000), new Item(ItemId.LOBSTER.id(), 20000),
		new Item(ItemId.TUNA.id(), 20000), new Item(ItemId.SALMON.id(), 20000), new Item(327, 20000), new Item(1614, 20000), new Item(1617, 20000), new Item(1611, 20000), new Item(1571, 20000), new Item(1601, 20000));

	@Override
	public void onTalkToNpc(Player p, Npc n) {
		if (n.getID() == NpcId.FERNAHEI.id()) {
			npcTalk(p, n, "Welcome to Fernahei's Food Shop!",
				"Would you like to see my items?");
			int menu = showMenu(p, n,
				"Yes please!",
				"No, but thanks for the offer.");
			if (menu == 0) {
				p.setAccessingShop(shop);
				ActionSender.showShop(p, shop);
			} else if (menu == 1) {
				npcTalk(p, n, "That's fine and thanks for your interest.");
			}
		}
	}

	@Override
	public boolean blockTalkToNpc(Player p, Npc n) {
		return n.getID() == NpcId.FERNAHEI.id();
	}

	@Override
	public Shop[] getShops(World world) {
		return new Shop[]{shop};
	}

	@Override
	public boolean isMembers() {
		return true;	
	}

}
