package com.openrsc.server.plugins.npcs.varrock;

import com.openrsc.server.constants.ItemId;
import com.openrsc.server.constants.NpcId;
import com.openrsc.server.model.Shop;
import com.openrsc.server.model.container.Item;
import com.openrsc.server.model.entity.npc.Npc;
import com.openrsc.server.model.entity.player.Player;
import com.openrsc.server.model.world.World;
import com.openrsc.server.net.rsc.ActionSender;
import com.openrsc.server.plugins.ShopInterface;
import com.openrsc.server.plugins.listeners.action.TalkToNpcListener;
import com.openrsc.server.plugins.listeners.executive.TalkToNpcExecutiveListener;

import static com.openrsc.server.plugins.Functions.npcTalk;
import static com.openrsc.server.plugins.Functions.showMenu;

public final class HorvikTheArmourer implements
	ShopInterface, TalkToNpcExecutiveListener, TalkToNpcListener {
	
	private final Shop shop = new Shop(false, 12400, 100, 40, 0, new Item(ItemId.BRONZE_CHAIN_MAIL_BODY.id(),
		100), new Item(ItemId.IRON_CHAIN_MAIL_BODY.id(), 100), new Item(ItemId.STEEL_CHAIN_MAIL_BODY.id(), 100), new Item(431, 100), new Item(ItemId.MITHRIL_CHAIN_MAIL_BODY.id(), 100), new Item(116, 100),
		new Item(ItemId.BRONZE_PLATE_MAIL_BODY.id(), 100), new Item(ItemId.IRON_PLATE_MAIL_BODY.id(), 100), new Item(ItemId.STEEL_PLATE_MAIL_BODY.id(), 100), new Item(120, 100),
		new Item(ItemId.BLACK_PLATE_MAIL_BODY.id(), 100), new Item(ItemId.MITHRIL_PLATE_MAIL_BODY.id(), 100), new Item(206, 100), new Item(ItemId.IRON_PLATE_MAIL_LEGS.id(), 100), new Item(121, 100), new Item(248, 100), new Item(122, 100), new Item(123, 100), new Item(108, 100), new Item(6, 100), new Item(109, 100), new Item(110, 100), new Item(230, 100), new Item(111, 100), new Item(104, 100), new Item(5, 100), new Item(470, 100), new Item(105, 100), new Item(106, 100), new Item(107, 100), new Item(214, 100), new Item(215, 100), new Item(215, 100), new Item(225, 100), new Item(434, 100), new Item(226, 100), new Item(227, 100), new Item(420, 100));


	@Override
	public boolean blockTalkToNpc(final Player p, final Npc n) {
		return n.getID() == NpcId.HORVIK_THE_ARMOURER.id();
	}

	@Override
	public Shop[] getShops(World world) {
		return new Shop[]{shop};
	}

	@Override
	public boolean isMembers() {
		return false;
	}

	@Override
	public void onTalkToNpc(final Player p, final Npc n) {
		npcTalk(p, n, "Hello, do you need any help?");
		int option = showMenu(p, n,
			"No thanks. I'm just looking around",
			"Do you want to trade?");

		if (option == 1) {
			npcTalk(p, n, "Yes, I have a fine selection of armour");
			p.setAccessingShop(shop);
			ActionSender.showShop(p, shop);
		}
	}

}
