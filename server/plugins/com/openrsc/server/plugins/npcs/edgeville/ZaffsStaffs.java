package com.openrsc.server.plugins.npcs.varrock;

import com.openrsc.server.constants.ItemId;
import com.openrsc.server.constants.NpcId;
import com.openrsc.server.model.Shop;
import com.openrsc.server.model.container.Item;
import com.openrsc.server.model.entity.npc.Npc;
import com.openrsc.server.model.entity.player.Player;
import com.openrsc.server.model.world.World;
import com.openrsc.server.net.rsc.ActionSender;
import com.openrsc.server.plugins.ShopInterface;
import com.openrsc.server.plugins.listeners.action.TalkToNpcListener;
import com.openrsc.server.plugins.listeners.executive.TalkToNpcExecutiveListener;

import static com.openrsc.server.plugins.Functions.npcTalk;
import static com.openrsc.server.plugins.Functions.showMenu;

public final class ZaffsStaffs implements
	ShopInterface, TalkToNpcExecutiveListener, TalkToNpcListener {
	
	private final Shop shop = new Shop(false, 12400, 100, 40, 0, new Item(101, 1000), new Item(102, 1000), new Item(103, 1000), new Item(197, 1000), new Item(198, 1000), new Item(32, 10000), new Item(34, 10000), new Item(35, 10000), new Item(36, 10000), new Item(37, 10000), new Item(41, 10000));


	@Override
	public boolean blockTalkToNpc(final Player p, final Npc n) {
		return n.getID() == NpcId.ZAFF.id();
	}

	@Override
	public Shop[] getShops(World world) {
		return new Shop[]{shop};
	}

	@Override
	public boolean isMembers() {
		return false;
	}

	@Override
	public void onTalkToNpc(final Player p, final Npc n) {
		npcTalk(p, n, "Would you like to buy or sell some magic equipment?");
		int option = showMenu(p, n, "Yes please", "No, thank you");
		if (option == 0) {
			npcTalk(p, n, "Ok, have a look see");
			p.setAccessingShop(shop);
			ActionSender.showShop(p, shop);
		}
	}

}
