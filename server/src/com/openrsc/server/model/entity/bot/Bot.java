package com.openrsc.server.model.entity.bot;

import com.openrsc.server.model.PlayerAppearance;
import com.openrsc.server.model.Point;
import com.openrsc.server.model.container.Bank;
import com.openrsc.server.model.container.Equipment;
import com.openrsc.server.model.container.Inventory;
import com.openrsc.server.model.container.Item;
import com.openrsc.server.model.entity.player.*;
import com.openrsc.server.model.world.World;
import com.openrsc.server.net.rsc.ActionSender;
import com.openrsc.server.util.rsc.DataConversions;

public class Bot extends Player {

	public Bot(World world) {
		super(world);
		isBot = true;
		super.setUsername("test bot");
		super.setUsernameHash(DataConversions.usernameToHash(super.getUsername()));
		super.password = "fsd";
		super.setCurrentIP("127.0.0.1");
		sessionStart = System.currentTimeMillis();
		currentLogin = System.currentTimeMillis();
		setBusy(true);
		trade = new Trade(this);
		duel = new Duel(this);
		playerSettings = new PlayerSettings(this);
		social = new Social(this);
		prayers = new Prayers(this);
		//
		Bank bank = new Bank(this);
		Inventory inv = new Inventory(this);
		Equipment equipment = new Equipment(this);
		this.setInventory(inv);
		this.setEquipment(equipment);
		this.setBank(bank);
		if (this.getLastLogin() == 0L) {
			this.setInitialLocation(Point.location(216, 744));
			//this.setChangingAppearance(true);
			setDefaultAppearance(this);
		}
		//Player p = this;
		//world.getServer().getPluginHandler().handleAction("PlayerLogin", new Object[]{p});
		ActionSender.sendLogin(this);
		/*this.teleport(120, 648, false);
		if (this.getCache().hasKey("tutorial")) {
			this.getCache().remove("tutorial");
		}*/
	}
	
	public static void setDefaultAppearance(Player player){
		player.setChangingAppearance(false);
		byte headGender = 1;
		byte headType = 0;
		byte bodyGender = 1;

		int hairColour = 2;
		int topColour = 8;
		int trouserColour = 14;
		int skinColour = 0;

		int playerMode1 = 0;
		int playerMode2 = 0;

		int headSprite = headType + 1;
		int bodySprite = bodyGender + 1;

		PlayerAppearance appearance = new PlayerAppearance(hairColour,
			topColour, trouserColour, skinColour, headSprite, bodySprite);
		if (!appearance.isValid()) {
			player.setSuspiciousPlayer(true, "player invalid appearance");
			return;
		}
		player.setMale(headGender == 1);
		player.getSettings().setAppearance(appearance);
		player.setWornItems(player.getSettings().getAppearance().getSprites());
		player.getUpdateFlags().setAppearanceChanged(true);
		//player.setIronMan(playerMode1);
		//player.setOneXp(playerMode2 == 1);
		//getUpdateFlags().setAppearanceChanged(true);
		/*if (player.isMale()) {
			if (player.getWorld().getServer().getConfig().WANT_EQUIPMENT_TAB) {
				Item top = player.getEquipment().get(1);
				if (top != null && top.getDef(player.getWorld()).isFemaleOnly()) {
					player.getInventory().unwieldItem(top, false);
					ActionSender.sendEquipmentStats(player, 1);
				}
			} else {
				Inventory inv = player.getInventory();
				for (int slot = 0; slot < inv.size(); slot++) {
					Item i = inv.get(slot);
					if (i.isWieldable(player.getWorld()) && i.getDef(player.getWorld()).getWieldPosition() == 1
						&& i.isWielded() && i.getDef(player.getWorld()).isFemaleOnly()) {
						player.getInventory().unwieldItem(i, false);
						ActionSender.sendInventoryUpdateItem(player, slot);
						break;
					}
				}
			}
		}
		int[] oldWorn = player.getWornItems();
		int[] oldAppearance = player.getSettings().getAppearance().getSprites();
		//player.getSettings().setAppearance(appearance);
		int[] newAppearance = player.getSettings().getAppearance().getSprites();
		for (int i = 0; i < 12; i++) {
			if (oldWorn[i] == oldAppearance[i]) {
				player.updateWornItems(i, newAppearance[i]);
			}
		}

		if (player.getWorld().getServer().getConfig().CHARACTER_CREATION_MODE == 1) {
			if (player.getLastLogin() == 0L) {
				player.setIronMan(playerMode1);
				player.setOneXp(playerMode2 == 1);
			}
		}*/
	}

}
