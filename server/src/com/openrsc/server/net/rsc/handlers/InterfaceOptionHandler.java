package com.openrsc.server.net.rsc.handlers;

import com.openrsc.server.constants.IronmanMode;
import com.openrsc.server.content.clan.Clan;
import com.openrsc.server.content.clan.ClanInvite;
import com.openrsc.server.content.clan.ClanPlayer;
import com.openrsc.server.content.clan.ClanRank;
import com.openrsc.server.model.entity.GroundItem;
import com.openrsc.server.content.party.Party;
import com.openrsc.server.content.party.PartyInvite;
import com.openrsc.server.content.party.PartyPlayer;
import com.openrsc.server.content.party.PartyRank;
import com.openrsc.server.model.entity.npc.Npc;
import com.openrsc.server.model.entity.player.Player;
import com.openrsc.server.net.Packet;
import com.openrsc.server.net.rsc.ActionSender;
import com.openrsc.server.model.container.Item;
import com.openrsc.server.net.rsc.PacketHandler;
import com.openrsc.server.plugins.Functions;
import com.openrsc.server.util.rsc.DataConversions;

public class InterfaceOptionHandler implements PacketHandler {


	private static String[] badWords = {
		"fuck", "ass", "bitch", "admin", "mod", "dev", "developer", "nigger", "niger",
		"whore", "pussy", "porn", "penis", "chink", "faggot", "cunt", "clit", "cock"};

	@Override
	public void handlePacket(Packet p, Player player) throws Exception {
		switch (p.readByte()) {
			case 0:
				player.setAttribute("swap_cert", p.readByte() == 1);
				break;
			case 1:
				player.setAttribute("swap_note", p.readByte() == 1);
				break;
			case 2:// Swap
				int slot = p.readInt();
				int to = p.readInt();

				if (player.getBank().swap(slot, to)) {
					ActionSender.updateBankItem(player, slot, player.getBank().get(slot).getID(),
						player.getBank().get(slot).getAmount());
					ActionSender.updateBankItem(player, to, player.getBank().get(to).getID(),
						player.getBank().get(to).getAmount());
				}
				break;
			case 3: // Insert
				slot = p.readInt();
				to = p.readInt();
				if (player.getBank().insert(slot, to)) {
					ActionSender.showBank(player);
				}
				break;

			case 4: // Insert
				slot = p.readInt();
				to = p.readInt();

				player.getInventory().swap(slot, to);
				break;
			case 5: // Swap
				slot = p.readInt();
				to = p.readInt();

				player.getInventory().insert(slot, to);
				ActionSender.sendInventory(player);
				break;
			case 6:
				player.checkAndInterruptBatchEvent();
				break;
			case 7:
				int secondary = (int) p.readByte();
				if (secondary == 0) {
					int mode = (int) p.readByte();
					if (mode < 0 || mode > 3) {
						player.setSuspiciousPlayer(true, "mode < 0 or mode > 3");
						return;
					}
					if (mode > -1) {
						if (mode == IronmanMode.Ironman.id()) {
							if (!player.getLocation().onTutorialIsland() && (player.getIronMan() <= IronmanMode.None.id() || player.getIronMan() > IronmanMode.Transfer.id())) {
								player.message("You cannot become an Iron Man after leaving Tutorial Island.");
								return;
							}
							if (player.getIronMan() == IronmanMode.Ironman.id()) {
								return;
							}
							if (!player.getLocation().onTutorialIsland()
								&& (player.getIronMan() == IronmanMode.Ultimate.id() || player.getIronMan() == IronmanMode.Hardcore.id())) {
								if (player.getIronManRestriction() == 0) {
									if (player.getCache().hasKey("bank_pin")) {
										Npc npc = Functions.getMultipleNpcsInArea(player, 11, 799, 800, 801);
										if (npc != null) {
											ActionSender.sendHideIronManInterface(player);
											player.setAttribute("ironman_delete", true);
											player.setAttribute("ironman_mode", mode);
											npc.initializeTalkScript(player);
										} else {
											player.message("The Iron Men are currently busy");
										}
									}
								} else {
									player.message("Your account is set to permanent - you cannot remove your status");
								}
								return;
							}
							player.setIronMan(IronmanMode.Ironman.id());
						} else if (mode == IronmanMode.Ultimate.id()) {
							if (!player.getLocation().onTutorialIsland() && player.getIronMan() != IronmanMode.Ultimate.id()) {
								player.message("You cannot become an Ultimate Iron Man after leaving Tutorial Island.");
								return;
							}
							if (player.getIronMan() == IronmanMode.Ultimate.id()) {
								return;
							}
							player.setIronMan(IronmanMode.Ultimate.id());
						} else if (mode == IronmanMode.Hardcore.id()) {
							if (!player.getLocation().onTutorialIsland() && player.getIronMan() != IronmanMode.Hardcore.id()) {
								player.message("You cannot become a Hardcore Iron Man after leaving Tutorial Island.");
								return;
							}
							if (player.getIronMan() == IronmanMode.Hardcore.id()) {
								return;
							}
							player.setIronMan(IronmanMode.Hardcore.id());
						} else {
							if (player.getIronMan() == IronmanMode.None.id()) {
								return;
							}
							if (!player.getLocation().onTutorialIsland()
								&& (player.getIronMan() == IronmanMode.Ironman.id() || player.getIronMan() == IronmanMode.Ultimate.id()
								|| player.getIronMan() == IronmanMode.Hardcore.id())) {
								if (player.getIronManRestriction() == 0) {
									if (player.getCache().hasKey("bank_pin")) {
										Npc npc = Functions.getMultipleNpcsInArea(player, 11, 799, 800, 801);
										if (npc != null) {
											ActionSender.sendHideIronManInterface(player);
											player.setAttribute("ironman_delete", true);
											player.setAttribute("ironman_mode", mode);
											npc.initializeTalkScript(player);
										} else {
											player.message("The Iron Men are currently busy");
										}
									}
								} else {
									player.message("Your account is set to permanent - you cannot remove your status");
								}
								return;
							}
							player.setIronMan(IronmanMode.None.id());
						}
						ActionSender.sendIronManMode(player);
					}
				} else if (secondary == 1) {
					int setting = (int) p.readByte();
					if (setting < 0 || setting > 1) {
						player.setSuspiciousPlayer(true, "setting < 0 or setting > 1");
						return;
					}
					if (!player.getLocation().onTutorialIsland()) {
						player.message("You cannot change this setting now that you have completed the Tutorial.");
						return;
					}
					if (setting > -1) {
						if (player.getIronMan() == IronmanMode.None.id()) {
							return;
						}
						if (setting == 0) {
							if (!player.getCache().hasKey("bank_pin")) {
								Npc npc = Functions.getMultipleNpcsInArea(player, 11, 799, 800, 801);
								if (npc != null) {
									ActionSender.sendHideIronManInterface(player);
									player.setAttribute("ironman_pin", true);
									npc.initializeTalkScript(player);
								} else {
									player.message("The Iron Men are currently busy");
								}
							} else {
								player.setIronManRestriction(0);
							}
						} else {
							player.setIronManRestriction(1);
						}
						ActionSender.sendIronManMode(player);
					}
				}
				break;
			case 8:
				int action = p.readByte();
				if (action == 0) {
					String bankpin = p.readString();
					if (bankpin.length() != 4) {
						return;
					}
					player.setAttribute("bank_pin_entered", bankpin);
				} else if (action == 1) {
					player.setAttribute("bank_pin_entered", "cancel");
				}
				break;
			case 10:
				if (player.isIronMan(IronmanMode.Ironman.id()) || player.isIronMan(IronmanMode.Ultimate.id())
					|| player.isIronMan(IronmanMode.Hardcore.id()) || player.isIronMan(IronmanMode.Transfer.id())) {
					player.message("As an Iron Man, you cannot use the Auction.");
					return;
				}
				if (player.getWorld().getServer().timeTillShutdown() > 0) {
					player.message("Auction house is disabled until server restart!");
					return;
				}
				/* Set true when auctioneer opens AH window for the player */
				if (!player.getAttribute("auctionhouse", false)) {
					return;
				}

				int type = p.readByte();
				switch (type) {
					case 0: /* Buy */
						int auctionBuyID = p.readInt();
						int amountBuy = p.readInt();

						if (System.currentTimeMillis() - player.getAttribute("ah_buy_item", (long) 0) < 5000) {
							ActionSender.sendBox(player, "@ora@[Auction House - Warning] % @whi@ You recently purchased an item, please wait 5 seconds.", false);
							return;
						}
						player.setAttribute("ah_buy_item", System.currentTimeMillis());

						player.getWorld().getMarket().addBuyAuctionItemTask(player, auctionBuyID, amountBuy);
						break;

					case 1: /* Create auction */
						int itemID = p.readInt();
						int amount = p.readInt();
						int price = p.readInt();
						player.getWorld().getMarket().addNewAuctionItemTask(player, itemID, amount, price);
						break;
					case 2:
						int auctionID = p.readInt();
						player.getWorld().getMarket().addCancelAuctionItemTask(player, auctionID);
						break;
					case 3:
						if (System.currentTimeMillis() - player.getAttribute("ah_refresh", (long) 0) < 5000) {
							player.message("@ora@[Auction House - Warning]@whi@ You recently refreshed, please wait 5 seconds.");
							return;
						}
						player.setAttribute("ah_refresh", System.currentTimeMillis());
						player.message("@gre@[Auction House]@whi@ List has been refreshed!");
						ActionSender.sendOpenAuctionHouse(player);
						break;
					case 4:
						player.setAttribute("auctionhouse", false);
						break;
					case 5:
						auctionID = p.readInt();

						player.getWorld().getMarket().addModeratorDeleteItemTask(player, auctionID);
						break;
				}
				break;
			case 11: // Clan Actions
				if (!player.getWorld().getServer().getConfig().WANT_CLANS) return;
				int actionType = p.readByte();
				switch (actionType) {
					case 0: // CREATE CLAN
						String clanName = p.readString();
						String clanTag = p.readString();
						if (clanName.length() < 2) {
							ActionSender.sendBox(player, "Clan name must be at least 2 characters in length", false);
							return;
						} else if (clanName.length() > 16) {
							ActionSender.sendBox(player, "Clan name length cannot exceed 16 characters in length", false);
							return;
						} else if (clanTag.length() < 2) {
							ActionSender.sendBox(player, "Clan tag need to be minimum 2 characters", false);
							return;
						} else if (clanTag.length() > 5) {
							ActionSender.sendBox(player, "Clan tag maximum length is 5 characters", false);
							return;
						} else if (!clanName.matches("^[\\p{IsAlphabetic}\\p{IsDigit}\\p{Space}]+$") || !clanTag.matches("^[\\p{IsAlphabetic}\\p{IsDigit}\\p{Space}]+$")) {
							ActionSender.sendBox(player, "Clan name and Clan tag can only contain regular letters and numbers", false);
							return;
						}
						for (String s : badWords) { // check every word
							if (clanName.contains(s) || clanTag.contains(s)) {
								ActionSender.sendBox(player, "Bad clan name or clan tag, try with something else", false);
								return;
							}
						}
						if (player.getClan() != null) {
							ActionSender.sendBox(player, "You are already in a clan", false);
							return;
						}

						if (player.getWorld().getClanManager().getClan(clanName) == null && player.getWorld().getClanManager().getClan(clanTag) == null) {
							Clan clan = new Clan(player.getWorld());
							clan.setClanName(clanName);
							clan.setClanTag(clanTag);

							ClanPlayer clanMember = clan.addPlayer(player);
							clanMember.setRank(ClanRank.LEADER);
							clan.setLeader(clanMember);

							player.getWorld().getClanManager().createClan(clan);
							player.message("You have created clan: " + clanName);
						} else {
							ActionSender.sendBox(player, "There is already a clan with this Clan Name or Clan Tag", false);
						}

						break;
					case 1:
						if (player.getClan() != null) {
							player.getClan().removePlayer(player.getUsername());
						}
						break;
					case 2:
						String playerInvited = p.readString();
						Player invited = player.getWorld().getPlayer(DataConversions.usernameToHash(playerInvited));
						/*if (!player.getClan().isAllowed(1, player)) {
							player.message("You are not allowed to invite into clan.");
							return;
						}*/
						if (invited != null) {
							ClanInvite.createClanInvite(player, invited);
						} else {
							ActionSender.sendBox(player, "Player is not online or could not be found!", false);
						}
						break;
					case 3:
						if (player.getActiveClanInvite() != null) {
							player.getActiveClanInvite().accept();
						}
						break;
					case 4:
						if (player.getActiveClanInvite() != null) {
							player.getActiveClanInvite().decline();
						}
						break;
					case 5: // KICK
						if (player.getClan() != null) {
							String playerToKick = p.readString();
							if (!player.getClan().isAllowed(0, player)) {
								player.message("You are not allowed to kick.");
								return;
							}
							if (player.getClan().getLeader().getUsername().equals(playerToKick)) {
								player.message("You can't kick the leader.");
								return;
							}
							player.getClan().removePlayer(playerToKick);
						}
						break;
					case 6: // RANK plaayer
						if (player.getClan() != null) {
							String playerRank = p.readString();
							int rank = p.readByte();
							if (rank >= 3) {
								rank = 0;
							}
							if (!player.getClan().getLeader().getUsername().equals(player.getUsername().replaceAll("_", " "))) {
								player.message("You are not the leader of this clan");
								return;
							}
							if (player.getClan().getLeader().getUsername().equals(playerRank)) {
								player.message("You are already the leader of the clan");
								return;
							}
							player.getClan().updateRankPlayer(player, playerRank, rank);
						}
						break;
					case 7: // CLAN SETTINGS
						if (player.getClan() != null) {
							int settingPreference = p.readByte();
							if (settingPreference > 3) {
								return;
							}
							int state = p.readByte();
							if (state >= 3) {
								state = 0;
							}
							if (!player.getClan().getLeader().getUsername().equals(player.getUsername())) {
								player.message("You are not the leader of this clan");
								return;
							}
							if (settingPreference == 0) {
								if (player.getClan().getKickSetting() == state) {
									return;
								}
								player.getClan().setKickSetting(state);
							} else if (settingPreference == 1) {
								if (player.getClan().getInviteSetting() == state) {
									return;
								}
								player.getClan().setInviteSetting(state);
							} else if (settingPreference == 2) {
								if (player.getClan().getAllowSearchJoin() == state) {
									return;
								}
								player.getClan().setAllowSearchJoin(state);
							}
							player.message("[CLAN]: You have updated clan settings");
							player.getClan().updateClanSettings();

						}
						break;
					case 8:
						ActionSender.sendClans(player);
						break;
					case 9:

						break;
				}
				break;
			case 12: // Party
				if (!player.getWorld().getServer().getConfig().WANT_PARTIES) return;
				int actionType2 = p.readByte();
				switch (actionType2) {
					case 0: // CREATE PARTY
						if (player.getParty() != null) {
							ActionSender.sendBox(player, "Leave your current party before joining another", false);
							return;
						}
						if (player.isIronMan(IronmanMode.Ironman.id()) || player.isIronMan(IronmanMode.Ultimate.id())
							|| player.isIronMan(IronmanMode.Hardcore.id()) || player.isIronMan(IronmanMode.Transfer.id())) {
							player.message("You are an Iron Man. You stand alone.");
							return;
						}

						Party party = new Party(player.getWorld());
						//party.setPartyName(partyName);
						//party.setPartyTag(partyTag);

						PartyPlayer partyMember = party.addPlayer(player);
						partyMember.setRank(PartyRank.LEADER);
						party.setLeader(partyMember);

						player.getWorld().getPartyManager().createParty(party);
						player.message("You have created a party: ");

						break;
					case 1:
						if (player.getParty() != null) {
							player.getParty().removePlayer(player.getUsername());
						}
						break;
					case 2:
						Player invited = player.getWorld().getPlayer(p.readShort());
						if (player.isIronMan(IronmanMode.Ironman.id()) || player.isIronMan(IronmanMode.Ultimate.id())
							|| player.isIronMan(IronmanMode.Hardcore.id()) || player.isIronMan(IronmanMode.Transfer.id())) {
							player.message("You are an Iron Man. You stand alone.");
							return;
						}
						if (player.getParty() == null) {
							String partyName = p.readString();
							String partyTag = p.readString();
							Party party1 = new Party(player.getWorld());
							party1.setPartyName(partyName);
							party1.setPartyTag(partyTag);

							PartyPlayer partyMember1 = party1.addPlayer(player);
							partyMember1.setRank(PartyRank.LEADER);
							party1.setLeader(partyMember1);

							player.getWorld().getPartyManager().createParty(party1);
							player.message("You have created a party: ");
						}
						if (player.getParty().getInviteSetting() == 1 && !player.getParty().getLeader().getUsername().equalsIgnoreCase(player.getUsername())) {
							player.message("Only the party owner can invite players to this party");
							return;
						}
						if (player.getParty().getInviteSetting() == 2 && !player.getParty().getPlayer(player.getUsername()).getRank().equals(PartyRank.GENERAL) && !player.getParty().getPlayer(player.getUsername()).getRank().equals(PartyRank.LEADER)) {
							player.message("Only the party owner can invite players to this party");
							return;
						}
						if (invited != null) {
							PartyInvite.createPartyInvite(player, invited);
						} else {
							ActionSender.sendBox(player, "Player is not online or could not be found!", false);
							return;
						}
						break;
					case 3:
						if (player.getActivePartyInvite() != null) {
							player.getActivePartyInvite().accept();
						}
						break;
					case 4:
						if (player.getActivePartyInvite() != null) {
							player.getActivePartyInvite().decline();
						}
						break;
					case 5: // kick
						if (player.getParty() != null) {
							String playerToKick = p.readString();
							/*if (!player.getParty().isAllowed(0, player)) {
								player.message("You are not allowed to kick from this party.");
								return;
							}*/
							if (player.getParty().getLeader().getUsername().equals(playerToKick)) {
								player.message("You can't kick the leader of the party.");
								return;
							}
							player.getParty().removePlayer(playerToKick);
						}
						break;
					case 6: // rank
						if (player.getParty() != null) {
							String playerRank = p.readString();
							int rank = p.readByte();
							if (rank >= 3) {
								rank = 0;
							}
							if (!player.getParty().getLeader().getUsername().equals(player.getUsername().replaceAll("_", " "))) {
								player.message("You are not the leader of this party");
								return;
							}
							if (player.getParty().getLeader().getUsername().equals(playerRank)) {
								player.message("You are already the leader of the party");
								return;
							}
							player.getParty().updateRankPlayer(player, playerRank, rank);
						}
						break;
					case 7: // Party SETTINGS
						if (player.getParty() != null) {
							int settingPreference = p.readByte();
							if (settingPreference > 3) {
								return;
							}
							int state = p.readByte();
							if (state >= 3) {
								state = 0;
							}
							if (!player.getParty().getLeader().getUsername().equals(player.getUsername())) {
								player.message("You are not the leader of this party");
								return;
							}
							if (settingPreference == 0) {
								if (player.getParty().getKickSetting() == state) {
									return;
								}
								player.getParty().setKickSetting(state);
							} else if (settingPreference == 1) {
								if (player.getParty().getInviteSetting() == state) {
									return;
								}
								player.getParty().setInviteSetting(state);
							} else if (settingPreference == 2) {
								if (player.getParty().getAllowSearchJoin() == state) {
									return;
								}
								player.getParty().setAllowSearchJoin(state);
							}
							player.message("[PARTY]: You have updated party settings");
							player.getParty().updatePartySettings();

						}
						break;
					case 8:
						ActionSender.sendParties(player);
						break;
					case 9:
						String playerInvited2 = p.readString();
						Player invited2 = player.getWorld().getPlayer(DataConversions.usernameToHash(playerInvited2));

						if(player.getParty() != null){
							if (player.getParty().getInviteSetting() == 1 && !player.getParty().getLeader().getUsername().equalsIgnoreCase(player.getUsername())) {
								player.message("Only the party owner can invite players to this party");
								return;
							}
							if (player.getParty().getInviteSetting() == 2 && !player.getParty().getPlayer(player.getUsername()).getRank().equals(PartyRank.GENERAL) && !player.getParty().getPlayer(player.getUsername()).getRank().equals(PartyRank.LEADER)) {
								player.message("Only the party owner can invite players to this party");
								return;
							}
						}
						if (player.isIronMan(IronmanMode.Ironman.id()) || player.isIronMan(IronmanMode.Ultimate.id())
							|| player.isIronMan(IronmanMode.Hardcore.id()) || player.isIronMan(IronmanMode.Transfer.id())) {
							player.message("You are an Iron Man. You stand alone.");
							return;
						}
						if (player.getParty() == null) {
							if (invited2 == null) {
								ActionSender.sendBox(player,
									"@lre@Party: %"
										+ " %"
										+ "This player is not online or does not exist %"
									, true);
								return;
							}
							if (invited2.equals(player)) {
								ActionSender.sendBox(player,
									"@lre@Party: %"
										+ " %"
										+ "You cannot invite yourself %"
									, true);
								return;
							}
							if (invited2.getParty() != null) {
								if (player.getParty() == invited2.getParty()) {
									player.message(invited2.getUsername() + " is already in your party");
									return;
								} else {
									invited2.message("@yel@" + player.getUsername() + "@whi@ tried to send you a party invite, but you are already in a party");
									ActionSender.sendBox(player,
										"@lre@Party: %"
											+ " %"
											+ invited2.getUsername() + " is already in a party %"
										, true);
									return;
								}
							} else {
								String partyName = p.readString();
								String partyTag = p.readString();
								Party party1 = new Party(player.getWorld());
								party1.setPartyName(partyName);
								party1.setPartyTag(partyTag);

								PartyPlayer partyMember1 = party1.addPlayer(player);
								partyMember1.setRank(PartyRank.LEADER);
								party1.setLeader(partyMember1);

								player.getWorld().getPartyManager().createParty(party1);
								player.message("You have created a party: ");
							}
						} else {
							if (invited2 == null) {
								ActionSender.sendBox(player,
									"@lre@Party: %"
										+ " %"
										+ "This player is not online or does not exist %"
									, true);
								return;
							}
						}
						if (invited2 != null) {
							PartyInvite.createPartyInvite(player, invited2);
						} else {
							ActionSender.sendBox(player, "Player is not online or could not be found!", false);
							return;
						}
						break;
				}
				break;
			case 13:
				int type0 = p.readByte();
				switch (type0) {
					case 0://reduce def
						int amount1 = p.readInt();
						int amountx1 = amount1 * 4;
						if(player.getLocation().inWilderness()){
							player.message("You cannot do that in the wilderness");
							return;
						}
						if(player.getDuel().isDuelActive()){
							player.message("You cannot do that while dueling");
							return;
						}
						if(player.inCombat()){
							player.message("You cannot do that whilst fighting");
							return;
						}
						if (System.currentTimeMillis() - player.getCombatTimer() < 10000){
							player.message("You must be out of combat for 10 seconds before changing stats");
							return;
						}
						if(player.getSkills().getExperience(1) < amountx1){
							player.message("You do not have that much exp in that stat");
							return;
						}
						player.reduceExp(1, amountx1, true);
						player.reduceExp(3, amountx1 / 3, true);
						if(player.getSkills().getMaxStat(3) < 10) {
							player.getSkills().setSkill(3, 10, 4616);
						}
						player.setPoints(player.getPoints() + amount1);
						ActionSender.sendPoints(player);
						player.checkEquipment();
					break;
					case 1://inc def
						int amount = p.readInt();
						int amountx = amount * 4;
						if(player.getLocation().inWilderness()){
							player.message("You cannot do that in the wilderness");
							return;
						}
						if(player.getDuel().isDuelActive()){
							player.message("You cannot do that while dueling");
							return;
						}
						if(player.inCombat()){
							player.message("You cannot do that whilst fighting");
							return;
						}
						if (System.currentTimeMillis() - player.getCombatTimer() < 10000){
							player.message("You must be out of combat for 10 seconds before changing stats");
							return;
						}
						if(player.getPoints() < amount){
							player.message("You do not have enough points");
							return;
						}
						player.incExp2(1, amountx, true);
						player.incExp2(3, amountx / 3, true);
						player.setPoints(player.getPoints() - amount);
						ActionSender.sendPoints(player);
					break;
					case 2://inc atk
						int amount0 = p.readInt();
						int amountx0 = amount0 * 4;
						if(player.getLocation().inWilderness()){
							player.message("You cannot do that in the wilderness");
							return;
						}
						if(player.getDuel().isDuelActive()){
							player.message("You cannot do that while dueling");
							return;
						}
						if(player.inCombat()){
							player.message("You cannot do that whilst fighting");
							return;
						}
						if (System.currentTimeMillis() - player.getCombatTimer() < 10000){
							player.message("You must be out of combat for 10 seconds before changing stats");
							return;
						}
						if(player.getPoints() < amount0){
							player.message("You do not have enough points");
							return;
						}
						player.incExp2(0, amountx0, true);
						player.incExp2(3, amountx0 / 3, true);
						player.setPoints(player.getPoints() - amount0);
						ActionSender.sendPoints(player);
					break;
					case 3://inc str
						int amount2 = p.readInt();
						int amountx2 = amount2 * 4;
						if(player.getLocation().inWilderness()){
							player.message("You cannot do that in the wilderness");
							return;
						}
						if(player.getDuel().isDuelActive()){
							player.message("You cannot do that while dueling");
							return;
						}
						if(player.inCombat()){
							player.message("You cannot do that whilst fighting");
							return;
						}
						if (System.currentTimeMillis() - player.getCombatTimer() < 10000){
							player.message("You must be out of combat for 10 seconds before changing stats");
							return;
						}
						if(player.getPoints() < amount2){
							player.message("You do not have enough points");
							return;
						}
						player.incExp2(2, amountx2, true);
						player.incExp2(3, amountx2 / 3, true);
						player.setPoints(player.getPoints() - amount2);
						ActionSender.sendPoints(player);
					break;
					case 4://inc rng
						int amount3 = p.readInt();
						int amountx3 = amount3 * 4;
						if(player.getLocation().inWilderness()){
							player.message("You cannot do that in the wilderness");
							return;
						}
						if(player.getDuel().isDuelActive()){
							player.message("You cannot do that while dueling");
							return;
						}
						if(player.inCombat()){
							player.message("You cannot do that whilst fighting");
							return;
						}
						if (System.currentTimeMillis() - player.getCombatTimer() < 10000){
							player.message("You must be out of combat for 10 seconds before changing stats");
							return;
						}
						if(player.getPoints() < amount3){
							player.message("You do not have enough points");
							return;
						}
						player.incExp2(4, amountx3, true);
						player.setPoints(player.getPoints() - amount3);
						ActionSender.sendPoints(player);
					break;
					case 5://inc pray
						int amount4 = p.readInt();
						int amountx4 = amount4 * 4;
						if(player.getLocation().inWilderness()){
							player.message("You cannot do that in the wilderness");
							return;
						}
						if(player.getDuel().isDuelActive()){
							player.message("You cannot do that while dueling");
							return;
						}
						if(player.inCombat()){
							player.message("You cannot do that whilst fighting");
							return;
						}
						if (System.currentTimeMillis() - player.getCombatTimer() < 10000){
							player.message("You must be out of combat for 10 seconds before changing stats");
							return;
						}
						if(player.getPoints() < amount4){
							player.message("You do not have enough points");
							return;
						}
						player.incExp2(5, amountx4, true);
						player.setPoints(player.getPoints() - amount4);
						ActionSender.sendPoints(player);
					break;
					case 6://inc mag
						int amount5 = p.readInt();
						int amountx5 = amount5 * 4;
						if(player.getLocation().inWilderness()){
							player.message("You cannot do that in the wilderness");
							return;
						}
						if(player.getDuel().isDuelActive()){
							player.message("You cannot do that while dueling");
							return;
						}
						if(player.inCombat()){
							player.message("You cannot do that whilst fighting");
							return;
						}
						if (System.currentTimeMillis() - player.getCombatTimer() < 10000){
							player.message("You must be out of combat for 10 seconds before changing stats");
							return;
						}
						if(player.getPoints() < amount5){
							player.message("You do not have enough points");
							return;
						}
						player.incExp2(6, amountx5, true);
						player.setPoints(player.getPoints() - amount5);
						ActionSender.sendPoints(player);
					break;
					case 7://reduce atk
						int amount00 = p.readInt();
						int amountx00 = amount00 * 4;
						if(player.getLocation().inWilderness()){
							player.message("You cannot do that in the wilderness");
							return;
						}
						if(player.getDuel().isDuelActive()){
							player.message("You cannot do that while dueling");
							return;
						}
						if(player.inCombat()){
							player.message("You cannot do that whilst fighting");
							return;
						}
						if (System.currentTimeMillis() - player.getCombatTimer() < 10000){
							player.message("You must be out of combat for 10 seconds before changing stats");
							return;
						}
						if(player.getSkills().getExperience(0) < amountx00){
							player.message("You do not have that much exp in that stat");
							return;
						}
						player.reduceExp(0, amountx00, true);
						player.reduceExp(3, amountx00 / 3, true);
						if(player.getSkills().getMaxStat(3) < 10) {
							player.getSkills().setSkill(3, 10, 4616);
						}
						player.setPoints(player.getPoints() + amount00);
						ActionSender.sendPoints(player);
						player.checkEquipment();
					break;
					case 8://reduce str
						int amount22 = p.readInt();
						int amountx22 = amount22 * 4;
						if(player.getLocation().inWilderness()){
							player.message("You cannot do that in the wilderness");
							return;
						}
						if(player.getDuel().isDuelActive()){
							player.message("You cannot do that while dueling");
							return;
						}
						if(player.inCombat()){
							player.message("You cannot do that whilst fighting");
							return;
						}
						if(player.getSkills().getExperience(2) < amountx22){
							player.message("You do not have that much exp in that stat");
							return;
						}
						if (System.currentTimeMillis() - player.getCombatTimer() < 10000){
							player.message("You must be out of combat for 10 seconds before changing stats");
							return;
						}
						player.reduceExp(2, amountx22, true);
						player.reduceExp(3, amountx22 / 3, true);
						if(player.getSkills().getMaxStat(3) < 10) {
							player.getSkills().setSkill(3, 10, 4616);
						}
						player.setPoints(player.getPoints() + amount22);
						ActionSender.sendPoints(player);
						player.checkEquipment();
					break;
					case 9://reduce rng
						int amount33 = p.readInt();
						int amountx33 = amount33 * 4;
						if(player.getLocation().inWilderness()){
							player.message("You cannot do that in the wilderness");
							return;
						}
						if(player.getDuel().isDuelActive()){
							player.message("You cannot do that while dueling");
							return;
						}
						if (System.currentTimeMillis() - player.getCombatTimer() < 10000){
							player.message("You must be out of combat for 10 seconds before changing stats");
							return;
						}
						if(player.inCombat()){
							player.message("You cannot do that whilst fighting");
							return;
						}
						if(player.getSkills().getExperience(4) < amountx33){
							player.message("You do not have that much exp in that stat");
							return;
						}
						player.reduceExp(4, amountx33, true);
						player.setPoints(player.getPoints() + amount33);
						ActionSender.sendPoints(player);
						player.checkEquipment();
					break;
					case 10://reduce pray
						int amount44 = p.readInt();
						int amountx44 = amount44 * 4;
						if(player.getLocation().inWilderness()){
							player.message("You cannot do that in the wilderness");
							return;
						}
						if(player.getDuel().isDuelActive()){
							player.message("You cannot do that while dueling");
							return;
						}
						if (System.currentTimeMillis() - player.getCombatTimer() < 10000){
							player.message("You must be out of combat for 10 seconds before changing stats");
							return;
						}
						if(player.inCombat()){
							player.message("You cannot do that whilst fighting");
							return;
						}
						if(player.getSkills().getExperience(5) < amountx44){
							player.message("You do not have that much exp in that stat");
							return;
						}
						player.reduceExp(5, amountx44, true);
						player.setPoints(player.getPoints() + amount44);
						ActionSender.sendPoints(player);
						player.checkEquipment();
					break;
					case 11://reduce mage
						int amount55 = p.readInt();
						int amountx55 = amount55 * 4;
						if(player.getLocation().inWilderness()){
							player.message("You cannot do that in the wilderness");
							return;
						}
						if(player.getDuel().isDuelActive()){
							player.message("You cannot do that while dueling");
							return;
						}
						if(player.inCombat()){
							player.message("You cannot do that whilst fighting");
							return;
						}
						if (System.currentTimeMillis() - player.getCombatTimer() < 10000){
							player.message("You must be out of combat for 10 seconds before changing stats");
							return;
						}
						if(player.getSkills().getExperience(6) < amountx55){
							player.message("You do not have that much exp in that stat");
							return;
						}
						player.reduceExp(6, amountx55, true);
						player.setPoints(player.getPoints() + amount55);
						ActionSender.sendPoints(player);
						player.checkEquipment();
					break;
					case 12://POINTS2GP
						int amount28 = p.readInt();
						if(player.getDuel().isDuelActive()){
							player.message("You cannot do that while dueling");
							return;
						}
						if(player.inCombat()){
							player.message("You cannot do that whilst fighting");
							return;
						}
						if(player.getPoints() < amount28 * 3){
							player.message("You do not have enough points");
							return;
						}
						Item item = new Item(10, amount28);
						if(player.getInventory().canHold(item)){
							player.getInventory().add(item, false);
							ActionSender.sendInventory(player);
						} else {
							player.getWorld().registerItem(
							new GroundItem(player.getWorld(), 10, player.getX(), player.getY(), amount28, player),
							94000);
							player.message("You don't have room to hold the gp. It falls to the ground!");
						}
						player.setPoints(player.getPoints() - amount28 * 3);
						ActionSender.sendPoints(player);
					break;
					case 13://SAVE PRESET
						int preset = p.readInt();
						if(player.getDuel().isDuelActive()){
							player.message("You cannot do that while dueling");
							return;
						}
						if(player.inCombat()){
							player.message("You cannot do that whilst fighting");
							return;
						}
						if(preset == 1){
							int p1;
							p1 = player.getSkills().getExperience(0) + player.getSkills().getExperience(1) + player.getSkills().getExperience(6) + player.getSkills().getExperience(5) + player.getSkills().getExperience(4) + player.getSkills().getExperience(2);
							player.setP1Total(p1 / 4);
							player.setP10(player.getSkills().getExperience(0));
							player.setP11(player.getSkills().getExperience(1));
							player.setP12(player.getSkills().getExperience(2));
							player.setP13(player.getSkills().getExperience(3));
							player.setP14(player.getSkills().getExperience(4));
							player.setP15(player.getSkills().getExperience(5));
							player.setP16(player.getSkills().getExperience(6));
						} else if(preset == 2){
							int p2;
							p2 = player.getSkills().getExperience(0) + player.getSkills().getExperience(1) + player.getSkills().getExperience(6) + player.getSkills().getExperience(5) + player.getSkills().getExperience(4) + player.getSkills().getExperience(2);
							player.setP2Total(p2 / 4);
							player.setP20(player.getSkills().getExperience(0));
							player.setP21(player.getSkills().getExperience(1));
							player.setP22(player.getSkills().getExperience(2));
							player.setP23(player.getSkills().getExperience(3));
							player.setP24(player.getSkills().getExperience(4));
							player.setP25(player.getSkills().getExperience(5));
							player.setP26(player.getSkills().getExperience(6));
						} else if(preset == 3){
							int p3;
							p3 = player.getSkills().getExperience(0) + player.getSkills().getExperience(1) + player.getSkills().getExperience(6) + player.getSkills().getExperience(5) + player.getSkills().getExperience(4) + player.getSkills().getExperience(2);
							player.setP3Total(p3 / 4);
							player.setP30(player.getSkills().getExperience(0));
							player.setP31(player.getSkills().getExperience(1));
							player.setP32(player.getSkills().getExperience(2));
							player.setP33(player.getSkills().getExperience(3));
							player.setP34(player.getSkills().getExperience(4));
							player.setP35(player.getSkills().getExperience(5));
							player.setP36(player.getSkills().getExperience(6));
						} else if(preset == 4){
							int p4;
							p4 = player.getSkills().getExperience(0) + player.getSkills().getExperience(1) + player.getSkills().getExperience(6) + player.getSkills().getExperience(5) + player.getSkills().getExperience(4) + player.getSkills().getExperience(2);
							player.setP4Total(p4 / 4);
							player.setP40(player.getSkills().getExperience(0));
							player.setP41(player.getSkills().getExperience(1));
							player.setP42(player.getSkills().getExperience(2));
							player.setP43(player.getSkills().getExperience(3));
							player.setP44(player.getSkills().getExperience(4));
							player.setP45(player.getSkills().getExperience(5));
							player.setP46(player.getSkills().getExperience(6));
						} else if(preset == 5){
							int p5;
							p5 = player.getSkills().getExperience(0) + player.getSkills().getExperience(1) + player.getSkills().getExperience(6) + player.getSkills().getExperience(5) + player.getSkills().getExperience(4) + player.getSkills().getExperience(2);
							player.setP5Total(p5 / 4);
							player.setP50(player.getSkills().getExperience(0));
							player.setP51(player.getSkills().getExperience(1));
							player.setP52(player.getSkills().getExperience(2));
							player.setP53(player.getSkills().getExperience(3));
							player.setP54(player.getSkills().getExperience(4));
							player.setP55(player.getSkills().getExperience(5));
							player.setP56(player.getSkills().getExperience(6));
						} else if(preset == 6){
							int p6;
							p6 = player.getSkills().getExperience(0) + player.getSkills().getExperience(1) + player.getSkills().getExperience(6) + player.getSkills().getExperience(5) + player.getSkills().getExperience(4) + player.getSkills().getExperience(2);
							player.setP6Total(p6 / 4);
							player.setP60(player.getSkills().getExperience(0));
							player.setP61(player.getSkills().getExperience(1));
							player.setP62(player.getSkills().getExperience(2));
							player.setP63(player.getSkills().getExperience(3));
							player.setP64(player.getSkills().getExperience(4));
							player.setP65(player.getSkills().getExperience(5));
							player.setP66(player.getSkills().getExperience(6));
						} else {
							player.message("Invalid preset");
							return;
						}
					break;
					case 14://LOAD PRESET 1
						if(player.getDuel().isDuelActive()){
							player.message("You cannot do that while dueling");
							return;
						}
						if(player.getLocation().inWilderness()){
							player.message("You cannot do that in the wilderness");
							return;
						}
						if(player.inCombat()){
							player.message("You cannot do that whilst fighting");
							return;
						}
						if(player.getSkills().getExperience(0) > 0){
							player.message("You must have no experience in any stat in order to apply a preset");
							return;
						}
						if(player.getSkills().getExperience(1) > 0){
							player.message("You must have no experience in any stat in order to apply a preset");
							return;
						}
						if(player.getSkills().getExperience(2) > 0){
							player.message("You must have no experience in any stat in order to apply a preset");
							return;
						}
						if(player.getSkills().getExperience(4) > 0){
							player.message("You must have no experience in any stat in order to apply a preset");
							return;
						}
						if(player.getSkills().getExperience(5) > 0){
							player.message("You must have no experience in any stat in order to apply a preset");
							return;
						}
						if(player.getSkills().getExperience(6) > 0){
							player.message("You must have no experience in any stat in order to apply a preset");
							return;
						}
						if (System.currentTimeMillis() - player.getCombatTimer() < 10000){
							player.message("You must be out of combat for 10 seconds before changing stats");
							return;
						}
						if(player.getP1Total() > player.getPoints()){
							player.message("You do not have enough points");
							return;
						}
						player.getSkills().setExperience(0, player.getP10());
						player.getSkills().setExperience(1, player.getP11());
						player.getSkills().setExperience(2, player.getP12());
						player.getSkills().setExperience(3, player.getP13());
						if(player.getSkills().getMaxStat(3) < 10) {
							player.getSkills().setSkill(3, 10, 4616);
						}
						player.getSkills().setExperience(4, player.getP14());
						player.getSkills().setExperience(5, player.getP15());
						player.getSkills().setExperience(6, player.getP16());
						player.getSkills().setLevelTo2(0, player.getSkills().getMaxStat(0));
						player.getSkills().setLevelTo2(1, player.getSkills().getMaxStat(1));
						player.getSkills().setLevelTo2(2, player.getSkills().getMaxStat(2));
						player.getSkills().setLevelTo2(3, player.getSkills().getMaxStat(3));
						player.getSkills().setLevelTo2(4, player.getSkills().getMaxStat(4));
						player.getSkills().setLevelTo2(5, player.getSkills().getMaxStat(5));
						player.getSkills().setLevelTo2(6, player.getSkills().getMaxStat(6));
						player.checkEquipment();
						player.getSkills().sendUpdateAll();
						player.setPoints(player.getPoints() - player.getP1Total());
						ActionSender.sendPoints(player);
					break;
					case 15://LOAD PRESET 2
						if(player.getDuel().isDuelActive()){
							player.message("You cannot do that while dueling");
							return;
						}
						if(player.getLocation().inWilderness()){
							player.message("You cannot do that in the wilderness");
							return;
						}
						if(player.inCombat()){
							player.message("You cannot do that whilst fighting");
							return;
						}
						if(player.getSkills().getExperience(0) > 0){
							player.message("You must have no experience in any stat in order to apply a preset");
							return;
						}
						if(player.getSkills().getExperience(1) > 0){
							player.message("You must have no experience in any stat in order to apply a preset");
							return;
						}
						if(player.getSkills().getExperience(2) > 0){
							player.message("You must have no experience in any stat in order to apply a preset");
							return;
						}
						if(player.getSkills().getExperience(4) > 0){
							player.message("You must have no experience in any stat in order to apply a preset");
							return;
						}
						if(player.getSkills().getExperience(5) > 0){
							player.message("You must have no experience in any stat in order to apply a preset");
							return;
						}
						if(player.getSkills().getExperience(6) > 0){
							player.message("You must have no experience in any stat in order to apply a preset");
							return;
						}
						if (System.currentTimeMillis() - player.getCombatTimer() < 10000){
							player.message("You must be out of combat for 10 seconds before changing stats");
							return;
						}
						if(player.getP2Total() > player.getPoints()){
							player.message("You do not have enough points");
							return;
						}
						player.getSkills().setExperience(0, player.getP20());
						player.getSkills().setExperience(1, player.getP21());
						player.getSkills().setExperience(2, player.getP22());
						player.getSkills().setExperience(3, player.getP23());
						if(player.getSkills().getMaxStat(3) < 10) {
							player.getSkills().setSkill(3, 10, 4616);
						}
						player.getSkills().setExperience(4, player.getP24());
						player.getSkills().setExperience(5, player.getP25());
						player.getSkills().setExperience(6, player.getP26());
						player.getSkills().setLevelTo2(0, player.getSkills().getMaxStat(0));
						player.getSkills().setLevelTo2(1, player.getSkills().getMaxStat(1));
						player.getSkills().setLevelTo2(2, player.getSkills().getMaxStat(2));
						player.getSkills().setLevelTo2(3, player.getSkills().getMaxStat(3));
						player.getSkills().setLevelTo2(4, player.getSkills().getMaxStat(4));
						player.getSkills().setLevelTo2(5, player.getSkills().getMaxStat(5));
						player.getSkills().setLevelTo2(6, player.getSkills().getMaxStat(6));
						player.checkEquipment();
						player.getSkills().sendUpdateAll();
						player.setPoints(player.getPoints() - player.getP2Total());
						ActionSender.sendPoints(player);
					break;
					case 16://LOAD PRESET 3
						if(player.getDuel().isDuelActive()){
							player.message("You cannot do that while dueling");
							return;
						}
						if(player.getLocation().inWilderness()){
							player.message("You cannot do that in the wilderness");
							return;
						}
						if(player.inCombat()){
							player.message("You cannot do that whilst fighting");
							return;
						}
						if(player.getSkills().getExperience(0) > 0){
							player.message("You must have no experience in any stat in order to apply a preset");
							return;
						}
						if(player.getSkills().getExperience(1) > 0){
							player.message("You must have no experience in any stat in order to apply a preset");
							return;
						}
						if(player.getSkills().getExperience(2) > 0){
							player.message("You must have no experience in any stat in order to apply a preset");
							return;
						}
						if(player.getSkills().getExperience(4) > 0){
							player.message("You must have no experience in any stat in order to apply a preset");
							return;
						}
						if(player.getSkills().getExperience(5) > 0){
							player.message("You must have no experience in any stat in order to apply a preset");
							return;
						}
						if(player.getSkills().getExperience(6) > 0){
							player.message("You must have no experience in any stat in order to apply a preset");
							return;
						}
						if (System.currentTimeMillis() - player.getCombatTimer() < 10000){
							player.message("You must be out of combat for 10 seconds before changing stats");
							return;
						}
						if(player.getP3Total() > player.getPoints()){
							player.message("You do not have enough points");
							return;
						}
						player.getSkills().setExperience(0, player.getP30());
						player.getSkills().setExperience(1, player.getP31());
						player.getSkills().setExperience(2, player.getP32());
						player.getSkills().setExperience(3, player.getP33());
						if(player.getSkills().getMaxStat(3) < 10) {
							player.getSkills().setSkill(3, 10, 4616);
						}
						player.getSkills().setExperience(4, player.getP34());
						player.getSkills().setExperience(5, player.getP35());
						player.getSkills().setExperience(6, player.getP36());
						player.getSkills().setLevelTo2(0, player.getSkills().getMaxStat(0));
						player.getSkills().setLevelTo2(1, player.getSkills().getMaxStat(1));
						player.getSkills().setLevelTo2(2, player.getSkills().getMaxStat(2));
						player.getSkills().setLevelTo2(3, player.getSkills().getMaxStat(3));
						player.getSkills().setLevelTo2(4, player.getSkills().getMaxStat(4));
						player.getSkills().setLevelTo2(5, player.getSkills().getMaxStat(5));
						player.getSkills().setLevelTo2(6, player.getSkills().getMaxStat(6));
						player.checkEquipment();
						player.getSkills().sendUpdateAll();
						player.setPoints(player.getPoints() - player.getP3Total());
						ActionSender.sendPoints(player);
					break;
					case 17://LOAD PRESET 4
						if(player.getDuel().isDuelActive()){
							player.message("You cannot do that while dueling");
							return;
						}
						if(player.getLocation().inWilderness()){
							player.message("You cannot do that in the wilderness");
							return;
						}
						if(player.inCombat()){
							player.message("You cannot do that whilst fighting");
							return;
						}
						if(player.getSkills().getExperience(0) > 0){
							player.message("You must have no experience in any stat in order to apply a preset");
							return;
						}
						if(player.getSkills().getExperience(1) > 0){
							player.message("You must have no experience in any stat in order to apply a preset");
							return;
						}
						if(player.getSkills().getExperience(2) > 0){
							player.message("You must have no experience in any stat in order to apply a preset");
							return;
						}
						if(player.getSkills().getExperience(4) > 0){
							player.message("You must have no experience in any stat in order to apply a preset");
							return;
						}
						if(player.getSkills().getExperience(5) > 0){
							player.message("You must have no experience in any stat in order to apply a preset");
							return;
						}
						if(player.getSkills().getExperience(6) > 0){
							player.message("You must have no experience in any stat in order to apply a preset");
							return;
						}
						if (System.currentTimeMillis() - player.getCombatTimer() < 10000){
							player.message("You must be out of combat for 10 seconds before changing stats");
							return;
						}
						if(player.getP4Total() > player.getPoints()){
							player.message("You do not have enough points");
							return;
						}
						player.getSkills().setExperience(0, player.getP40());
						player.getSkills().setExperience(1, player.getP41());
						player.getSkills().setExperience(2, player.getP42());
						player.getSkills().setExperience(3, player.getP43());
						if(player.getSkills().getMaxStat(3) < 10) {
							player.getSkills().setSkill(3, 10, 4616);
						}
						player.getSkills().setExperience(4, player.getP44());
						player.getSkills().setExperience(5, player.getP45());
						player.getSkills().setExperience(6, player.getP46());
						player.getSkills().setLevelTo2(0, player.getSkills().getMaxStat(0));
						player.getSkills().setLevelTo2(1, player.getSkills().getMaxStat(1));
						player.getSkills().setLevelTo2(2, player.getSkills().getMaxStat(2));
						player.getSkills().setLevelTo2(3, player.getSkills().getMaxStat(3));
						player.getSkills().setLevelTo2(4, player.getSkills().getMaxStat(4));
						player.getSkills().setLevelTo2(5, player.getSkills().getMaxStat(5));
						player.getSkills().setLevelTo2(6, player.getSkills().getMaxStat(6));
						player.checkEquipment();
						player.getSkills().sendUpdateAll();
						player.setPoints(player.getPoints() - player.getP4Total());
						ActionSender.sendPoints(player);
					break;
					case 18://LOAD PRESET 5
						if(player.getDuel().isDuelActive()){
							player.message("You cannot do that while dueling");
							return;
						}
						if(player.getLocation().inWilderness()){
							player.message("You cannot do that in the wilderness");
							return;
						}
						if(player.inCombat()){
							player.message("You cannot do that whilst fighting");
							return;
						}
						if(player.getSkills().getExperience(0) > 0){
							player.message("You must have no experience in any stat in order to apply a preset");
							return;
						}
						if(player.getSkills().getExperience(1) > 0){
							player.message("You must have no experience in any stat in order to apply a preset");
							return;
						}
						if(player.getSkills().getExperience(2) > 0){
							player.message("You must have no experience in any stat in order to apply a preset");
							return;
						}
						if(player.getSkills().getExperience(4) > 0){
							player.message("You must have no experience in any stat in order to apply a preset");
							return;
						}
						if(player.getSkills().getExperience(5) > 0){
							player.message("You must have no experience in any stat in order to apply a preset");
							return;
						}
						if(player.getSkills().getExperience(6) > 0){
							player.message("You must have no experience in any stat in order to apply a preset");
							return;
						}
						if (System.currentTimeMillis() - player.getCombatTimer() < 10000){
							player.message("You must be out of combat for 10 seconds before changing stats");
							return;
						}
						if(player.getP5Total() > player.getPoints()){
							player.message("You do not have enough points");
							return;
						}
						player.getSkills().setExperience(0, player.getP50());
						player.getSkills().setExperience(1, player.getP51());
						player.getSkills().setExperience(2, player.getP52());
						player.getSkills().setExperience(3, player.getP53());
						if(player.getSkills().getMaxStat(3) < 10) {
							player.getSkills().setSkill(3, 10, 4616);
						}
						player.getSkills().setExperience(4, player.getP54());
						player.getSkills().setExperience(5, player.getP55());
						player.getSkills().setExperience(6, player.getP56());
						player.getSkills().setLevelTo2(0, player.getSkills().getMaxStat(0));
						player.getSkills().setLevelTo2(1, player.getSkills().getMaxStat(1));
						player.getSkills().setLevelTo2(2, player.getSkills().getMaxStat(2));
						player.getSkills().setLevelTo2(3, player.getSkills().getMaxStat(3));
						player.getSkills().setLevelTo2(4, player.getSkills().getMaxStat(4));
						player.getSkills().setLevelTo2(5, player.getSkills().getMaxStat(5));
						player.getSkills().setLevelTo2(6, player.getSkills().getMaxStat(6));
						player.checkEquipment();
						player.getSkills().sendUpdateAll();
						player.setPoints(player.getPoints() - player.getP5Total());
						ActionSender.sendPoints(player);
					break;
					case 19://LOAD PRESET 6
						if(player.getDuel().isDuelActive()){
							player.message("You cannot do that while dueling");
							return;
						}
						if(player.getLocation().inWilderness()){
							player.message("You cannot do that in the wilderness");
							return;
						}
						if(player.inCombat()){
							player.message("You cannot do that whilst fighting");
							return;
						}
						if(player.getSkills().getExperience(0) > 0){
							player.message("You must have no experience in any stat in order to apply a preset");
							return;
						}
						if(player.getSkills().getExperience(1) > 0){
							player.message("You must have no experience in any stat in order to apply a preset");
							return;
						}
						if(player.getSkills().getExperience(2) > 0){
							player.message("You must have no experience in any stat in order to apply a preset");
							return;
						}
						if(player.getSkills().getExperience(4) > 0){
							player.message("You must have no experience in any stat in order to apply a preset");
							return;
						}
						if(player.getSkills().getExperience(5) > 0){
							player.message("You must have no experience in any stat in order to apply a preset");
							return;
						}
						if(player.getSkills().getExperience(6) > 0){
							player.message("You must have no experience in any stat in order to apply a preset");
							return;
						}
						if (System.currentTimeMillis() - player.getCombatTimer() < 10000){
							player.message("You must be out of combat for 10 seconds before changing stats");
							return;
						}
						if(player.getP6Total() > player.getPoints()){
							player.message("You do not have enough points");
							return;
						}
						player.getSkills().setExperience(0, player.getP60());
						player.getSkills().setExperience(1, player.getP61());
						player.getSkills().setExperience(2, player.getP62());
						player.getSkills().setExperience(3, player.getP63());
						if(player.getSkills().getMaxStat(3) < 10) {
							player.getSkills().setSkill(3, 10, 4616);
						}
						player.getSkills().setExperience(4, player.getP64());
						player.getSkills().setExperience(5, player.getP65());
						player.getSkills().setExperience(6, player.getP66());
						player.getSkills().setLevelTo2(0, player.getSkills().getMaxStat(0));
						player.getSkills().setLevelTo2(1, player.getSkills().getMaxStat(1));
						player.getSkills().setLevelTo2(2, player.getSkills().getMaxStat(2));
						player.getSkills().setLevelTo2(3, player.getSkills().getMaxStat(3));
						player.getSkills().setLevelTo2(4, player.getSkills().getMaxStat(4));
						player.getSkills().setLevelTo2(5, player.getSkills().getMaxStat(5));
						player.getSkills().setLevelTo2(6, player.getSkills().getMaxStat(6));
						player.checkEquipment();
						player.getSkills().sendUpdateAll();
						player.setPoints(player.getPoints() - player.getP6Total());
						ActionSender.sendPoints(player);
					break;
				}
				break;
			case 14:
				int type1128 = p.readByte();
				switch (type1128) {
					case 0:
						player.setF2OppName(0);
						ActionSender.sendF2OppName(player);
						break;
					case 1:
						player.setF2OppName(1);
						ActionSender.sendF2OppName(player);
						break;
					case 2:
						player.setF2Hits(0);
						ActionSender.sendF2Hits(player);
						break;
					case 3:
						player.setF2Hits(1);
						ActionSender.sendF2Hits(player);
						break;
					case 4:
						player.setF2Fatigue(0);
						ActionSender.sendF2Fatigue(player);
						break;
					case 5:
						player.setF2Fatigue(1);
						ActionSender.sendF2Fatigue(player);
						break;
					case 6:
						player.setF2Prayer(0);
						ActionSender.sendF2Prayer(player);
						break;
					case 7:
						player.setF2Prayer(1);
						ActionSender.sendF2Prayer(player);
						break;
					case 8:
						player.setF2Date(0);
						ActionSender.sendF2Date(player);
						break;
					case 9:
						player.setF2Date(1);
						ActionSender.sendF2Date(player);
						break;
					case 10:
						player.setOp(0);
						ActionSender.sendOp(player);
						break;
					case 11:
						player.setOp(1);
						ActionSender.sendOp(player);
						break;
					case 12:
						player.setF2KillStreak(0);
						ActionSender.sendF2KillStreak(player);
						break;
					case 13:
						player.setF2KillStreak(1);
						ActionSender.sendF2KillStreak(player);
						break;
					case 14:
						player.setWantF2SideMenu(0);
						ActionSender.sendWantF2SideMenu(player);
						break;
					case 15:
						player.setWantF2SideMenu(1);
						ActionSender.sendWantF2SideMenu(player);
						break;
				}
			break;
			case 15:
				int t2 = p.readByte();
				switch (t2) {
					case 0:
					int t0 = p.readInt();
						if(player.getWorld().getTdmInProgress() > 0) {
							player.message("TDM is currently in progress. You must wait until it is finished.");
							return;
						}
						if(t0 > 123){
							player.message("123 is the maximum combat level");
							return;
						}
						if(t0 < 3){
							player.message("3 is the minimum combat level");
							return;
						}
						player.getWorld().setTdmCbLvl(t0);
						ActionSender.sendTdmCbLvl(player);
						for (Player p2 : player.getWorld().getPlayers()) {
							ActionSender.sendTdmCbLvl(p2);
						}
						break;
					case 1:
					int t11 = p.readInt();
						if(player.getWorld().getTdmInProgress() > 0) {
							player.message("TDM is currently in progress. You must wait until it is finished.");
							return;
						}
						if(t11 > 50){
							player.message("50 is the maximum kill limit");
							return;
						}
						if(t11 < 10){
							player.message("10 is the minimum kill limit");
							return;
						}
						player.getWorld().setTdmKillLimit(t11);
						ActionSender.sendTdmKillLimit(player);
						for (Player p3 : player.getWorld().getPlayers()) {
							ActionSender.sendTdmKillLimit(p3);
						}
						break;
					case 2:
						player.setF2Hits(0);
						ActionSender.sendF2Hits(player);
						break;
					case 3:
						player.setF2Hits(1);
						ActionSender.sendF2Hits(player);
						break;
					case 4:
						player.setF2Fatigue(0);
						ActionSender.sendF2Fatigue(player);
						break;
					case 5:
						player.setF2Fatigue(1);
						ActionSender.sendF2Fatigue(player);
						break;
					case 6:
						player.setF2Prayer(0);
						ActionSender.sendF2Prayer(player);
						break;
					case 7:
						player.setF2Prayer(1);
						ActionSender.sendF2Prayer(player);
						break;
					case 8:
						player.setF2Date(0);
						ActionSender.sendF2Date(player);
						break;
					case 9:
						player.setF2Date(1);
						ActionSender.sendF2Date(player);
						break;
					case 10:
						player.setOp(0);
						ActionSender.sendOp(player);
						break;
					case 11:
						player.setOp(1);
						ActionSender.sendOp(player);
						break;
					case 12:
						player.setF2KillStreak(0);
						ActionSender.sendF2KillStreak(player);
						break;
					case 13:
						player.setF2KillStreak(1);
						ActionSender.sendF2KillStreak(player);
						break;
				}
			break;
		}
	}
}
