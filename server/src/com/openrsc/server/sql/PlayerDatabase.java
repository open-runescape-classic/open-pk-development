package com.openrsc.server.sql;

import com.openrsc.server.Server;
import com.openrsc.server.external.ItemDefinition;
import com.openrsc.server.login.LoginRequest;
import com.openrsc.server.model.PlayerAppearance;
import com.openrsc.server.model.Point;
import com.openrsc.server.model.container.Bank;
import com.openrsc.server.model.container.Equipment;
import com.openrsc.server.model.container.Inventory;
import com.openrsc.server.model.container.Item;
import com.openrsc.server.model.entity.npc.Npc;
import com.openrsc.server.model.entity.player.Player;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.sql.Blob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public class PlayerDatabase {

	/**
	 * The asynchronous logger.
	 */
	private static final Logger LOGGER = LogManager.getLogger();
	private static final String[] gameSettings = {"cameraauto", "onemouse", "soundoff"};
	private static final String[] privacySettings = {"block_chat", "block_private", "block_trade",
		"block_duel"};
	private final DatabaseConnection conn;
	private boolean useTransactions = false;

	private final Server server;

	public final Server getServer() {
		return server;
	}

	public PlayerDatabase(Server server) {
		this.server = server;
		conn = new DatabaseConnection(getServer(), "PlayerDatabase");
		/**
		 * This prevents the connection from committing automatically every
		 * query. It's here so we can cache update queries, enabling us to
		 * make one batch update with all of the update queries nested in
		 * between `START TRANSACTION' query and `COMMIT' query. `COMMIT'
		 * query will commit all of the update queries made after the last
		 * `START TRANSACTION' query.
		 */
		getDatabaseConnection().executeUpdate("UPDATE `" + getServer().getConfig().MYSQL_TABLE_PREFIX + "players` SET `online`='0' WHERE online='1'");
	}

	public DatabaseConnection getDatabaseConnection() {
		return conn;
	}

	public boolean savePlayer(Player s) {
		if(s.isBot){
			LOGGER.error("SKIPPING SAVING: " + s.getUsername());
			return false;
		}
		if (!playerExists(s.getDatabaseID())) {
			LOGGER.error("ERROR SAVING: " + s.getUsername());
			return false;
		}

		PreparedStatement statement;
		try {
			if (useTransactions)
				getDatabaseConnection().executeQuery("START TRANSACTION");

			updateLongs(getDatabaseConnection().getGameQueries().save_DeleteBank, s.getDatabaseID());
			if (s.getBank().size() > 0) {
				statement = getDatabaseConnection().prepareStatement(getDatabaseConnection().getGameQueries().save_AddBank);
				int slot = 0;
				for (Item item : s.getBank().getItems()) {
					statement.setInt(1, s.getDatabaseID());
					statement.setInt(2, item.getID());
					statement.setInt(3, item.getAmount());
					statement.setInt(4, slot++);
					statement.addBatch();
				}
				statement.executeBatch();
			}

			if (getServer().getConfig().WANT_BANK_PRESETS) {
				statement = getDatabaseConnection().prepareStatement(getDatabaseConnection().getGameQueries().save_AddBankPreset);
				for (int k = 0; k < Bank.PRESET_COUNT; k++) {
					if (s.getBank().presets[k].changed) {
						updateLongs(getDatabaseConnection().getGameQueries().save_DeleteBankPresets, s.getDatabaseID(), k);
						ByteArrayOutputStream inventoryBuffer = new ByteArrayOutputStream();
						DataOutputStream inventoryWriter = new DataOutputStream(inventoryBuffer);
						for (Item inventoryItem : s.getBank().presets[k].inventory) {
							if (inventoryItem.getID() == -1)
								inventoryWriter.writeByte(-1);
							else {
								inventoryWriter.writeShort(inventoryItem.getID());
								if (inventoryItem.getDef(s.getWorld()) != null && inventoryItem.getDef(s.getWorld()).isStackable())
									inventoryWriter.writeInt(inventoryItem.getAmount());
							}

						}
						inventoryWriter.close();
						Blob inventoryBlob = new javax.sql.rowset.serial.SerialBlob(inventoryBuffer.toByteArray());

						ByteArrayOutputStream equipmentBuffer = new ByteArrayOutputStream();
						DataOutputStream equipmentWriter = new DataOutputStream(equipmentBuffer);
						for (Item equipmentItem : s.getBank().presets[k].equipment) {
							if (equipmentItem.getID() == -1)
								equipmentWriter.writeByte(-1);
							else {
								equipmentWriter.writeShort(equipmentItem.getID());
								if (equipmentItem.getDef(s.getWorld()) != null && equipmentItem.getDef(s.getWorld()).isStackable())
									equipmentWriter.writeInt(equipmentItem.getAmount());
							}

						}
						equipmentWriter.close();
						Blob equipmentBlob = new javax.sql.rowset.serial.SerialBlob(equipmentBuffer.toByteArray());
						statement.setInt(1, s.getDatabaseID());
						statement.setInt(2, k);
						statement.setBlob(3, inventoryBlob);
						statement.setBlob(4, equipmentBlob);
						statement.addBatch();
						statement.executeBatch();
					}
				}
			}

			updateLongs(getDatabaseConnection().getGameQueries().save_DeleteInv, s.getDatabaseID());
			if (s.getInventory().size() > 0) {
				statement = getDatabaseConnection().prepareStatement(getDatabaseConnection().getGameQueries().save_AddInvItem);
				int slot = 0;
				for (Item item : s.getInventory().getItems()) {
					statement.setInt(1, s.getDatabaseID());
					statement.setInt(2, item.getID());
					statement.setInt(3, item.getAmount());
					statement.setInt(4, (item.isWielded() ? 1 : 0));
					statement.setInt(5, slot++);
					statement.addBatch();
				}
				statement.executeBatch();
			}

			if (getServer().getConfig().WANT_EQUIPMENT_TAB) {
				updateLongs(getDatabaseConnection().getGameQueries().save_DeleteEquip, s.getDatabaseID());
				statement = getDatabaseConnection().prepareStatement(getDatabaseConnection().getGameQueries().save_SaveEquip);
				Item item;
				for (int i = 0; i < Equipment.slots; i++) {
					item = s.getEquipment().get(i);
					if (item != null) {
						statement.setInt(1, s.getDatabaseID());
						statement.setInt(2, item.getID());
						statement.setInt(3, item.getAmount());
						statement.addBatch();
					}
				}
				statement.executeBatch();
			}


			updateLongs(getDatabaseConnection().getGameQueries().save_DeleteQuests, s.getDatabaseID());
			if (s.getQuestStages().size() > 0) {
				statement = getDatabaseConnection().prepareStatement(getDatabaseConnection().getGameQueries().save_AddQuest);
				Set<Integer> keys = s.getQuestStages().keySet();
				for (int id : keys) {
					statement.setInt(1, s.getDatabaseID());
					statement.setInt(2, id);
					statement.setInt(3, s.getQuestStage(id));
					statement.addBatch();
				}
				statement.executeBatch();
			}

			s.getCache().store("last_spell_cast", s.getCastTimer());

			updateLongs(getDatabaseConnection().getGameQueries().save_DeleteCache, s.getDatabaseID());
			if (s.getCache().getCacheMap().size() > 0) {
				statement = getDatabaseConnection().prepareStatement(
					"INSERT INTO `" + getDatabaseConnection().getGameQueries().PREFIX + "player_cache` (`playerID`, `type`, `key`, `value`) VALUES(?,?,?,?)");

				for (String key : s.getCache().getCacheMap().keySet()) {
					Object o = s.getCache().getCacheMap().get(key);
					if (o instanceof Integer) {
						statement.setInt(1, s.getDatabaseID());
						statement.setInt(2, 0);
						statement.setString(3, key);
						statement.setInt(4, (Integer) o);
						statement.addBatch();
					}
					if (o instanceof String) {
						statement.setInt(1, s.getDatabaseID());
						statement.setInt(2, 1);
						statement.setString(3, key);
						statement.setString(4, (String) o);
						statement.addBatch();

					}
					if (o instanceof Boolean) {
						statement.setInt(1, s.getDatabaseID());
						statement.setInt(2, 2);
						statement.setString(3, key);
						statement.setInt(4, ((Boolean) o) ? 1 : 0);
						statement.addBatch();
					}
					if (o instanceof Long) {
						statement.setInt(1, s.getDatabaseID());
						statement.setInt(2, 3);
						statement.setString(3, key);
						statement.setLong(4, ((Long) o));
						statement.addBatch();
					}
					statement.executeBatch();
				}
			}

			if (s.getKillCacheUpdated()) {
				savePlayersNPCKills(s);
			}

			statement = getDatabaseConnection().prepareStatement(getDatabaseConnection().getGameQueries().save_UpdateBasicInfo);
			statement.setInt(1, s.getCombatLevel());
			statement.setInt(2, s.getSkills().getTotalLevel());
			statement.setInt(3, s.getX());
			statement.setInt(4, s.getY());
			statement.setInt(5, s.getFatigue());
			statement.setLong(6, s.getKills());
			statement.setLong(7, s.getDeaths());
			statement.setLong(8, s.getKills());
			statement.setLong(9, s.getPkPoints());
			statement.setLong(10, s.getPoints());
			statement.setInt(11, s.getF2OppName());
			statement.setInt(12, s.getF2Hits());
			statement.setInt(13, s.getF2Fatigue());
			statement.setInt(14, s.getF2Prayer());
			statement.setInt(15, s.getF2Date());
			statement.setInt(16, s.getF2KillStreak());
			statement.setInt(17, s.getOp());
			statement.setLong(18, s.getClanPoints());
			statement.setLong(19, s.getP1Total());
			statement.setLong(20, s.getP2Total());
			statement.setLong(21, s.getP3Total());
			statement.setLong(22, s.getP4Total());
			statement.setLong(23, s.getP5Total());
			statement.setLong(24, s.getP6Total());
			statement.setLong(25, s.getP10());
			statement.setLong(26, s.getP11());
			statement.setLong(27, s.getP12());
			statement.setLong(28, s.getP13());
			statement.setLong(29, s.getP14());
			statement.setLong(30, s.getP15());
			statement.setLong(31, s.getP16());
			statement.setLong(32, s.getP20());
			statement.setLong(33, s.getP21());
			statement.setLong(34, s.getP22());
			statement.setLong(35, s.getP23());
			statement.setLong(36, s.getP24());
			statement.setLong(37, s.getP25());
			statement.setLong(38, s.getP26());
			statement.setLong(39, s.getP30());
			statement.setLong(40, s.getP31());
			statement.setLong(41, s.getP32());
			statement.setLong(42, s.getP33());
			statement.setLong(43, s.getP34());
			statement.setLong(44, s.getP35());
			statement.setLong(45, s.getP36());
			statement.setLong(46, s.getP40());
			statement.setLong(47, s.getP41());
			statement.setLong(48, s.getP42());
			statement.setLong(49, s.getP43());
			statement.setLong(50, s.getP44());
			statement.setLong(51, s.getP45());
			statement.setLong(52, s.getP46());
			statement.setLong(53, s.getP50());
			statement.setLong(54, s.getP51());
			statement.setLong(55, s.getP52());
			statement.setLong(56, s.getP53());
			statement.setLong(57, s.getP54());
			statement.setLong(58, s.getP55());
			statement.setLong(59, s.getP56());
			statement.setLong(60, s.getP60());
			statement.setLong(61, s.getP61());
			statement.setLong(62, s.getP62());
			statement.setLong(63, s.getP63());
			statement.setLong(64, s.getP64());
			statement.setLong(65, s.getP65());
			statement.setLong(66, s.getP66());
			statement.setLong(67, s.getWantLoginBox());
			statement.setLong(68, s.getWantOgCombatStyleBox());
			statement.setLong(69, s.getWantF2SideMenu());
			statement.setInt(70, s.getIronMan());
			statement.setInt(71, s.getIronManRestriction());
			statement.setInt(72, s.getHCIronmanDeath());
			statement.setInt(73, s.calculateQuestPoints());
			statement.setInt(74, s.getSettings().getAppearance().getHairColour());
			statement.setInt(75, s.getSettings().getAppearance().getTopColour());
			statement.setInt(76, s.getSettings().getAppearance().getTrouserColour());
			statement.setInt(77, s.getSettings().getAppearance().getSkinColour());
			statement.setInt(78, s.getSettings().getAppearance().getHead());
			statement.setInt(79, s.getSettings().getAppearance().getBody());
			statement.setInt(80, s.isMale() ? 1 : 0);
			statement.setLong(81, s.getClanKills());
			statement.setLong(82, s.getClanDeaths());
			statement.setInt(83, s.getCombatStyle());
			statement.setLong(84, s.getMuteExpires());
			statement.setLong(85, s.getBankSize());
			statement.setLong(86, s.getGroupID());
			statement.setInt(87, s.getDatabaseID());
			statement.executeUpdate();

			// PRIVACY SETTINGS
			setPrivacySettings(s.getSettings().getPrivacySettings(), s.getDatabaseID());

			// GAME SETTINGS
			setGameSettings(s.getSettings().getGameSettings(), s.getDatabaseID());

			statement = getDatabaseConnection().prepareStatement(getDatabaseConnection().getGameQueries().updateExperience);
			statement.setInt(getServer().getConstants().getSkills().getSkillsCount() + 1, s.getDatabaseID());
			for (int index = 0; index < getServer().getConstants().getSkills().getSkillsCount(); index++)
				statement.setInt(index + 1, s.getSkills().getExperience(index));
			statement.executeUpdate();

			statement = getDatabaseConnection().prepareStatement(getDatabaseConnection().getGameQueries().updateStats);
			statement.setInt(getServer().getConstants().getSkills().getSkillsCount() + 1, s.getDatabaseID());
			for (int index = 0; index < getServer().getConstants().getSkills().getSkillsCount(); index++)
				statement.setInt(index + 1, s.getSkills().getLevel(index));
			statement.executeUpdate();

			if (useTransactions)
				getDatabaseConnection().executeQuery("COMMIT");
			return true;
		} catch (Exception e) {
			LOGGER.catching(e);
			return false;
		}
	}

	private boolean usernameToId(String username) {
		ResultSet result = null;
		try {
			if (useTransactions)
				getDatabaseConnection().executeQuery("START TRANSACTION");
			result = resultSetFromString(getDatabaseConnection().getGameQueries().userToId, username);
			if (useTransactions)
				getDatabaseConnection().executeQuery("COMMIT");
			return !result.isBeforeFirst();
		} catch (Exception e) {
			LOGGER.catching(e);
		}
		return true;
	}

	public boolean addFriend(int playerID, long friend, String friendName) {
		if (usernameToId(friendName)) return false;
		try {
			if (useTransactions)
				getDatabaseConnection().executeQuery("START TRANSACTION");
			PreparedStatement prepared = getDatabaseConnection().prepareStatement(getDatabaseConnection().getGameQueries().addFriend);
			prepared.setInt(1, playerID);
			prepared.setLong(2, friend);
			prepared.setString(3, friendName);
			prepared.executeUpdate();
			if (useTransactions)
				getDatabaseConnection().executeQuery("COMMIT");
			return true;
		} catch (Exception e) {
			LOGGER.catching(e);
		}
		return false;
	}

	public void removeFriend(int playerID, long friend) {
		try {
			if (useTransactions)
				getDatabaseConnection().executeQuery("START TRANSACTION");
			PreparedStatement prepared = getDatabaseConnection().prepareStatement(getDatabaseConnection().getGameQueries().removeFriend);
			prepared.setInt(1, playerID);
			prepared.setLong(2, friend);
			prepared.executeUpdate();
			if (useTransactions)
				getDatabaseConnection().executeQuery("COMMIT");
		} catch (Exception e) {
			LOGGER.catching(e);
		}
	}

	public boolean addIgnore(int playerID, long friend, String friendName) {
		if (usernameToId(friendName)) return false;
		try {
			if (useTransactions)
				getDatabaseConnection().executeQuery("START TRANSACTION");
			PreparedStatement prepared = getDatabaseConnection().prepareStatement(getDatabaseConnection().getGameQueries().addIgnore);
			prepared.setInt(1, playerID);
			prepared.setLong(2, friend);
			prepared.executeUpdate();
			if (useTransactions)
				getDatabaseConnection().executeQuery("COMMIT");
			return true;
		} catch (Exception e) {
			LOGGER.catching(e);
		}
		return false;
	}

	public void removeIgnore(int playerID, long friend) {
		try {
			if (useTransactions)
				getDatabaseConnection().executeQuery("START TRANSACTION");
			PreparedStatement prepared = getDatabaseConnection().prepareStatement(getDatabaseConnection().getGameQueries().removeIgnore);
			prepared.setInt(1, playerID);
			prepared.setLong(2, friend);
			prepared.executeUpdate();
			if (useTransactions)
				getDatabaseConnection().executeQuery("COMMIT");
		} catch (Exception e) {
			LOGGER.catching(e);
		}
	}

	public void chatBlock(int on, long user) {
		updateIntsLongs(getDatabaseConnection().getGameQueries().chatBlock, new int[]{on}, new long[]{user});
	}

	public void privateBlock(int on, long user) {
		updateIntsLongs(getDatabaseConnection().getGameQueries().privateBlock, new int[]{on}, new long[]{user});
	}

	public void tradeBlock(int on, long user) {
		updateIntsLongs(getDatabaseConnection().getGameQueries().tradeBlock, new int[]{on}, new long[]{user});
	}

	public void duelBlock(int on, long user) {
		updateIntsLongs(getDatabaseConnection().getGameQueries().duelBlock, new int[]{on}, new long[]{user});
	}

	public void addNpcDrop(Player player, Npc npc, int dropId, int dropAmount) {
		try {
			PreparedStatement statementInsert = getServer().getDatabaseConnection().prepareStatement(
				getDatabaseConnection().getGameQueries().npcDropInsert);
			statementInsert.setInt(1, dropId);
			statementInsert.setInt(2, player.getDatabaseID());
			statementInsert.setInt(3, dropAmount);
			statementInsert.setInt(4, npc.getID());
			int insertResult = statementInsert.executeUpdate();
		} catch (SQLException e) {
			LOGGER.catching(e);
		}
	}

	private boolean playerExists(int user) {
		return hasNextFromInt(getDatabaseConnection().getGameQueries().basicInfo, user);
	}

	private void setGameSettings(boolean[] settings, int user) {
		for (int i = 0; i < settings.length; i++) {
			getDatabaseConnection().executeUpdate("UPDATE `" + getDatabaseConnection().getGameQueries().PREFIX + "players` SET " + gameSettings[i] + "="
				+ (settings[i] ? 1 : 0) + " WHERE id='" + user + "'");
		}
	}

	private void setPrivacySettings(boolean[] settings, int user) {
		for (int i = 0; i < settings.length; i++) {
			getDatabaseConnection().executeUpdate("UPDATE `" + getDatabaseConnection().getGameQueries().PREFIX + "players` SET " + privacySettings[i] + "="
				+ (settings[i] ? 1 : 0) + " WHERE id='" + user + "'");
		}
	}

	/*public void setTeleportStones(int stones, int user) {
		conn.executeUpdate("UPDATE `users` SET teleport_stone="
			+ stones + " WHERE id='" + user + "'");
	}*/

	private void savePlayersNPCKills(Player player) {
		Map<Integer, Integer> uniqueIDMap = new HashMap<>();


		try {
			ResultSet result = resultSetFromInteger(getDatabaseConnection().getGameQueries().npcKillSelectAll, player.getDatabaseID());
			while (result.next()) {
				int key = result.getInt("npcID");
				int value = result.getInt("ID");
				uniqueIDMap.put(key, value);
			}

			PreparedStatement statement = getDatabaseConnection().prepareStatement(getDatabaseConnection().getGameQueries().npcKillUpdate);
			PreparedStatement statementInsert = getDatabaseConnection().prepareStatement(getDatabaseConnection().getGameQueries().npcKillInsert);
			for (Iterator<Map.Entry<Integer, Integer>> it = player.getKillCache().entrySet().iterator(); it.hasNext(); ) {
				Map.Entry<Integer, Integer> e = it.next();
				if (!uniqueIDMap.containsKey(e.getKey())) {
					statementInsert.setInt(1, e.getValue());
					statementInsert.setInt(2, e.getKey());
					statementInsert.setInt(3, player.getDatabaseID());
					statementInsert.addBatch();
				} else {
					statement.setInt(1, e.getValue());
					statement.setInt(2, uniqueIDMap.get(e.getKey()));
					statement.setInt(3, e.getKey());
					statement.setInt(4, player.getDatabaseID());
					statement.addBatch();
				}
			}
			statement.executeBatch();
			statementInsert.executeBatch();
		} catch (SQLException a) {
			LOGGER.catching(a);
		}
		player.setKillCacheUpdated(false);
	}

	public Player loadPlayer(LoginRequest rq) {
		Player save = new Player(getServer().getWorld(), rq);
		ResultSet result = resultSetFromString(getDatabaseConnection().getGameQueries().playerData, save.getUsername());
		try {
			if (!result.next()) {
				return save;
			}
			save.setOwner(result.getInt("id"));
			save.setDatabaseID(result.getInt("id"));
			save.setGroupID(result.getInt("group_id"));
			save.setCombatStyle((byte) result.getInt("combatstyle"));
			save.setLastLogin(result.getLong("login_date"));
			save.setLastIP(result.getString("login_ip"));
			save.setInitialLocation(new Point(result.getInt("x"), result.getInt("y")));

			save.setFatigue(result.getInt("fatigue"));
			save.setKills(result.getInt("kills"));
			save.setDeaths(result.getInt("deaths"));
			save.setKills(result.getInt("kills"));
			save.setPkPoints(result.getInt("pkpoints"));
			save.setPoints(result.getInt("points"));
			save.setF2OppName(result.getInt("f2oppname"));
			save.setF2Hits(result.getInt("f2hits"));
			save.setF2Fatigue(result.getInt("f2fatigue"));
			save.setF2Prayer(result.getInt("f2prayer"));
			save.setF2Date(result.getInt("f2date"));
			save.setF2KillStreak(result.getInt("f2killstreak"));
			save.setOp(result.getInt("op"));
			save.setClanPoints(0);
			save.setP1Total(result.getInt("p1total"));
			save.setP2Total(result.getInt("p2total"));
			save.setP3Total(result.getInt("p3total"));
			save.setP4Total(result.getInt("p4total"));
			save.setP5Total(result.getInt("p5total"));
			save.setP6Total(result.getInt("p6total"));
			save.setP10(result.getInt("p10"));
			save.setP11(result.getInt("p11"));
			save.setP12(result.getInt("p12"));
			save.setP13(result.getInt("p13"));
			save.setP14(result.getInt("p14"));
			save.setP15(result.getInt("p15"));
			save.setP16(result.getInt("p16"));
			save.setP20(result.getInt("p20"));
			save.setP21(result.getInt("p21"));
			save.setP22(result.getInt("p22"));
			save.setP23(result.getInt("p23"));
			save.setP24(result.getInt("p24"));
			save.setP25(result.getInt("p25"));
			save.setP26(result.getInt("p26"));
			save.setP30(result.getInt("p30"));
			save.setP31(result.getInt("p31"));
			save.setP32(result.getInt("p32"));
			save.setP33(result.getInt("p33"));
			save.setP34(result.getInt("p34"));
			save.setP35(result.getInt("p35"));
			save.setP36(result.getInt("p36"));
			save.setP40(result.getInt("p40"));
			save.setP41(result.getInt("p41"));
			save.setP42(result.getInt("p42"));
			save.setP43(result.getInt("p43"));
			save.setP44(result.getInt("p44"));
			save.setP45(result.getInt("p45"));
			save.setP46(result.getInt("p46"));
			save.setP50(result.getInt("p50"));
			save.setP51(result.getInt("p51"));
			save.setP52(result.getInt("p52"));
			save.setP53(result.getInt("p53"));
			save.setP54(result.getInt("p54"));
			save.setP55(result.getInt("p55"));
			save.setP56(result.getInt("p56"));
			save.setP60(result.getInt("p60"));
			save.setP61(result.getInt("p61"));
			save.setP62(result.getInt("p62"));
			save.setP63(result.getInt("p63"));
			save.setP64(result.getInt("p64"));
			save.setP65(result.getInt("p65"));
			save.setP66(result.getInt("p66"));
			save.setWantLoginBox(result.getInt("wantloginbox"));
			save.setWantF2SideMenu(result.getInt("wantf2sidemenu"));
			save.setWantOgCombatStyleBox(result.getInt("wantogcombatstylebox"));
			save.setIronMan(result.getInt("iron_man"));
			save.setIronManRestriction(result.getInt("iron_man_restriction"));
			save.setHCIronmanDeath(result.getInt("hc_ironman_death"));
			save.setQuestPoints(result.getShort("quest_points"));

			save.getSettings().setPrivacySetting(0, result.getInt("block_chat") == 1); // done
			save.getSettings().setPrivacySetting(1, result.getInt("block_private") == 1);
			save.getSettings().setPrivacySetting(2, result.getInt("block_trade") == 1);
			save.getSettings().setPrivacySetting(3, result.getInt("block_duel") == 1);

			save.getSettings().setGameSetting(0, result.getInt("cameraauto") == 1);
			save.getSettings().setGameSetting(1, result.getInt("onemouse") == 1);
			save.getSettings().setGameSetting(2, result.getInt("soundoff") == 1);

			save.setBankSize(result.getShort("bank_size"));

			PlayerAppearance pa = new PlayerAppearance(result.getInt("haircolour"), result.getInt("topcolour"),
				result.getInt("trousercolour"), result.getInt("skincolour"), result.getInt("headsprite"),
				result.getInt("bodysprite"));

			save.getSettings().setAppearance(pa);
			save.setMale(result.getInt("male") == 1);
			save.setClanKills(result.getInt("clankills"));
			save.setClanDeaths(result.getInt("clandeaths"));
			save.setWornItems(save.getSettings().getAppearance().getSprites());

			save.getSkills().loadExp(fetchExperience(save.getDatabaseID()));

			save.getSkills().loadLevels(fetchLevels(save.getDatabaseID()));

			result = resultSetFromInteger(getDatabaseConnection().getGameQueries().playerPendingRecovery, save.getDatabaseID());
			if (result.next()) {
				save.setLastRecoveryChangeRequest(result.getLong("date_set"));
			}

			result = resultSetFromInteger(getDatabaseConnection().getGameQueries().playerInvItems, save.getDatabaseID());

			Inventory inv = new Inventory(save);
			Equipment equipment = new Equipment(save);


			while (result.next()) {
				Item item = new Item(result.getInt("id"), result.getInt("amount"));
				ItemDefinition itemDef = item.getDef(save.getWorld());
				item.setWielded(false);
				if (item.isWieldable(save.getWorld()) && result.getInt("wielded") == 1) {
					if (itemDef != null) {
						if (getServer().getConfig().WANT_EQUIPMENT_TAB)
							equipment.equip(itemDef.getWieldPosition(), item);
						else {
							item.setWielded(true);
							inv.add(item, false);
						}
						save.updateWornItems(itemDef.getWieldPosition(), itemDef.getAppearanceId(),
							itemDef.getWearableId(), true);

					}
				} else
					inv.add(item, false);
			}
			save.setInventory(inv);

			if (getServer().getConfig().WANT_EQUIPMENT_TAB) {
				result = resultSetFromInteger(getDatabaseConnection().getGameQueries().playerEquipped, save.getDatabaseID());
				while (result.next()) {
					Item item = new Item(result.getInt("id"), result.getInt("amount"));
					ItemDefinition itemDef = item.getDef(save.getWorld());
					if (item.isWieldable(save.getWorld())) {
						equipment.equip(itemDef.getWieldPosition(), item);
						save.updateWornItems(itemDef.getWieldPosition(), itemDef.getAppearanceId(),
							itemDef.getWearableId(), true);
					}
				}

				save.setEquipment(equipment);
			}

			result = resultSetFromInteger(getDatabaseConnection().getGameQueries().playerBankItems, save.getDatabaseID());
			Bank bank = new Bank(save);
			while (result.next()) {
				bank.add(new Item(result.getInt("id"), result.getInt("amount")));
			}
			if (getServer().getConfig().WANT_BANK_PRESETS) {
				result = resultSetFromInteger(getDatabaseConnection().getGameQueries().playerBankPresets, save.getDatabaseID());
				while (result.next()) {
					int slot = result.getInt("slot");
					Blob inventoryItems = result.getBlob("inventory");
					Blob equipmentItems = result.getBlob("equipment");
					bank.loadPreset(slot, inventoryItems, equipmentItems);
				}
			}
			save.setBank(bank);

			save.getSocial().addFriends(longListFromResultSet(
				resultSetFromInteger(getDatabaseConnection().getGameQueries().playerFriends, save.getDatabaseID()), "friend"));

			save.getSocial().addIgnore(longListFromResultSet(
				resultSetFromInteger(getDatabaseConnection().getGameQueries().playerIngored, save.getDatabaseID()), "ignore"));

			result = resultSetFromInteger(getDatabaseConnection().getGameQueries().playerQuests, save.getDatabaseID());
			while (result.next()) {
				save.setQuestStage(result.getInt("id"), result.getInt("stage"));
			}

			save.setQuestPoints(save.calculateQuestPoints());

			/*result = resultSetFromInteger(conn.getGameQueries().playerAchievements, save.getDatabaseID());
			while (result.next()) {
				save.setAchievementStatus(result.getInt("id"), result.getInt("status"));
			}*/

			result = resultSetFromInteger(getDatabaseConnection().getGameQueries().playerCache, save.getDatabaseID());
			while (result.next()) {
				int identifier = result.getInt("type");

				String key = result.getString("key");
				if (identifier == 0) {
					save.getCache().put(key, result.getInt("value"));
				}
				if (identifier == 1) {
					save.getCache().put(key, result.getString("value"));
				}
				if (identifier == 2) {
					save.getCache().put(key, result.getBoolean("value"));
				}
				if (identifier == 3) {
					save.getCache().put(key, result.getLong("value"));
				}
			}

			try {
				save.setCastTimer(save.getCache().getLong("last_spell_cast"));
			} catch (Throwable t) {
				save.setCastTimer();
			}

			result = resultSetFromInteger(getDatabaseConnection().getGameQueries().npcKillSelectAll, save.getDatabaseID());
			while (result.next()) {
				int key = result.getInt("npcID");
				int value = result.getInt("killCount");
				save.getKillCache().put(key, value);
			}

			/*result = resultSetFromInteger(conn.getGameQueries().unreadMessages, save.getOwner());
			while (result.next()) {
				save.setUnreadMessages(result.getInt(1));
			}*/

			/*result = resultSetFromInteger(conn.getGameQueries().teleportStones, save.getOwner());
			while (result.next()) {
				save.setTeleportStones(result.getInt(1));
			}*/

		} catch (SQLException e) {
			LOGGER.catching(e);
			return null;
		}
		return save;
	}

	private ResultSet resultSetFromString(String query, String... longA) {
		PreparedStatement prepared = null;
		ResultSet result = null;
		try {
			prepared = getDatabaseConnection().prepareStatement(query);

			for (int i = 1; i <= longA.length; i++) {
				prepared.setString(i, longA[i - 1]);
			}

			result = prepared.executeQuery();

		} catch (SQLException e) {
			if (prepared != null)
				LOGGER.catching(e);
			else
				System.out.println("Failed to create prepared statement: " + query);
		}
		return result;
	}

	private void updateLongs(String statement, int... intA) {
		PreparedStatement prepared = null;
		try {
			if (useTransactions)
				getDatabaseConnection().executeQuery("START TRANSACTION");
			prepared = getDatabaseConnection().prepareStatement(statement);

			for (int i = 1; i <= intA.length; i++) {
				prepared.setInt(i, intA[i - 1]);
			}

			prepared.executeUpdate();

			if (useTransactions)
				getDatabaseConnection().executeQuery("COMMIT");
		} catch (SQLException e) {
			if (prepared != null)
				LOGGER.catching(e);
			else
				System.out.println("Failed to create prepared statement: " + statement);
		}
	}

	private void updateIntsLongs(String statement, int[] intA, long[] longA) {
		PreparedStatement prepared = null;
		try {
			if (useTransactions)
				getDatabaseConnection().executeQuery("START TRANSACTION");
			prepared = getDatabaseConnection().prepareStatement(statement);

			for (int i = 1; i <= intA.length; i++) {
				prepared.setInt(i, intA[i - 1]);
			}
			int offset = intA.length + 1;
			for (int i = 0; i < longA.length; i++) {
				prepared.setLong(i + offset, longA[i]);
			}

			prepared.executeUpdate();

			if (useTransactions)
				getDatabaseConnection().executeQuery("COMMIT");
		} catch (SQLException e) {
			if (prepared != null)
				LOGGER.catching(e);
			else
				System.out.println("Failed to create prepared statement: " + statement);
		}
	}

	public String banPlayer(String user, int time) {
		String query;
		String replyMessage;
		if (time == -1) {
			query = "UPDATE `" + getDatabaseConnection().getGameQueries().PREFIX + "players` SET `banned`='" + time + "' WHERE `username` LIKE '" + user + "'";
			replyMessage = user + " has been banned permanently";
		} else if (time == 0) {
			query = "UPDATE `" + getDatabaseConnection().getGameQueries().PREFIX + "players` SET `banned`='" + time + "' WHERE `username` LIKE '" + user + "'";
			replyMessage = user + " has been unbanned.";
		} else {
			query = "UPDATE `" + getDatabaseConnection().getGameQueries().PREFIX + "players` SET `banned`='" + (System.currentTimeMillis() + (time * 60000))
				+ "', offences = offences + 1 WHERE `username` LIKE '" + user + "'";
			replyMessage = user + " has been banned for " + time + " minutes";
		}
		try {
			getServer().getDatabaseConnection().executeUpdate(query);
		} catch (Exception e) {
			return "There is not an account by that username";
		}
		return replyMessage;
	}

	private int[] fetchLevels(int playerID) {
		ResultSet result = resultSetFromInteger(getDatabaseConnection().getGameQueries().playerCurExp, playerID);
		try {
			result.next();
		} catch (SQLException e1) {
			LOGGER.catching(e1);
			return null;
		}
		int[] data = new int[getServer().getConstants().getSkills().getSkillsCount()];
		for (int i = 0; i < data.length; i++) {
			try {
				data[i] = result.getInt("cur_" + getServer().getConstants().getSkills().getSkillName(i));
			} catch (SQLException e) {
				LOGGER.catching(e);
				return null;
			}
		}
		return data;
	}

	private int[] fetchExperience(int playerID) {
		int[] data = new int[getServer().getConstants().getSkills().getSkillsCount()];
		try {
			PreparedStatement statement = getDatabaseConnection().prepareStatement(getDatabaseConnection().getGameQueries().playerExp);
			statement.setInt(1, playerID);
			ResultSet result = statement.executeQuery();
			result.next();
			for (int i = 0; i < data.length; i++) {
				try {
					data[i] = result.getInt("exp_" + getServer().getConstants().getSkills().getSkillName(i));
				} catch (SQLException e) {
					LOGGER.catching(e);
					return null;
				}
			}
		} catch (SQLException e1) {
			LOGGER.catching(e1);
			return null;
		}
		return data;
	}

	private List<Long> longListFromResultSet(ResultSet result, String param) throws SQLException {
		List<Long> list = new ArrayList<Long>();

		while (result.next()) {
			list.add(result.getLong(param));
		}

		return list;
	}

	private ResultSet resultSetFromInteger(String statement, int... longA) {
		PreparedStatement prepared = null;
		ResultSet result = null;
		try {
			prepared = getDatabaseConnection().prepareStatement(statement);

			for (int i = 1; i <= longA.length; i++) {
				prepared.setInt(i, longA[i - 1]);
			}

			result = prepared.executeQuery();

		} catch (SQLException e) {
			if (prepared != null)
				LOGGER.catching(e);
			else
				System.out.println("Failed to create prepared statement: " + statement);
		}
		return result;
	}

	private boolean hasNextFromInt(String statement, int... intA) {
		PreparedStatement prepared = null;
		ResultSet result = null;
		try {
			prepared = getDatabaseConnection().prepareStatement(statement);

			for (int i = 1; i <= intA.length; i++) {
				prepared.setInt(i, intA[i - 1]);
			}

			result = prepared.executeQuery();
		} catch (SQLException e) {
			if (prepared != null)
				LOGGER.catching(e);
			else
				System.out.println("Failed to create prepared statement: " + statement);
		}

		try {
			return Objects.requireNonNull(result).next();
		} catch (Exception e) {
			return false;
		}
	}
}
